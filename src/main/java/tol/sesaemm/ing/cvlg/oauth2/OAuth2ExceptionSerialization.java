/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.cvlg.oauth2;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;

/**
 *
 * @author I. en C. Cristian Luna <cristian.luna@sesaemm.org.mx>
 * Colaboracion: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
@JsonSerialize(using = OAuth2ExceptionsSerializer.class)
public class OAuth2ExceptionSerialization extends OAuth2Exception
{ 
    /**
     * @param mensaje
     */
    public OAuth2ExceptionSerialization(String mensaje)
    {
        super(mensaje);
    }
} 