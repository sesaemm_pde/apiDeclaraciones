/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.cvlg.oauth2;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 *
 * @author I. en C. Cristian Luna <cristian.luna@sesaemm.org.mx>
 * Colaboracion: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "tol.sesaemm.ing.cvlg")
public class OAuth2Configuration
{ 
    
    /**
     * @return BCryptPasswordEncoder
     */
    @Bean
    public BCryptPasswordEncoder bcryptPasswordEncoder()
    {
        return new BCryptPasswordEncoder();
    }    
    
    /**
     * @return PuntoEntradaAutenticacion
     */
    @Bean
    public AuthenticationForEntryPoint puntoEntradaAutenticacion()
    {
        return new AuthenticationForEntryPoint();
    }
    
    /**
     * @return ControladorAccesoDenegado
     */
    @Bean
    public ControllerAccessDenied controladorAccesoDenegado()
    {
        return new ControllerAccessDenied();
    }
    
    /**
     * @return TokenStore
     */
    @Bean
    public TokenStore tokenStore()
    {
        InMemoryTokenStore inMemoryTokenStore = new InMemoryTokenStore();
        inMemoryTokenStore.setAuthenticationKeyGenerator(new AuthenticationKeysGenerator());

        return inMemoryTokenStore;
    }    
    
    /**
     * @return TraductorExcepcionesRespuestaWeb
     */
    @Bean
    public ExceptionTranslatorWebResponse traductorExcepcionesRespuestaWeb()
    {
        return new ExceptionTranslatorWebResponse();
    }
    
    /**
     * @return InformacionAdicionalRespuestaTokenAcceso
     */    
    @Bean
    public AdditionalInformationAccessTokenResponse informacionAdicionalRespuestaTokenAcceso()
    {
        return new AdditionalInformationAccessTokenResponse();
    }
    
} 