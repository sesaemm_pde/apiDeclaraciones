/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.cvlg.oauth2;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 *
 * @author I. en C. Cristian Luna <cristian.luna@sesaemm.org.mx>
 * Colaboracion: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class WebApplicationSecurityInitializer extends AbstractSecurityWebApplicationInitializer{}