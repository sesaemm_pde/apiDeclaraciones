/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.cvlg.oauth2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import tol.sesaemm.ing.jidv.config.ConfVariablesEjecucion;

/**
 *
 * @author I. en C. Cristian Luna <cristian.luna@sesaemm.org.mx>
 * Colaboracion: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ismael Ortiz
 * ismael.ortiz@sesaemm.org.mx
 */
@Configuration
@EnableAuthorizationServer
public class AuthenticationServerConfiguration extends AuthorizationServerConfigurerAdapter
{

    @Autowired
    @Qualifier("bcryptPasswordEncoder")
    private BCryptPasswordEncoder bcryptPasswordEncoder;

    @Autowired
    @Qualifier("traductorExcepcionesRespuestaWeb")
    private ExceptionTranslatorWebResponse traductorExcepcionesRespuestaWeb;

    @Autowired
    @Qualifier("tokenStore")
    private TokenStore tokenStore;

    @Autowired
    @Qualifier("authenticationManagerBean")
    private AuthenticationManager authenticationManager;

    @Autowired
    @Qualifier("informacionAdicionalRespuestaTokenAcceso")
    private AdditionalInformationAccessTokenResponse informacionAdicionalRespuestaTokenAcceso;

    /**
     * @param clients
     * @throws Exception
     */
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception
    {
        clients.inMemory()
                .withClient(ConfVariablesEjecucion.tokenCLIENT)
                .secret(bcryptPasswordEncoder.encode(ConfVariablesEjecucion.tokenSECRET))
                .scopes("read")
                .authorizedGrantTypes("password", "refresh_token")
                .authorities("ROLE_CLIENT", "ROLE_TRUSTED_CLIENT")
                .accessTokenValiditySeconds(360)
                .refreshTokenValiditySeconds(720);
    }

    /**
     * @param endpoints
     * @throws Exception
     */
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception
    {
        endpoints
                .pathMapping("/oauth/token", "/oauth")
                .exceptionTranslator(traductorExcepcionesRespuestaWeb)
                .tokenStore(tokenStore)
                .authenticationManager(authenticationManager)
                .tokenEnhancer(informacionAdicionalRespuestaTokenAcceso)
                .reuseRefreshTokens(false);
    }

    /**
     * @param oauthServer
     * @throws Exception
     */
    @Override
    public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception
    {
        oauthServer.realm("/apiSPIC").allowFormAuthenticationForClients();
    }
}
