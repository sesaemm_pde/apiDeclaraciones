/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.cvlg.oauth2;

import java.util.HashMap;
import java.util.Map;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

/**
 *
 * @author I. en C. Cristian Luna <cristian.luna@sesaemm.org.mx>
 * Colaboracion: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class AdditionalInformationAccessTokenResponse implements TokenEnhancer
{ 
    /**
     * @param oaat
     * @param oaa
     * @return OAuth2AccessToken
     */
    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken oaat, OAuth2Authentication oaa)
    { 
        Map<String, Object> listaInformacionAdicional;      
        
        listaInformacionAdicional = new HashMap<>();
        listaInformacionAdicional.put("refresh_token_expires_in", 720);
        
        ((DefaultOAuth2AccessToken) oaat).setAdditionalInformation(listaInformacionAdicional);
        
        return oaat;
    } 
} 