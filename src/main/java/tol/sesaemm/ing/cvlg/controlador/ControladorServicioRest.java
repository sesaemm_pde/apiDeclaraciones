package tol.sesaemm.ing.cvlg.controlador;

import com.google.gson.Gson;
import org.bson.Document;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tol.sesaemm.ing.jidv.interfaces.LogicaDeNegocio;
import tol.sesaemm.ing.jidv.logicaNegocio.LogicaDeNegocioV01;
import tol.sesaemm.javabeans.s1declaraciones.Declaraciones;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesPost;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx
 * Colaboracion: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
@RestController
public class ControladorServicioRest
{

    /**
     * Mensaje principale del web service
     *
     * @return
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String webapp()
    {
        return "Servicio Web de conexión SESAEMM - SESNA";
    }

    /**
     * Obtiene en formato Json estructura peticion Post (atributos de paginacion
     * y resultados de consulta) de las declaraciones
     *
     * @param Bcuerpo Estructura Post
     *
     * @throws Exception
     * @return Cadena de estructura Post en formato Json
     */
    @RequestMapping(value = "/v2/declaraciones", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public String obtenerEstructuraPostFormatoJsonDeclaraciones(@RequestBody(required = false) String Bcuerpo) throws Exception
    { 

        LogicaDeNegocio logicaNegocio;                  
        DeclaracionesPost estructuraPost;               
        Declaraciones declaraciones;                    
        Document documentoDeError;                      
        String strRFCSolicitante;                       
        boolean bolMostrarDatosPublicosYPrivados;       

        logicaNegocio = new LogicaDeNegocioV01();

        try
        {

            Bcuerpo = new String(Bcuerpo.getBytes("ISO-8859-1"), "UTF-8");
            estructuraPost = new Gson().fromJson(Bcuerpo, DeclaracionesPost.class);
            estructuraPost = logicaNegocio.obtenerValoresPorDefaultFiltroDeclaraciones(estructuraPost);
            strRFCSolicitante = "";
            
            if (estructuraPost.getQuery().getRfcSolicitante() != null)
            { 
                if (estructuraPost.getQuery().getRfcSolicitante().equals("") == false)
                { 
                    strRFCSolicitante = estructuraPost.getQuery().getRfcSolicitante();
                } 
            } 
            
            bolMostrarDatosPublicosYPrivados = logicaNegocio.esValidoRFC(strRFCSolicitante);             
            declaraciones = logicaNegocio.obtenerDeclaracionesPost(estructuraPost);  
            logicaNegocio.guardarBitacoraDeEventos(estructuraPost, declaraciones.getPagination(), "Correcta");
            
            return logicaNegocio.generarJsonDeclaracionesPost(declaraciones, bolMostrarDatosPublicosYPrivados);
        }
        catch (Exception ex)
        {

            documentoDeError = new Document();
            documentoDeError.append("code", "declaraciones");
            documentoDeError.append("message", "Error al obtener listado de declaraciones: " + ex.toString());

            estructuraPost = logicaNegocio.obtenerValoresPorDefaultFiltroDeclaraciones(new DeclaracionesPost());
            logicaNegocio.guardarBitacoraDeEventos(estructuraPost, null, "Error al obtener listado de declaraciones: " + ex.toString());

            return documentoDeError.toJson();
        }
    } 

    @RequestMapping(value = "/403", produces = "application/json;charset=UTF-8")
    public String error403()
    {

        Document documentoDeError = new Document();

        documentoDeError.append("code", "403");
        documentoDeError.append("message", "Forbidden");

        return documentoDeError.toJson();
    }

    @RequestMapping(value = "/404", produces = "application/json;charset=UTF-8")
    public String error404()
    {

        Document documentoDeError = new Document();

        documentoDeError.append("code", "404");
        documentoDeError.append("message", "Not Found");

        return documentoDeError.toJson();
    }

    @RequestMapping(value = "/405", produces = "application/json;charset=UTF-8")
    public String error405()
    {

        Document documentoDeError = new Document();

        documentoDeError.append("code", "405");
        documentoDeError.append("message", "Method Not Allowed");

        return documentoDeError.toJson();
    }

    @RequestMapping(value = "/500", produces = "application/json;charset=UTF-8")
    public String error500()
    {

        Document documentoDeError = new Document();

        documentoDeError.append("code", "500");
        documentoDeError.append("message", "Internal Server Error");

        return documentoDeError.toJson();
    }
    
}
