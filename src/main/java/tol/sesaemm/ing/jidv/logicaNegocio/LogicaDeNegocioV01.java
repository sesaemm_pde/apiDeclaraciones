/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.logicaNegocio;

import java.util.ArrayList;
import java.util.List;
import org.bson.Document;
import org.bson.json.JsonMode;
import org.bson.json.JsonWriterSettings;
import tol.sesaemm.funciones.s1declaraciones.DeclaracionesDocumentoActividadAnualAnterior;
import tol.sesaemm.funciones.s1declaraciones.DeclaracionesDocumentoAdeudos;
import tol.sesaemm.funciones.s1declaraciones.DeclaracionesDocumentoApoyos;
import tol.sesaemm.funciones.s1declaraciones.DeclaracionesDocumentoBeneficiosPrivados;
import tol.sesaemm.funciones.s1declaraciones.DeclaracionesDocumentoBienesInmuebles;
import tol.sesaemm.funciones.s1declaraciones.DeclaracionesDocumentoBienesMuebles;
import tol.sesaemm.funciones.s1declaraciones.DeclaracionesDocumentoClientesPrincipales;
import tol.sesaemm.funciones.s1declaraciones.DeclaracionesDocumentoDatosCurricularesDeclarante;
import tol.sesaemm.funciones.s1declaraciones.DeclaracionesDocumentoDatosDependienteEconomico;
import tol.sesaemm.funciones.s1declaraciones.DeclaracionesDocumentoDatosEmpleoCargoComision;
import tol.sesaemm.funciones.s1declaraciones.DeclaracionesDocumentoDatosGenerales;
import tol.sesaemm.funciones.s1declaraciones.DeclaracionesDocumentoDatosPareja;
import tol.sesaemm.funciones.s1declaraciones.DeclaracionesDocumentoDomicilioDeclarante;
import tol.sesaemm.funciones.s1declaraciones.DeclaracionesDocumentoExperienciaLaboral;
import tol.sesaemm.funciones.s1declaraciones.DeclaracionesDocumentoFideicomisos;
import tol.sesaemm.funciones.s1declaraciones.DeclaracionesDocumentoIngresos;
import tol.sesaemm.funciones.s1declaraciones.DeclaracionesDocumentoInversiones;
import tol.sesaemm.funciones.s1declaraciones.DeclaracionesDocumentoMetadata;
import tol.sesaemm.funciones.s1declaraciones.DeclaracionesDocumentoParticipacion;
import tol.sesaemm.funciones.s1declaraciones.DeclaracionesDocumentoParticipacionTomaDecisiones;
import tol.sesaemm.funciones.s1declaraciones.DeclaracionesDocumentoPrestamoOComodato;
import tol.sesaemm.funciones.s1declaraciones.DeclaracionesDocumentoRepresentacion;
import tol.sesaemm.funciones.s1declaraciones.DeclaracionesDocumentoVehiculos;
import tol.sesaemm.ing.jidv.integracion.DAOWs;
import tol.sesaemm.ing.jidv.interfaces.LogicaDeNegocio;
import tol.sesaemm.javabeans.USUARIO_TOKEN_ACCESO;
import tol.sesaemm.javabeans.s1declaraciones.Declaraciones;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesBienesInmueblesQuery;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesPagination;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesPost;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesQuery;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesResults;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesSort;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx
 */
public class LogicaDeNegocioV01 implements LogicaDeNegocio
{

    /**
     * Guardar acciones (eventos) del usuario en el uso de la API en bitacora
     *
     * @param estructuraPost      Estructura para la peticion POST
     * @param valoresDePaginacion Valores de paginacion
     *
     * @param strEstatusConsulta  Estatus de la consulta
     *
     * @throws Exception
     */
    @Override
    public void guardarBitacoraDeEventos(Object estructuraPost, Object valoresDePaginacion, String strEstatusConsulta) throws Exception
    { 
        DAOWs.guardarBitacoraDeEventos(estructuraPost, valoresDePaginacion, strEstatusConsulta);
    } 

    /**
     * Obtener listado de usuarios token de acceso
     *
     * @throws Exception
     * @return arreglo de usuarios token de acceso
     */
    @Override
    public ArrayList<USUARIO_TOKEN_ACCESO> obtenerUsuariosTokenDeAcceso() throws Exception
    { 
        return DAOWs.obtenerUsuariosTokenDeAcceso();
    } 
    
    /**
     * Obtener valores por default de la estructura post
     *
     * @param estructuraPost Estructura para la peticion POST
     *
     * @return Valores por default de la estructura post
     *
     * @throws Exception
     */
    @Override
    public DeclaracionesPost obtenerValoresPorDefaultFiltroDeclaraciones(DeclaracionesPost estructuraPost) throws Exception
    { 

        if (estructuraPost.getPage() <= 0)
        { 
            estructuraPost.setPage(1);
        } 

        if (estructuraPost.getPageSize() <= 0)
        { 
            estructuraPost.setPageSize(10);
        } 
        else if (estructuraPost.getPageSize() > 200)
        { 
            estructuraPost.setPageSize(200);
        } 
        
        if (estructuraPost.getSort() == null)
        { 
            estructuraPost.setSort(new DeclaracionesSort());
        } 

        if (estructuraPost.getQuery() == null)
        { 
            estructuraPost.setQuery(new DeclaracionesQuery());
        } 

        if (estructuraPost.getQuery().getBienesInmuebles() == null)
        { 
            estructuraPost.getQuery().setBienesInmuebles(new DeclaracionesBienesInmueblesQuery());
        } 

        return estructuraPost;
    } 

    /**
     * Obtener listado de declaraciones por alta, modificacion o conclusion
     *
     * @param estructuraPost Estructura para la peticion POST
     *
     * @return listado de declaraciones por alta, modificacion o conclusion
     *
     * @throws Exception
     */
    @Override
    public Declaraciones obtenerDeclaracionesPost(DeclaracionesPost estructuraPost) throws Exception
    { 
        return DAOWs.obtenerDeclaracionesPost(estructuraPost);
    } 

    /**
     * Obtener estatus de privacidad de los campos de declaraciones
     *
     * @param coleccion Coleccion de campos publicos
     *
     * @return Estatus de privacidad de los campos de declaraciones
     *
     * @throws Exception
     */

    /**
     * Generar en formato JSON las declaraciones consultadas
     *
     * @param estructuraDeclaraciones                   Objeto que almacena la estructura del
     * @param bolMostrarDatosPublicosYPrivados          Almacena si se muestran datos publicos y privados o solo publicos
     *
     * @return Formato JSON estructura POST
     *
     * @throws Exception
     */
    @Override
    public String generarJsonDeclaracionesPost(Declaraciones estructuraDeclaraciones, boolean bolMostrarDatosPublicosYPrivados) throws Exception
    { 

        DeclaracionesPagination atributosDePaginacion;                              
        ArrayList<DeclaracionesResults> declaraciones;                              
        Document documentoDeclaracion;                                              
        List<Document> listaDocumentosDeclaraciones;                               
        JsonWriterSettings settings;                                                
        Document documentoSituacionPatrimonial;                                     
        Document documentoInteres;                                                  
        Document documentoSituacionPatrimonialInteres;                              

        atributosDePaginacion = estructuraDeclaraciones.getPagination();
        declaraciones = estructuraDeclaraciones.getResults();

        listaDocumentosDeclaraciones = new ArrayList<>();

        for (DeclaracionesResults declaracion : declaraciones)
        { 
            
            documentoDeclaracion = new Document();
            
            if (declaracion.getMetadata().getTipo().toUpperCase().contains("INICI"))
            { 
                declaracion.getMetadata().setTipo("INICIAL");
            } 
            else if (declaracion.getMetadata().getTipo().toUpperCase().contains("MODIFICA"))
            { 
                declaracion.getMetadata().setTipo("MODIFICACIÓN");
            } 
            else if (declaracion.getMetadata().getTipo().toUpperCase().contains("CONCLU"))
            { 
                declaracion.getMetadata().setTipo("CONCLUSIÓN");
            } 
            
            documentoDeclaracion.append("id", declaracion.getIdSpdn().toHexString());
            documentoDeclaracion.append("metadata", new DeclaracionesDocumentoMetadata(declaracion.getMetadata()).obtenerMetadata());
            
            documentoSituacionPatrimonial = new Document();
            documentoInteres = new Document();
            documentoSituacionPatrimonialInteres = new Document();
            
            switch (declaracion.getMetadata().getTipo().toUpperCase())
            { 
                case "INICIAL":
                    try
                    { 
                        documentoSituacionPatrimonial.append("datosGenerales", new DeclaracionesDocumentoDatosGenerales(declaracion.getDeclaracion().getSituacionPatrimonial().getDatosGenerales(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoDeDatosGenerales());
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->datosGenerales: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        Document documentoDomicilioDeclarante = new DeclaracionesDocumentoDomicilioDeclarante(declaracion.getDeclaracion().getSituacionPatrimonial().getDomicilioDeclarante(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoDomicilioDeclarante();
                        if (documentoDomicilioDeclarante.isEmpty() == false)
                        { 
                            documentoSituacionPatrimonial.append("domicilioDeclarante", documentoDomicilioDeclarante);
                        } 
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->domicilioDeclarante: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        documentoSituacionPatrimonial.append("datosCurricularesDeclarante", new DeclaracionesDocumentoDatosCurricularesDeclarante(declaracion.getDeclaracion().getSituacionPatrimonial().getDatosCurricularesDeclarante(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoDatosCurricularesDeclarante());
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->datosCurricularesDeclarante: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        documentoSituacionPatrimonial.append("datosEmpleoCargoComision", new DeclaracionesDocumentoDatosEmpleoCargoComision(declaracion.getDeclaracion().getSituacionPatrimonial().getDatosEmpleoCargoComision(), declaracion.getMetadata().getTipo(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoDeDatosEmpleoCargoComision());
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->datosEmpleoCargoComision: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        documentoSituacionPatrimonial.append("experienciaLaboral", new DeclaracionesDocumentoExperienciaLaboral(declaracion.getDeclaracion().getSituacionPatrimonial().getExperienciaLaboral(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoExperienciaLaboral());
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->experienciaLaboral: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        Document documentoDatosPareja = new DeclaracionesDocumentoDatosPareja(declaracion.getDeclaracion().getSituacionPatrimonial().getDatosPareja(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoDatosPareja();
                        if (documentoDatosPareja.isEmpty() == false)
                        { 
                            documentoSituacionPatrimonial.append("datosPareja", documentoDatosPareja);
                        } 
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->datosPareja: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        Document documentoDatosDependienteEconomico = new DeclaracionesDocumentoDatosDependienteEconomico(declaracion.getDeclaracion().getSituacionPatrimonial().getDatosDependienteEconomico(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoDatosDependienteEconomico();
                        if (!documentoDatosDependienteEconomico.isEmpty())
                        { 
                            documentoSituacionPatrimonial.append("datosDependienteEconomico", documentoDatosDependienteEconomico);
                        } 
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->datosDependienteEconomico: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        documentoSituacionPatrimonial.append("ingresos", new DeclaracionesDocumentoIngresos(declaracion.getDeclaracion().getSituacionPatrimonial().getIngresos(), declaracion.getMetadata().getTipo(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoIngresos());
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->ingresos: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        documentoSituacionPatrimonial.append("actividadAnualAnterior", new DeclaracionesDocumentoActividadAnualAnterior(declaracion.getDeclaracion().getSituacionPatrimonial().getActividadAnualAnterior(), declaracion.getMetadata().getTipo(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoActividadAnualAnterior());
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->actividadAnualAnterior: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        Document documentoBienesInmuebles = new DeclaracionesDocumentoBienesInmuebles(declaracion.getDeclaracion().getSituacionPatrimonial().getBienesInmuebles(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoBienesInmuebles();
                        if (documentoBienesInmuebles.isEmpty() == false)
                        { 
                            documentoSituacionPatrimonial.append("bienesInmuebles", documentoBienesInmuebles);
                        } 
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->bienesInmuebles: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        Document documentoVehiculos = new DeclaracionesDocumentoVehiculos(declaracion.getDeclaracion().getSituacionPatrimonial().getVehiculos(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoVehiculos();
                        if (documentoVehiculos.isEmpty() == false)
                        { 
                            documentoSituacionPatrimonial.append("vehiculos", documentoVehiculos);
                        } 
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->vehiculos: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        Document documentoBienesMuebles = new DeclaracionesDocumentoBienesMuebles(declaracion.getDeclaracion().getSituacionPatrimonial().getBienesMuebles(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoBienesMuebles();
                        if (documentoBienesMuebles.isEmpty() == false)
                        { 
                            documentoSituacionPatrimonial.append("bienesMuebles", documentoBienesMuebles);
                        } 
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->bienesMuebles: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        Document documentoInversiones = new DeclaracionesDocumentoInversiones(declaracion.getDeclaracion().getSituacionPatrimonial().getInversiones(), declaracion.getMetadata().getTipo(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoInversiones();
                        if (documentoInversiones.isEmpty() == false)
                        { 
                            documentoSituacionPatrimonial.append("inversiones", documentoInversiones);
                        } 
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->inversiones: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        Document documentoAdeudos = new DeclaracionesDocumentoAdeudos(declaracion.getDeclaracion().getSituacionPatrimonial().getAdeudos(), declaracion.getMetadata().getTipo(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoAdeudos();
                        if (documentoAdeudos.isEmpty() == false)
                        { 
                            documentoSituacionPatrimonial.append("adeudos", documentoAdeudos);
                        } 
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->adeudos: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        Document documentoPrestamoOComodato = new DeclaracionesDocumentoPrestamoOComodato(declaracion.getDeclaracion().getSituacionPatrimonial().getPrestamoOComodato(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoPrestamoOComodato();
                        if (documentoPrestamoOComodato.isEmpty() == false)
                        { 
                            documentoSituacionPatrimonial.append("prestamoOComodato", documentoPrestamoOComodato);
                        } 
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->prestamoOComodato: " + e.getMessage());
                    } 
                    
                    documentoSituacionPatrimonialInteres.append("situacionPatrimonial", documentoSituacionPatrimonial);
                    
                    if (declaracion.getDeclaracion().getInteres() != null)
                    { 
                        
                        try
                        { 
                            Document documentoParticipacion = new DeclaracionesDocumentoParticipacion(declaracion.getDeclaracion().getInteres().getParticipacion(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoParticipacion();
                            if (documentoParticipacion.isEmpty() == false)
                            { 
                                documentoInteres.append("participacion", documentoParticipacion);
                            } 
                        } 
                        catch (Exception e)
                        { 
                            throw new Exception("Error en interes->participacion: " + e.getMessage());
                        } 
                        
                        try
                        { 
                            Document documentoParticipacionTomaDecisiones = new DeclaracionesDocumentoParticipacionTomaDecisiones(declaracion.getDeclaracion().getInteres().getParticipacionTomaDecisiones(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoParticipacionTomaDecisiones();
                            if (documentoParticipacionTomaDecisiones.isEmpty() == false)
                            { 
                                documentoInteres.append("participacionTomaDecisiones", documentoParticipacionTomaDecisiones);
                            } 
                        } 
                        catch (Exception e)
                        { 
                            throw new Exception("Error en interes->participacionTomaDecisiones: " + e.getMessage());
                        } 
                        
                        try
                        { 
                            Document documentoApoyos = new DeclaracionesDocumentoApoyos(declaracion.getDeclaracion().getInteres().getApoyos(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoApoyos();
                            if (documentoApoyos.isEmpty() == false)
                            { 
                                documentoInteres.append("apoyos", documentoApoyos);
                            } 
                        } 
                        catch (Exception e)
                        { 
                            throw new Exception("Error en interes->apoyos: " + e.getMessage());
                        } 
                        
                        try
                        { 
                            Document documentoRepresentacion = new DeclaracionesDocumentoRepresentacion(declaracion.getDeclaracion().getInteres().getRepresentacion(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoRepresentacion();
                            if (documentoRepresentacion.isEmpty() == false)
                            { 
                                documentoInteres.append("representacion", documentoRepresentacion);
                            } 
                        } 
                        catch (Exception e)
                        { 
                            throw new Exception("Error en interes->representacion: " + e.getMessage());
                        } 
                        
                        try
                        { 
                            Document documentoClientesPrincipales = new DeclaracionesDocumentoClientesPrincipales(declaracion.getDeclaracion().getInteres().getClientesPrincipales(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoClientesPrincipales();
                            if (documentoClientesPrincipales.isEmpty() == false)
                            { 
                                documentoInteres.append("clientesPrincipales", documentoClientesPrincipales);
                            } 
                        } 
                        catch (Exception e)
                        { 
                            throw new Exception("Error en interes->clientesPrincipales: " + e.getMessage());
                        } 
                        
                        try
                        { 
                            Document documentoBeneficiosPrivados = new DeclaracionesDocumentoBeneficiosPrivados(declaracion.getDeclaracion().getInteres().getBeneficiosPrivados(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoBeneficiosPrivados();
                            if (documentoBeneficiosPrivados.isEmpty() == false)
                            { 
                                documentoInteres.append("beneficiosPrivados", documentoBeneficiosPrivados);
                            } 
                        } 
                        catch (Exception e)
                        { 
                            throw new Exception("Error en interes->beneficiosPrivados: " + e.getMessage());
                        } 
                        
                        try
                        { 
                            Document documentoFideicomisos = new DeclaracionesDocumentoFideicomisos(declaracion.getDeclaracion().getInteres().getFideicomisos(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoFideicomisos();
                            if (documentoFideicomisos.isEmpty() == false)
                            { 
                                documentoInteres.append("fideicomisos", documentoFideicomisos);
                            } 
                        } 
                        catch (Exception e)
                        { 
                            throw new Exception("Error en interes->fideicomisos: " + e.getMessage());
                        } 
                        
                        if (documentoInteres.isEmpty() == false)
                        { 
                            documentoSituacionPatrimonialInteres.append("interes", documentoInteres);
                        } 
                    } 
                    
                    documentoDeclaracion.append("declaracion", documentoSituacionPatrimonialInteres);
                    break;
                
                case "MODIFICACIÓN":
                
                    try
                    { 
                        documentoSituacionPatrimonial.append("datosGenerales", new DeclaracionesDocumentoDatosGenerales(declaracion.getDeclaracion().getSituacionPatrimonial().getDatosGenerales(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoDeDatosGenerales());
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->datosGenerales: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        Document documentoDomicilioDeclarante = new DeclaracionesDocumentoDomicilioDeclarante(declaracion.getDeclaracion().getSituacionPatrimonial().getDomicilioDeclarante(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoDomicilioDeclarante();
                        if (documentoDomicilioDeclarante.isEmpty() == false)
                        { 
                            documentoSituacionPatrimonial.append("domicilioDeclarante", documentoDomicilioDeclarante);
                        } 
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->domicilioDeclarante: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        documentoSituacionPatrimonial.append("datosCurricularesDeclarante", new DeclaracionesDocumentoDatosCurricularesDeclarante(declaracion.getDeclaracion().getSituacionPatrimonial().getDatosCurricularesDeclarante(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoDatosCurricularesDeclarante());
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->datosCurricularesDeclarante: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        documentoSituacionPatrimonial.append("datosEmpleoCargoComision", new DeclaracionesDocumentoDatosEmpleoCargoComision(declaracion.getDeclaracion().getSituacionPatrimonial().getDatosEmpleoCargoComision(), declaracion.getMetadata().getTipo(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoDeDatosEmpleoCargoComision());
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->datosEmpleoCargoComision: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        documentoSituacionPatrimonial.append("experienciaLaboral", new DeclaracionesDocumentoExperienciaLaboral(declaracion.getDeclaracion().getSituacionPatrimonial().getExperienciaLaboral(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoExperienciaLaboral());
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->experienciaLaboral: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        Document documentoDatosPareja = new DeclaracionesDocumentoDatosPareja(declaracion.getDeclaracion().getSituacionPatrimonial().getDatosPareja(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoDatosPareja();
                        if (documentoDatosPareja.isEmpty() == false)
                        { 
                            documentoSituacionPatrimonial.append("datosPareja", documentoDatosPareja);
                        } 
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->datosPareja: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        Document documentoDatosDependienteEconomico = new DeclaracionesDocumentoDatosDependienteEconomico(declaracion.getDeclaracion().getSituacionPatrimonial().getDatosDependienteEconomico(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoDatosDependienteEconomico();
                        if (!documentoDatosDependienteEconomico.isEmpty())
                        { 
                            documentoSituacionPatrimonial.append("datosDependienteEconomico", documentoDatosDependienteEconomico);
                        } 
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->datosDependienteEconomico: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        documentoSituacionPatrimonial.append("ingresos", new DeclaracionesDocumentoIngresos(declaracion.getDeclaracion().getSituacionPatrimonial().getIngresos(), declaracion.getMetadata().getTipo(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoIngresos());
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->ingresos: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        Document documentoBienesInmuebles = new DeclaracionesDocumentoBienesInmuebles(declaracion.getDeclaracion().getSituacionPatrimonial().getBienesInmuebles(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoBienesInmuebles();
                        if (documentoBienesInmuebles.isEmpty() == false)
                        { 
                            documentoSituacionPatrimonial.append("bienesInmuebles", documentoBienesInmuebles);
                        } 
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->bienesInmuebles: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        Document documentoVehiculos = new DeclaracionesDocumentoVehiculos(declaracion.getDeclaracion().getSituacionPatrimonial().getVehiculos(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoVehiculos();
                        if (documentoVehiculos.isEmpty() == false)
                        { 
                            documentoSituacionPatrimonial.append("vehiculos", documentoVehiculos);
                        } 
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->vehiculos: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        Document documentoBienesMuebles = new DeclaracionesDocumentoBienesMuebles(declaracion.getDeclaracion().getSituacionPatrimonial().getBienesMuebles(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoBienesMuebles();
                        if (documentoBienesMuebles.isEmpty() == false)
                        { 
                            documentoSituacionPatrimonial.append("bienesMuebles", documentoBienesMuebles);
                        } 
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->bienesMuebles: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        Document documentoInversiones = new DeclaracionesDocumentoInversiones(declaracion.getDeclaracion().getSituacionPatrimonial().getInversiones(), declaracion.getMetadata().getTipo(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoInversiones();
                        if (documentoInversiones.isEmpty() == false)
                        { 
                            documentoSituacionPatrimonial.append("inversiones", documentoInversiones);
                        } 
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->inversiones: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        Document documentoAdeudos = new DeclaracionesDocumentoAdeudos(declaracion.getDeclaracion().getSituacionPatrimonial().getAdeudos(), declaracion.getMetadata().getTipo(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoAdeudos();
                        if (documentoAdeudos.isEmpty() == false)
                        { 
                            documentoSituacionPatrimonial.append("adeudos", documentoAdeudos);
                        } 
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->adeudos: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        Document documentoPrestamoOComodato = new DeclaracionesDocumentoPrestamoOComodato(declaracion.getDeclaracion().getSituacionPatrimonial().getPrestamoOComodato(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoPrestamoOComodato();
                        if (documentoPrestamoOComodato.isEmpty() == false)
                        { 
                            documentoSituacionPatrimonial.append("prestamoOComodato", documentoPrestamoOComodato);
                        } 
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->prestamoOComodato: " + e.getMessage());
                    } 
                    
                    documentoSituacionPatrimonialInteres.append("situacionPatrimonial", documentoSituacionPatrimonial);
                    
                    if (declaracion.getDeclaracion().getInteres() != null)
                    { 
                        
                        try
                        { 
                            Document documentoParticipacion = new DeclaracionesDocumentoParticipacion(declaracion.getDeclaracion().getInteres().getParticipacion(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoParticipacion();
                            if (documentoParticipacion.isEmpty() == false)
                            { 
                                documentoInteres.append("participacion", documentoParticipacion);
                            } 
                        } 
                        catch (Exception e)
                        { 
                            throw new Exception("Error en interes->participacion: " + e.getMessage());
                        } 
                        
                        try
                        { 
                            Document documentoParticipacionTomaDecisiones = new DeclaracionesDocumentoParticipacionTomaDecisiones(declaracion.getDeclaracion().getInteres().getParticipacionTomaDecisiones(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoParticipacionTomaDecisiones();
                            if (documentoParticipacionTomaDecisiones.isEmpty() == false)
                            { 
                                documentoInteres.append("participacionTomaDecisiones", documentoParticipacionTomaDecisiones);
                            } 
                        } 
                        catch (Exception e)
                        { 
                            throw new Exception("Error en interes->participacionTomaDecisiones: " + e.getMessage());
                        } 
                        
                        try
                        { 
                            Document documentoApoyos = new DeclaracionesDocumentoApoyos(declaracion.getDeclaracion().getInteres().getApoyos(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoApoyos();
                            if (documentoApoyos.isEmpty() == false)
                            { 
                                documentoInteres.append("apoyos", documentoApoyos);
                            } 
                        } 
                        catch (Exception e)
                        { 
                            throw new Exception("Error en interes->apoyos: " + e.getMessage());
                        } 
                        
                        try
                        { 
                            Document documentoRepresentacion = new DeclaracionesDocumentoRepresentacion(declaracion.getDeclaracion().getInteres().getRepresentacion(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoRepresentacion();
                            if (documentoRepresentacion.isEmpty() == false)
                            { 
                                documentoInteres.append("representacion", documentoRepresentacion);
                            } 
                        } 
                        catch (Exception e)
                        { 
                            throw new Exception("Error en interes->representacion: " + e.getMessage());
                        } 
                        
                        try
                        { 
                            Document documentoClientesPrincipales = new DeclaracionesDocumentoClientesPrincipales(declaracion.getDeclaracion().getInteres().getClientesPrincipales(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoClientesPrincipales();
                            if (documentoClientesPrincipales.isEmpty() == false)
                            { 
                                documentoInteres.append("clientesPrincipales", documentoClientesPrincipales);
                            } 
                        } 
                        catch (Exception e)
                        { 
                            throw new Exception("Error en interes->clientesPrincipales: " + e.getMessage());
                        } 
                        
                        try
                        { 
                            Document documentoBeneficiosPrivados = new DeclaracionesDocumentoBeneficiosPrivados(declaracion.getDeclaracion().getInteres().getBeneficiosPrivados(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoBeneficiosPrivados();
                            if (documentoBeneficiosPrivados.isEmpty() == false)
                            { 
                                documentoInteres.append("beneficiosPrivados", documentoBeneficiosPrivados);
                            } 
                        } 
                        catch (Exception e)
                        { 
                            throw new Exception("Error en interes->beneficiosPrivados: " + e.getMessage());
                        } 
                        
                        try
                        { 
                            Document documentoFideicomisos = new DeclaracionesDocumentoFideicomisos(declaracion.getDeclaracion().getInteres().getFideicomisos(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoFideicomisos();
                            if (documentoFideicomisos.isEmpty() == false)
                            { 
                                documentoInteres.append("fideicomisos", documentoFideicomisos);
                            } 
                        } 
                        catch (Exception e)
                        { 
                            throw new Exception("Error en interes->fideicomisos: " + e.getMessage());
                        } 
                        
                        if (documentoInteres.isEmpty() == false)
                        { 
                            documentoSituacionPatrimonialInteres.append("interes", documentoInteres);
                        } 
                    } 
                    
                    documentoDeclaracion.append("declaracion", documentoSituacionPatrimonialInteres);
                    break;
                
                case "CONCLUSIÓN":
                
                    try
                    { 
                        documentoSituacionPatrimonial.append("datosGenerales", new DeclaracionesDocumentoDatosGenerales(declaracion.getDeclaracion().getSituacionPatrimonial().getDatosGenerales(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoDeDatosGenerales());
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->datosGenerales: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        Document documentoDomicilioDeclarante = new DeclaracionesDocumentoDomicilioDeclarante(declaracion.getDeclaracion().getSituacionPatrimonial().getDomicilioDeclarante(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoDomicilioDeclarante();
                        if (documentoDomicilioDeclarante.isEmpty() == false)
                        { 
                            documentoSituacionPatrimonial.append("domicilioDeclarante", documentoDomicilioDeclarante);
                        } 
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->domicilioDeclarante: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        documentoSituacionPatrimonial.append("datosCurricularesDeclarante", new DeclaracionesDocumentoDatosCurricularesDeclarante(declaracion.getDeclaracion().getSituacionPatrimonial().getDatosCurricularesDeclarante(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoDatosCurricularesDeclarante());
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->datosCurricularesDeclarante: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        documentoSituacionPatrimonial.append("datosEmpleoCargoComision", new DeclaracionesDocumentoDatosEmpleoCargoComision(declaracion.getDeclaracion().getSituacionPatrimonial().getDatosEmpleoCargoComision(), declaracion.getMetadata().getTipo(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoDeDatosEmpleoCargoComision());
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->datosEmpleoCargoComision: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        documentoSituacionPatrimonial.append("experienciaLaboral", new DeclaracionesDocumentoExperienciaLaboral(declaracion.getDeclaracion().getSituacionPatrimonial().getExperienciaLaboral(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoExperienciaLaboral());
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->experienciaLaboral: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        Document documentoDatosPareja = new DeclaracionesDocumentoDatosPareja(declaracion.getDeclaracion().getSituacionPatrimonial().getDatosPareja(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoDatosPareja();
                        if (documentoDatosPareja.isEmpty() == false)
                        { 
                            documentoSituacionPatrimonial.append("datosPareja", documentoDatosPareja);
                        } 
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->datosPareja: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        Document documentoDatosDependienteEconomico = new DeclaracionesDocumentoDatosDependienteEconomico(declaracion.getDeclaracion().getSituacionPatrimonial().getDatosDependienteEconomico(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoDatosDependienteEconomico();
                        if (!documentoDatosDependienteEconomico.isEmpty())
                        { 
                            documentoSituacionPatrimonial.append("datosDependienteEconomico", documentoDatosDependienteEconomico);
                        } 
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->datosDependienteEconomico: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        documentoSituacionPatrimonial.append("ingresos", new DeclaracionesDocumentoIngresos(declaracion.getDeclaracion().getSituacionPatrimonial().getIngresos(), declaracion.getMetadata().getTipo(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoIngresos());
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->ingresos: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        documentoSituacionPatrimonial.append("actividadAnualAnterior", new DeclaracionesDocumentoActividadAnualAnterior(declaracion.getDeclaracion().getSituacionPatrimonial().getActividadAnualAnterior(), declaracion.getMetadata().getTipo(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoActividadAnualAnterior());
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->actividadAnualAnterior: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        Document documentoBienesInmuebles = new DeclaracionesDocumentoBienesInmuebles(declaracion.getDeclaracion().getSituacionPatrimonial().getBienesInmuebles(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoBienesInmuebles();
                        if (documentoBienesInmuebles.isEmpty() == false)
                        { 
                            documentoSituacionPatrimonial.append("bienesInmuebles", documentoBienesInmuebles);
                        } 
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->bienesInmuebles: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        Document documentoVehiculos = new DeclaracionesDocumentoVehiculos(declaracion.getDeclaracion().getSituacionPatrimonial().getVehiculos(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoVehiculos();
                        if (documentoVehiculos.isEmpty() == false)
                        { 
                            documentoSituacionPatrimonial.append("vehiculos", documentoVehiculos);
                        } 
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->vehiculos: " + e.getMessage());
                    } 
                
                    try
                    { 
                        Document documentoBienesMuebles = new DeclaracionesDocumentoBienesMuebles(declaracion.getDeclaracion().getSituacionPatrimonial().getBienesMuebles(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoBienesMuebles();
                        if (documentoBienesMuebles.isEmpty() == false)
                        { 
                            documentoSituacionPatrimonial.append("bienesMuebles", documentoBienesMuebles);
                        } 
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->bienesMuebles: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        Document documentoInversiones = new DeclaracionesDocumentoInversiones(declaracion.getDeclaracion().getSituacionPatrimonial().getInversiones(), declaracion.getMetadata().getTipo(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoInversiones();
                        if (documentoInversiones.isEmpty() == false)
                        { 
                            documentoSituacionPatrimonial.append("inversiones", documentoInversiones);
                        } 
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->inversiones: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        Document documentoAdeudos = new DeclaracionesDocumentoAdeudos(declaracion.getDeclaracion().getSituacionPatrimonial().getAdeudos(), declaracion.getMetadata().getTipo(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoAdeudos();
                        if (documentoAdeudos.isEmpty() == false)
                        { 
                            documentoSituacionPatrimonial.append("adeudos", documentoAdeudos);
                        } 
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->adeudos: " + e.getMessage());
                    } 
                    
                    try
                    { 
                        Document documentoPrestamoOComodato = new DeclaracionesDocumentoPrestamoOComodato(declaracion.getDeclaracion().getSituacionPatrimonial().getPrestamoOComodato(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoPrestamoOComodato();
                        if (documentoPrestamoOComodato.isEmpty() == false)
                        { 
                            documentoSituacionPatrimonial.append("prestamoOComodato", documentoPrestamoOComodato);
                        } 
                    } 
                    catch (Exception e)
                    { 
                        throw new Exception("Error en situacionPatrimonial->prestamoOComodato: " + e.getMessage());
                    } 
                    
                    documentoSituacionPatrimonialInteres.append("situacionPatrimonial", documentoSituacionPatrimonial);
                    
                    if (declaracion.getDeclaracion().getInteres() != null)
                    { 
                        
                        try
                        { 
                            Document documentoParticipacion = new DeclaracionesDocumentoParticipacion(declaracion.getDeclaracion().getInteres().getParticipacion(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoParticipacion();
                            if (documentoParticipacion.isEmpty() == false)
                            { 
                                documentoInteres.append("participacion", documentoParticipacion);
                            } 
                        } 
                        catch (Exception e)
                        { 
                            throw new Exception("Error en interes->participacion: " + e.getMessage());
                        } 
                        
                        try
                        { 
                            Document documentoParticipacionTomaDecisiones = new DeclaracionesDocumentoParticipacionTomaDecisiones(declaracion.getDeclaracion().getInteres().getParticipacionTomaDecisiones(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoParticipacionTomaDecisiones();
                            if (documentoParticipacionTomaDecisiones.isEmpty() == false)
                            { 
                                documentoInteres.append("participacionTomaDecisiones", documentoParticipacionTomaDecisiones);
                            } 
                        } 
                        catch (Exception e)
                        { 
                            throw new Exception("Error en interes->participacionTomaDecisiones: " + e.getMessage());
                        } 
                        
                        try
                        { 
                            Document documentoApoyos = new DeclaracionesDocumentoApoyos(declaracion.getDeclaracion().getInteres().getApoyos(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoApoyos();
                            if (documentoApoyos.isEmpty() == false)
                            { 
                                documentoInteres.append("apoyos", documentoApoyos);
                            } 
                        } 
                        catch (Exception e)
                        { 
                            throw new Exception("Error en interes->apoyos: " + e.getMessage());
                        } 
                        
                        try
                        { 
                            Document documentoRepresentacion = new DeclaracionesDocumentoRepresentacion(declaracion.getDeclaracion().getInteres().getRepresentacion(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoRepresentacion();
                            if (documentoRepresentacion.isEmpty() == false)
                            { 
                                documentoInteres.append("representacion", documentoRepresentacion);
                            } 
                        } 
                        catch (Exception e)
                        { 
                            throw new Exception("Error en interes->representacion: " + e.getMessage());
                        } 
                        
                        try
                        { 
                            Document documentoClientesPrincipales = new DeclaracionesDocumentoClientesPrincipales(declaracion.getDeclaracion().getInteres().getClientesPrincipales(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoClientesPrincipales();
                            if (documentoClientesPrincipales.isEmpty() == false)
                            { 
                                documentoInteres.append("clientesPrincipales", documentoClientesPrincipales);
                            } 
                        } 
                        catch (Exception e)
                        { 
                            throw new Exception("Error en interes->clientesPrincipales: " + e.getMessage());
                        } 
                        
                        try
                        { 
                            Document documentoBeneficiosPrivados = new DeclaracionesDocumentoBeneficiosPrivados(declaracion.getDeclaracion().getInteres().getBeneficiosPrivados(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoBeneficiosPrivados();
                            if (documentoBeneficiosPrivados.isEmpty() == false)
                            { 
                                documentoInteres.append("beneficiosPrivados", documentoBeneficiosPrivados);
                            } 
                        } 
                        catch (Exception e)
                        { 
                            throw new Exception("Error en interes->beneficiosPrivados: " + e.getMessage());
                        } 
                        
                        try
                        { 
                            Document documentoFideicomisos = new DeclaracionesDocumentoFideicomisos(declaracion.getDeclaracion().getInteres().getFideicomisos(), bolMostrarDatosPublicosYPrivados).obtenerDocumentoFideicomisos();
                            if (documentoFideicomisos.isEmpty() == false)
                            { 
                                documentoInteres.append("fideicomisos", documentoFideicomisos);
                            } 
                        } 
                        catch (Exception e)
                        { 
                            throw new Exception("Error en interes->fideicomisos: " + e.getMessage());
                        } 
                        
                        if (documentoInteres.isEmpty() == false)
                        { 
                            documentoSituacionPatrimonialInteres.append("interes", documentoInteres);
                        } 
                    } 
                    
                    documentoDeclaracion.append("declaracion", documentoSituacionPatrimonialInteres);
                    break;
                
                default:
                    break;
            } 

            listaDocumentosDeclaraciones.add(documentoDeclaracion);
            
        } 

        documentoDeclaracion = new Document();
        documentoDeclaracion.append("pagination",
                new Document("pageSize", atributosDePaginacion.getPageSize())
                        .append("page", atributosDePaginacion.getPage())
                        .append("totalRows", atributosDePaginacion.getTotalRows())
                        .append("hasNextPage", atributosDePaginacion.isHasNextPage()));
        

        documentoDeclaracion.append("results", listaDocumentosDeclaraciones);

        settings = JsonWriterSettings.builder().outputMode(JsonMode.RELAXED).build();

        return documentoDeclaracion.toJson(settings);
    } 
    

    /**
     * Convierte una cadena a una expresión regular
     *
     * @param palabra
     *
     * @return
     */
    @Override
    public String crearExpresionRegularDePalabra(String palabra)
    { 

        String expReg = "";
        char[] arregloDePalabras = palabra.toLowerCase().toCharArray();

        for (char letra : arregloDePalabras)
        { 
            switch (letra)
            {
                case 'a':
                    expReg += "[aáAÁ]";
                    break;
                case 'á':
                    expReg += "[aáAÁ]";
                    break;
                case 'e':
                    expReg += "[eéEÉ]";
                    break;
                case 'é':
                    expReg += "[eéEÉ]";
                    break;
                case 'i':
                    expReg += "[iíIÍ]";
                    break;
                case 'í':
                    expReg += "[iíIÍ]";
                    break;
                case 'o':
                    expReg += "[oóOÓ]";
                    break;
                case 'ó':
                    expReg += "[oóOÓ]";
                    break;
                case 'u':
                    expReg += "[uúUÚ]";
                    break;
                case 'ú':
                    expReg += "[uúUÚ]";
                    break;
                default:
                    expReg += letra;
                    break;
            }
        } 

        return expReg;
    } 

    /**
     * Valida RFC
     *
     * @param rfc RFC
     *
     * @return Falso o verdadero
     *
     * @throws Exception
     */
    @Override
    public boolean esValidoRFC(String rfc) throws Exception
    { 
        return DAOWs.esValidoRFC(rfc);
    } 
}
