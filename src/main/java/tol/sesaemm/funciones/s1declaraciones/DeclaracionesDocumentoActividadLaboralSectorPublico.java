package tol.sesaemm.funciones.s1declaraciones;

import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesActividadLaboralSectorPublicoDatosPareja;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesActividadLaboralSectorPublicoDependienteEconomico;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesDocumentoActividadLaboralSectorPublico
{

    private DeclaracionesActividadLaboralSectorPublicoDatosPareja sectorPublicoDatosPareja;
    private DeclaracionesActividadLaboralSectorPublicoDependienteEconomico sectorPublicoDependienteEconomico;
    private Document documentoActividadLaboralSectorPublico;
    private String strSeccion;

    public DeclaracionesDocumentoActividadLaboralSectorPublico(DeclaracionesActividadLaboralSectorPublicoDatosPareja sectorPublicoDatosPareja, DeclaracionesActividadLaboralSectorPublicoDependienteEconomico sectorPublicoDependienteEconomico, String strSeccion)
    {
        this.sectorPublicoDatosPareja = sectorPublicoDatosPareja;
        this.sectorPublicoDependienteEconomico = sectorPublicoDependienteEconomico;
        this.strSeccion = strSeccion;
    }

    public Document obtenerDocumentoActividadLaboralSectorPublico()
    {

        documentoActividadLaboralSectorPublico = new Document();

        if (strSeccion.equals("datosPareja"))
        {

            if (sectorPublicoDatosPareja.getNivelOrdenGobierno() != null)
            {
                documentoActividadLaboralSectorPublico.append("nivelOrdenGobierno", sectorPublicoDatosPareja.getNivelOrdenGobierno());
            }

            if (sectorPublicoDatosPareja.getAmbitoPublico() != null)
            {
                documentoActividadLaboralSectorPublico.append("ambitoPublico", sectorPublicoDatosPareja.getAmbitoPublico());
            }

            if (sectorPublicoDatosPareja.getNombreEntePublico() != null)
            {
                documentoActividadLaboralSectorPublico.append("nombreEntePublico", sectorPublicoDatosPareja.getNombreEntePublico());
            }

            if (sectorPublicoDatosPareja.getAreaAdscripcion() != null)
            {
                documentoActividadLaboralSectorPublico.append("areaAdscripcion", sectorPublicoDatosPareja.getAreaAdscripcion());
            }

            if (sectorPublicoDatosPareja.getEmpleoCargoComision() != null)
            {
                documentoActividadLaboralSectorPublico.append("empleoCargoComision", sectorPublicoDatosPareja.getEmpleoCargoComision());
            }

            if (sectorPublicoDatosPareja.getFuncionPrincipal() != null)
            {
                documentoActividadLaboralSectorPublico.append("funcionPrincipal", sectorPublicoDatosPareja.getFuncionPrincipal());
            }

            if (sectorPublicoDatosPareja.getSalarioMensualNeto() != null)
            {
                Document documentoSalarioMensualNeto = new Document();
                if (sectorPublicoDatosPareja.getSalarioMensualNeto().getValor() != null)
                {
                    documentoSalarioMensualNeto.append("valor", sectorPublicoDatosPareja.getSalarioMensualNeto().getValor());
                }
                if (sectorPublicoDatosPareja.getSalarioMensualNeto().getMoneda() != null)
                {
                    documentoSalarioMensualNeto.append("moneda", sectorPublicoDatosPareja.getSalarioMensualNeto().getMoneda());
                }
                if (documentoSalarioMensualNeto.isEmpty() == false)
                {
                    documentoActividadLaboralSectorPublico.append("salarioMensualNeto", documentoSalarioMensualNeto);
                }
            }

            if (sectorPublicoDatosPareja.getFechaIngreso() != null)
            {
                documentoActividadLaboralSectorPublico.append("fechaIngreso", sectorPublicoDatosPareja.getFechaIngreso());
            }

        } else if (strSeccion.equals("datosDependienteEconomico"))
        {

            if (sectorPublicoDependienteEconomico.getNivelOrdenGobierno() != null)
            {
                documentoActividadLaboralSectorPublico.append("nivelOrdenGobierno", sectorPublicoDependienteEconomico.getNivelOrdenGobierno());
            }

            if (sectorPublicoDependienteEconomico.getAmbitoPublico() != null)
            {
                documentoActividadLaboralSectorPublico.append("ambitoPublico", sectorPublicoDependienteEconomico.getAmbitoPublico());
            }

            if (sectorPublicoDependienteEconomico.getNombreEntePublico() != null)
            {
                documentoActividadLaboralSectorPublico.append("nombreEntePublico", sectorPublicoDependienteEconomico.getNombreEntePublico());
            }

            if (sectorPublicoDependienteEconomico.getAreaAdscripcion() != null)
            {
                documentoActividadLaboralSectorPublico.append("areaAdscripcion", sectorPublicoDependienteEconomico.getAreaAdscripcion());
            }

            if (sectorPublicoDependienteEconomico.getEmpleoCargoComision() != null)
            {
                documentoActividadLaboralSectorPublico.append("empleoCargoComision", sectorPublicoDependienteEconomico.getEmpleoCargoComision());
            }

            if (sectorPublicoDependienteEconomico.getFuncionPrincipal() != null)
            {
                documentoActividadLaboralSectorPublico.append("funcionPrincipal", sectorPublicoDependienteEconomico.getFuncionPrincipal());
            }

            if (sectorPublicoDependienteEconomico.getSalarioMensualNeto() != null)
            {
                Document documentoSalarioMensualNeto = new Document();
                if (sectorPublicoDependienteEconomico.getSalarioMensualNeto().getValor() != null)
                {
                    documentoSalarioMensualNeto.append("valor", sectorPublicoDependienteEconomico.getSalarioMensualNeto().getValor());
                }
                if (sectorPublicoDependienteEconomico.getSalarioMensualNeto().getMoneda() != null)
                {
                    documentoSalarioMensualNeto.append("moneda", sectorPublicoDependienteEconomico.getSalarioMensualNeto().getMoneda());
                }
                if (documentoSalarioMensualNeto.isEmpty() == false)
                {
                    documentoActividadLaboralSectorPublico.append("salarioMensualNeto", documentoSalarioMensualNeto);
                }
            }

            if (sectorPublicoDependienteEconomico.getFechaIngreso() != null)
            {
                documentoActividadLaboralSectorPublico.append("fechaIngreso", sectorPublicoDependienteEconomico.getFechaIngreso());
            }

        }

        return documentoActividadLaboralSectorPublico;

    }

}
