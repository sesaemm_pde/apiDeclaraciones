package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesBienesMuebles;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesDocumentoBienesMuebles
{

    private DeclaracionesBienesMuebles datosBienesMuebles;
    private boolean bolMostrarDatosPublicosYPrivados;
    private Document documentoBienesMuebles;

    public DeclaracionesDocumentoBienesMuebles(DeclaracionesBienesMuebles datosBienesMuebles, boolean bolMostrarDatosPublicosYPrivados) throws Exception
    {
        this.datosBienesMuebles = datosBienesMuebles;
        this.bolMostrarDatosPublicosYPrivados = bolMostrarDatosPublicosYPrivados;
    }

    public Document obtenerDocumentoBienesMuebles()
    {

        documentoBienesMuebles = new Document();

        if (datosBienesMuebles != null)
        {
            if (this.bolMostrarDatosPublicosYPrivados == false)
            {

                if (datosBienesMuebles.getNinguno() != null)
                {
                    documentoBienesMuebles.append("ninguno", datosBienesMuebles.getNinguno());
                }

                if (datosBienesMuebles.getBienMueble() != null)
                {
                    ArrayList<Document> arregloBienMueble = new DeclaracionesListaDocumentosBienesMuebles(datosBienesMuebles.getBienMueble(), this.bolMostrarDatosPublicosYPrivados).obtenerArregloDocumentosBienesMuebles();
                    if (arregloBienMueble.isEmpty() == false)
                    {
                        documentoBienesMuebles.append("bienMueble", arregloBienMueble);
                    }
                }

            } else
            {

                if (datosBienesMuebles.getNinguno() != null)
                {
                    documentoBienesMuebles.append("ninguno", datosBienesMuebles.getNinguno());
                }

                if (datosBienesMuebles.getBienMueble() != null)
                {
                    ArrayList<Document> arregloBienMueble = new DeclaracionesListaDocumentosBienesMuebles(datosBienesMuebles.getBienMueble(), this.bolMostrarDatosPublicosYPrivados).obtenerArregloDocumentosBienesMuebles();
                    if (arregloBienMueble.isEmpty() == false)
                    {
                        documentoBienesMuebles.append("bienMueble", arregloBienMueble);
                    }
                }

                if (datosBienesMuebles.getAclaracionesObservaciones() != null)
                {
                    documentoBienesMuebles.append("aclaracionesObservaciones", datosBienesMuebles.getAclaracionesObservaciones());
                }

            }
        }

        return documentoBienesMuebles;

    }

}
