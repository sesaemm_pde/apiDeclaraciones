package tol.sesaemm.funciones.s1declaraciones;

import com.google.gson.Gson;
import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesExperienciaSectorPublicoSectorPrivado;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesListaDocumentosExperienciaLaboral
{

    private ArrayList<DeclaracionesExperienciaSectorPublicoSectorPrivado> datosExperiencia;
    private ArrayList<Document> arregloListaExperienciaLaboral;

    public DeclaracionesListaDocumentosExperienciaLaboral(ArrayList<DeclaracionesExperienciaSectorPublicoSectorPrivado> datosExperiencia)
    {
        this.datosExperiencia = datosExperiencia;
    }

    public ArrayList<Document> obtenerArregloDocumentosExperienciaLaboral()
    {

        DeclaracionesExperienciaSectorPublicoSectorPrivado experienciaSectorPublicoSectorPrivado;

        arregloListaExperienciaLaboral = new ArrayList<>();

        for (int j = 0; j < datosExperiencia.size(); j++)
        {

            experienciaSectorPublicoSectorPrivado = new Gson().fromJson(new Gson().toJson(datosExperiencia.get(j)), DeclaracionesExperienciaSectorPublicoSectorPrivado.class);

            Document documentoExperienciaLaboral = new Document();

            if (experienciaSectorPublicoSectorPrivado.getAmbitoSector() != null)
            {
                if (experienciaSectorPublicoSectorPrivado.getAmbitoSector().getClave() != null)
                {

                    if (experienciaSectorPublicoSectorPrivado.getAmbitoSector().getClave().equals("PUB"))
                    {

                        if (experienciaSectorPublicoSectorPrivado.getTipoOperacion() != null)
                        {
                            documentoExperienciaLaboral.append("tipoOperacion", experienciaSectorPublicoSectorPrivado.getTipoOperacion());
                        }

                        Document documentoAmbitoSector = new Document();
                        documentoAmbitoSector.append("clave", experienciaSectorPublicoSectorPrivado.getAmbitoSector().getClave());
                        if (experienciaSectorPublicoSectorPrivado.getAmbitoSector().getValor() != null)
                        {
                            documentoAmbitoSector.append("valor", experienciaSectorPublicoSectorPrivado.getAmbitoSector().getValor());
                        }
                        if (documentoAmbitoSector.isEmpty() == false)
                        {
                            documentoExperienciaLaboral.append("ambitoSector", documentoAmbitoSector);
                        }

                        if (experienciaSectorPublicoSectorPrivado.getNivelOrdenGobierno() != null)
                        {
                            documentoExperienciaLaboral.append("nivelOrdenGobierno", experienciaSectorPublicoSectorPrivado.getNivelOrdenGobierno());
                        }

                        if (experienciaSectorPublicoSectorPrivado.getAmbitoPublico() != null)
                        {
                            documentoExperienciaLaboral.append("ambitoPublico", experienciaSectorPublicoSectorPrivado.getAmbitoPublico());
                        }

                        if (experienciaSectorPublicoSectorPrivado.getNombreEntePublico() != null)
                        {
                            documentoExperienciaLaboral.append("nombreEntePublico", experienciaSectorPublicoSectorPrivado.getNombreEntePublico());
                        }

                        if (experienciaSectorPublicoSectorPrivado.getAreaAdscripcion() != null)
                        {
                            documentoExperienciaLaboral.append("areaAdscripcion", experienciaSectorPublicoSectorPrivado.getAreaAdscripcion());
                        }

                        if (experienciaSectorPublicoSectorPrivado.getEmpleoCargoComision() != null)
                        {
                            documentoExperienciaLaboral.append("empleoCargoComision", experienciaSectorPublicoSectorPrivado.getEmpleoCargoComision());
                        }

                        if (experienciaSectorPublicoSectorPrivado.getFuncionPrincipal() != null)
                        {
                            documentoExperienciaLaboral.append("funcionPrincipal", experienciaSectorPublicoSectorPrivado.getFuncionPrincipal());
                        }

                        if (experienciaSectorPublicoSectorPrivado.getFechaIngreso() != null)
                        {
                            documentoExperienciaLaboral.append("fechaIngreso", experienciaSectorPublicoSectorPrivado.getFechaIngreso());
                        }

                        if (experienciaSectorPublicoSectorPrivado.getFechaEgreso() != null)
                        {
                            documentoExperienciaLaboral.append("fechaEgreso", experienciaSectorPublicoSectorPrivado.getFechaEgreso());
                        }

                        if (experienciaSectorPublicoSectorPrivado.getUbicacion() != null)
                        {
                            documentoExperienciaLaboral.append("ubicacion", experienciaSectorPublicoSectorPrivado.getUbicacion());
                        }

                        if (documentoExperienciaLaboral.isEmpty() == false)
                        {
                            arregloListaExperienciaLaboral.add(documentoExperienciaLaboral);
                        }
                    } else
                    {

                        if (experienciaSectorPublicoSectorPrivado.getTipoOperacion() != null)
                        {
                            documentoExperienciaLaboral.append("tipoOperacion", experienciaSectorPublicoSectorPrivado.getTipoOperacion());
                        }

                        Document documentoAmbitoSector = new Document();
                        documentoAmbitoSector.append("clave", experienciaSectorPublicoSectorPrivado.getAmbitoSector().getClave());
                        if (experienciaSectorPublicoSectorPrivado.getAmbitoSector().getValor() != null)
                        {
                            documentoAmbitoSector.append("valor", experienciaSectorPublicoSectorPrivado.getAmbitoSector().getValor());
                        }
                        if (documentoAmbitoSector.isEmpty() == false)
                        {
                            documentoExperienciaLaboral.append("ambitoSector", documentoAmbitoSector);
                        }

                        if (experienciaSectorPublicoSectorPrivado.getNombreEmpresaSociedadAsociacion() != null)
                        {
                            documentoExperienciaLaboral.append("nombreEmpresaSociedadAsociacion", experienciaSectorPublicoSectorPrivado.getNombreEmpresaSociedadAsociacion());
                        }

                        if (experienciaSectorPublicoSectorPrivado.getRfc() != null)
                        {
                            documentoExperienciaLaboral.append("rfc", experienciaSectorPublicoSectorPrivado.getRfc());
                        }

                        if (experienciaSectorPublicoSectorPrivado.getArea() != null)
                        {
                            documentoExperienciaLaboral.append("area", experienciaSectorPublicoSectorPrivado.getArea());
                        }

                        if (experienciaSectorPublicoSectorPrivado.getPuesto() != null)
                        {
                            documentoExperienciaLaboral.append("puesto", experienciaSectorPublicoSectorPrivado.getPuesto());
                        }

                        if (experienciaSectorPublicoSectorPrivado.getSector() != null)
                        {
                            Document documentoSector = new Document();
                            if (experienciaSectorPublicoSectorPrivado.getSector().getClave() != null)
                            {
                                documentoSector.append("clave", experienciaSectorPublicoSectorPrivado.getSector().getClave());
                            }
                            if (experienciaSectorPublicoSectorPrivado.getSector().getValor() != null)
                            {
                                documentoSector.append("valor", experienciaSectorPublicoSectorPrivado.getSector().getValor());
                            }
                            if (documentoSector.isEmpty() == false)
                            {
                                documentoExperienciaLaboral.append("sector", documentoSector);
                            }
                        }

                        if (experienciaSectorPublicoSectorPrivado.getFechaIngreso() != null)
                        {
                            documentoExperienciaLaboral.append("fechaIngreso", experienciaSectorPublicoSectorPrivado.getFechaIngreso());
                        }

                        if (experienciaSectorPublicoSectorPrivado.getFechaEgreso() != null)
                        {
                            documentoExperienciaLaboral.append("fechaEgreso", experienciaSectorPublicoSectorPrivado.getFechaEgreso());
                        }

                        if (experienciaSectorPublicoSectorPrivado.getUbicacion() != null)
                        {
                            documentoExperienciaLaboral.append("ubicacion", experienciaSectorPublicoSectorPrivado.getUbicacion());
                        }

                        if (documentoExperienciaLaboral.isEmpty() == false)
                        {
                            arregloListaExperienciaLaboral.add(documentoExperienciaLaboral);
                        }
                    }

                }
            }
        }

        return arregloListaExperienciaLaboral;
    }

}
