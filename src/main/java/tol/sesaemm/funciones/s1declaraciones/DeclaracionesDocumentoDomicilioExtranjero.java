package tol.sesaemm.funciones.s1declaraciones;

import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesDomicilioExtranjero;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesDocumentoDomicilioExtranjero
{

    private DeclaracionesDomicilioExtranjero domicilioExtranjero;
    private Document documentoDomicilioExtranjero;

    public DeclaracionesDocumentoDomicilioExtranjero(DeclaracionesDomicilioExtranjero domicilioExtranjero)
    {
        this.domicilioExtranjero = domicilioExtranjero;
    }

    public Document obtenerDocumentoDomicilioExtranjero()
    {

        documentoDomicilioExtranjero = new Document();

        if (domicilioExtranjero.getCalle() != null)
        {
            documentoDomicilioExtranjero.append("calle", domicilioExtranjero.getCalle());
        }

        if (domicilioExtranjero.getNumeroExterior() != null)
        {
            documentoDomicilioExtranjero.append("numeroExterior", domicilioExtranjero.getNumeroExterior());
        }

        if (domicilioExtranjero.getNumeroInterior() != null)
        {
            documentoDomicilioExtranjero.append("numeroInterior", domicilioExtranjero.getNumeroInterior());
        }

        if (domicilioExtranjero.getCiudadLocalidad() != null)
        {
            documentoDomicilioExtranjero.append("ciudadLocalidad", domicilioExtranjero.getCiudadLocalidad());
        }

        if (domicilioExtranjero.getEstadoProvincia() != null)
        {
            documentoDomicilioExtranjero.append("estadoProvincia", domicilioExtranjero.getEstadoProvincia());
        }

        if (domicilioExtranjero.getPais() != null)
        {
            documentoDomicilioExtranjero.append("pais", domicilioExtranjero.getPais());
        }

        if (domicilioExtranjero.getCodigoPostal() != null)
        {
            documentoDomicilioExtranjero.append("codigoPostal", domicilioExtranjero.getCodigoPostal());
        }

        return documentoDomicilioExtranjero;

    }
}
