package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesApoyos;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesDocumentoApoyos
{

    private DeclaracionesApoyos datosApoyos;
    private boolean bolMostrarDatosPublicosYPrivados;
    private Document documentoApoyos;

    public DeclaracionesDocumentoApoyos(DeclaracionesApoyos datosApoyos, boolean bolMostrarDatosPublicosYPrivados) throws Exception
    {
        this.datosApoyos = datosApoyos;
        this.bolMostrarDatosPublicosYPrivados = bolMostrarDatosPublicosYPrivados;
    }

    public Document obtenerDocumentoApoyos()
    {

        documentoApoyos = new Document();

        if (datosApoyos != null)
        {
            if (this.bolMostrarDatosPublicosYPrivados == false)
            {

                if (datosApoyos.getNinguno() != null)
                {
                    documentoApoyos.append("ninguno", datosApoyos.getNinguno());
                }

                if (datosApoyos.getApoyo() != null)
                {
                    ArrayList<Document> listaApoyos = new DeclaracionesListaDocumentosApoyos(datosApoyos.getApoyo(), this.bolMostrarDatosPublicosYPrivados).obtenerArregloDocumentosListaApoyos();
                    if (listaApoyos.isEmpty() == false)
                    {
                        documentoApoyos.append("apoyo", listaApoyos);
                    }
                }

            } else
            {

                if (datosApoyos.getNinguno() != null)
                {
                    documentoApoyos.append("ninguno", datosApoyos.getNinguno());
                }

                if (datosApoyos.getApoyo() != null)
                {
                    ArrayList<Document> listaApoyos = new DeclaracionesListaDocumentosApoyos(datosApoyos.getApoyo(), this.bolMostrarDatosPublicosYPrivados).obtenerArregloDocumentosListaApoyos();
                    if (listaApoyos.isEmpty() == false)
                    {
                        documentoApoyos.append("apoyo", listaApoyos);
                    }
                }

                if (datosApoyos.getAclaracionesObservaciones() != null)
                {
                    documentoApoyos.append("aclaracionesObservaciones", datosApoyos.getAclaracionesObservaciones());
                }

            }
        }

        return documentoApoyos;

    }

}
