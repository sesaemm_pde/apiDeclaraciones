package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import java.util.List;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesInversion;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesListaDocumentosInversiones
{

    private ArrayList<DeclaracionesInversion> inversion;                
    private boolean bolMostrarDatosPublicosYPrivados;                   
    private String strTipoDeclaracion;                                  
    private ArrayList<Document> arregloListaInversiones;                

    public DeclaracionesListaDocumentosInversiones(ArrayList<DeclaracionesInversion> inversion, String strTipoDeclaracion, boolean bolMostrarDatosPublicosYPrivados)
    {
        this.inversion = inversion;
        this.strTipoDeclaracion = strTipoDeclaracion;
        this.bolMostrarDatosPublicosYPrivados = bolMostrarDatosPublicosYPrivados;
    }
    
    public ArrayList<Document> obtenerArregloDocumentosListaInversiones()
    { 

        List<Document> arregloListaTitularBien;                                     
        List<Document> arregloListaTercero;                                         

        arregloListaInversiones = new ArrayList<>();

        if (this.bolMostrarDatosPublicosYPrivados == false)
        { 
            for (int j = 0; j < inversion.size(); j++)
            { 
                Document documentoInversion = new Document();
                if (inversion.get(j).getTitular() != null)
                {
                    if (inversion.get(j).getTitular().size() == 1)
                    {
                        if (inversion.get(j).getTitular().get(0).getClave() != null)
                        {
                            if (inversion.get(j).getTitular().get(0).getClave().equals("DEC") == true)
                            {
                                
                                if (inversion.get(j).getTipoOperacion() != null)
                                {
                                    documentoInversion.append("tipoOperacion", inversion.get(j).getTipoOperacion());
                                }
                                
                                if (inversion.get(j).getTipoInversion() != null)
                                { 
                                    Document documentoTipoInversion = new Document();
                                    
                                    if (inversion.get(j).getTipoInversion().getClave() != null)
                                    {
                                        documentoTipoInversion.append("clave", inversion.get(j).getTipoInversion().getClave());
                                    }
                                    
                                    if (inversion.get(j).getTipoInversion().getValor() != null)
                                    {
                                        documentoTipoInversion.append("valor", inversion.get(j).getTipoInversion().getValor());
                                    }
                                    
                                    if (documentoTipoInversion.isEmpty() == false)
                                    {
                                        documentoInversion.append("tipoInversion", documentoTipoInversion);
                                    }
                                } 
                                
                                if (inversion.get(j).getSubTipoInversion() != null)
                                { 
                                    Document documentoSubTipoInversion = new Document();
                                    
                                    if (inversion.get(j).getSubTipoInversion().getClave() != null)
                                    {
                                        documentoSubTipoInversion.append("clave", inversion.get(j).getSubTipoInversion().getClave());
                                    }
                                    
                                    if (inversion.get(j).getSubTipoInversion().getValor() != null)
                                    {
                                        documentoSubTipoInversion.append("valor", inversion.get(j).getSubTipoInversion().getValor());
                                    }
                                    
                                    if (documentoSubTipoInversion.isEmpty() == false)
                                    {
                                        documentoInversion.append("subTipoInversion", documentoSubTipoInversion);
                                    }
                                } 
                                
                                arregloListaTitularBien = new ArrayList<>();
                                Document documentoTitularBien = new Document();
                                
                                documentoTitularBien.append("clave", inversion.get(j).getTitular().get(0).getClave());
                                
                                if (inversion.get(j).getTitular().get(0).getValor() != null)
                                {
                                    documentoTitularBien.append("valor", inversion.get(j).getTitular().get(0).getValor());
                                }
                                
                                if (documentoTitularBien.isEmpty() == false)
                                {
                                    arregloListaTitularBien.add(documentoTitularBien);
                                    documentoInversion.append("titular", arregloListaTitularBien);
                                }
                                
                                if (inversion.get(j).getTercero() != null)
                                { 
                                    arregloListaTercero = new ArrayList<>();
                                    for (int l = 0; l < inversion.get(j).getTercero().size(); l++)
                                    { 
                                        Document documentoTercero = new Document();
                                        if (inversion.get(j).getTercero().get(l).getTipoPersona() != null)
                                        {
                                            if (inversion.get(j).getTercero().get(l).getTipoPersona().equals("MORAL"))
                                            {
                                                
                                                documentoTercero.append("tipoPersona", inversion.get(j).getTercero().get(l).getTipoPersona());
                                                
                                                if (inversion.get(j).getTercero().get(l).getNombreRazonSocial() != null)
                                                {
                                                    documentoTercero.append("nombreRazonSocial", inversion.get(j).getTercero().get(l).getNombreRazonSocial());
                                                }
                                                
                                                if (inversion.get(j).getTercero().get(l).getRfc() != null)
                                                {
                                                    documentoTercero.append("rfc", inversion.get(j).getTercero().get(l).getRfc());
                                                }
                                                
                                            }
                                        }
                                        if (documentoTercero.isEmpty() == false)
                                        {
                                            arregloListaTercero.add(documentoTercero);
                                        }
                                    } 
                                    if (arregloListaTercero.isEmpty() == false)
                                    {
                                        documentoInversion.append("tercero", arregloListaTercero);
                                    }
                                } 
                                
                                if (inversion.get(j).getLocalizacionInversion() != null)
                                { 
                                    Document documentoLocalizacionInversion = new Document();
                                    
                                    if (inversion.get(j).getLocalizacionInversion().getPais() != null)
                                    {
                                        documentoLocalizacionInversion.append("pais", inversion.get(j).getLocalizacionInversion().getPais());
                                    }
                                    
                                    if (inversion.get(j).getLocalizacionInversion().getInstitucionRazonSocial() != null)
                                    {
                                        documentoLocalizacionInversion.append("institucionRazonSocial", inversion.get(j).getLocalizacionInversion().getInstitucionRazonSocial());
                                    }
                                    
                                    if (inversion.get(j).getLocalizacionInversion().getRfc() != null)
                                    {
                                        documentoLocalizacionInversion.append("rfc", inversion.get(j).getLocalizacionInversion().getRfc());
                                    }
                                    
                                    if (documentoLocalizacionInversion.isEmpty() == false)
                                    {
                                        documentoInversion.append("localizacionInversion", documentoLocalizacionInversion);
                                    }
                                } 

                                if (strTipoDeclaracion.equals("INICIAL") == false)
                                { 
                                    if (inversion.get(j).getPorcentajeIncrementoDecremento() != null)
                                    {
                                        documentoInversion.append("porcentajeIncrementoDecremento", inversion.get(j).getPorcentajeIncrementoDecremento());
                                    }
                                } 
                                
                            }
                        }
                    }
                }
                if (documentoInversion.isEmpty() == false)
                {
                    arregloListaInversiones.add(documentoInversion);
                }
            } 
        } 
        else
        { 
            
            for (int j = 0; j < inversion.size(); j++)
            { 
                Document documentoInversion = new Document();
                
                if (inversion.get(j).getTipoOperacion() != null)
                {
                    documentoInversion.append("tipoOperacion", inversion.get(j).getTipoOperacion());
                }
                
                if (inversion.get(j).getTipoInversion() != null)
                { 
                    Document documentoTipoInversion = new Document();
                    
                    if (inversion.get(j).getTipoInversion().getClave() != null)
                    {
                        documentoTipoInversion.append("clave", inversion.get(j).getTipoInversion().getClave());
                    }
                    
                    if (inversion.get(j).getTipoInversion().getValor() != null)
                    {
                        documentoTipoInversion.append("valor", inversion.get(j).getTipoInversion().getValor());
                    }
                    
                    if (documentoTipoInversion.isEmpty() == false)
                    {
                        documentoInversion.append("tipoInversion", documentoTipoInversion);
                    }
                } 
                
                if (inversion.get(j).getSubTipoInversion() != null)
                { 
                    Document documentoSubTipoInversion = new Document();
                    
                    if (inversion.get(j).getSubTipoInversion().getClave() != null)
                    {
                        documentoSubTipoInversion.append("clave", inversion.get(j).getSubTipoInversion().getClave());
                    }
                    
                    if (inversion.get(j).getSubTipoInversion().getValor() != null)
                    {
                        documentoSubTipoInversion.append("valor", inversion.get(j).getSubTipoInversion().getValor());
                    }
                    
                    if (documentoSubTipoInversion.isEmpty() == false)
                    {
                        documentoInversion.append("subTipoInversion", documentoSubTipoInversion);
                    }
                } 
                
                if (inversion.get(j).getTitular() != null)
                { 
                    arregloListaTitularBien = new ArrayList<>();
                    for (int k = 0; k < inversion.get(j).getTitular().size(); k++)
                    { 
                        Document documentoTitularBien = new Document();
                        
                        if (inversion.get(j).getTitular().get(k).getClave() != null)
                        {
                            documentoTitularBien.append("clave", inversion.get(j).getTitular().get(k).getClave());
                        }
                        
                        if (inversion.get(j).getTitular().get(k).getValor() != null)
                        {
                            documentoTitularBien.append("valor", inversion.get(j).getTitular().get(k).getValor());
                        }
                        
                        if (documentoTitularBien.isEmpty() == false)
                        {
                            arregloListaTitularBien.add(documentoTitularBien);
                        }
                    } 
                    if (arregloListaTitularBien.isEmpty() == false)
                    { 
                        documentoInversion.append("titular", arregloListaTitularBien);
                    } 
                } 
                
                if (inversion.get(j).getTercero() != null)
                { 
                    arregloListaTercero = new ArrayList<>();
                    for (int l = 0; l < inversion.get(j).getTercero().size(); l++)
                    { 
                        Document documentoTercero = new Document();
                        
                        if (inversion.get(j).getTercero().get(l).getTipoPersona() != null)
                        {
                            documentoTercero.append("tipoPersona", inversion.get(j).getTercero().get(l).getTipoPersona());
                        }
                        
                        if (inversion.get(j).getTercero().get(l).getNombreRazonSocial() != null)
                        {
                            documentoTercero.append("nombreRazonSocial", inversion.get(j).getTercero().get(l).getNombreRazonSocial());
                        }
                        
                        if (inversion.get(j).getTercero().get(l).getRfc() != null)
                        {
                            documentoTercero.append("rfc", inversion.get(j).getTercero().get(l).getRfc());
                        }
                        
                        if (documentoTercero.isEmpty() == false)
                        {
                            arregloListaTercero.add(documentoTercero);
                        }
                    } 
                    if (arregloListaTercero.isEmpty() == false)
                    { 
                        documentoInversion.append("tercero", arregloListaTercero);
                    } 
                } 
                
                if (inversion.get(j).getNumeroCuentaContrato() != null)
                {
                    documentoInversion.append("numeroCuentaContrato", inversion.get(j).getNumeroCuentaContrato());
                }

                if (inversion.get(j).getLocalizacionInversion() != null)
                { 
                    Document documentoLocalizacionInversion = new Document();
                    
                    if (inversion.get(j).getLocalizacionInversion().getPais() != null)
                    {
                        documentoLocalizacionInversion.append("pais", inversion.get(j).getLocalizacionInversion().getPais());
                    }
                    
                    if (inversion.get(j).getLocalizacionInversion().getInstitucionRazonSocial() != null)
                    {
                        documentoLocalizacionInversion.append("institucionRazonSocial", inversion.get(j).getLocalizacionInversion().getInstitucionRazonSocial());
                    }
                    
                    if (inversion.get(j).getLocalizacionInversion().getRfc() != null)
                    {
                        documentoLocalizacionInversion.append("rfc", inversion.get(j).getLocalizacionInversion().getRfc());
                    }
                    
                    if (documentoLocalizacionInversion.isEmpty() == false)
                    {
                        documentoInversion.append("localizacionInversion", documentoLocalizacionInversion);
                    }
                } 
                
                switch (strTipoDeclaracion)
                { 
                    case "INICIAL":
                        
                        if (inversion.get(j).getSaldoSituacionActual() != null)
                        { 
                            Document documentoSaldoSituacionActual = new Document();
                            
                            if (inversion.get(j).getSaldoSituacionActual().getValor() != null)
                            {
                                documentoSaldoSituacionActual.append("valor", inversion.get(j).getSaldoSituacionActual().getValor());
                            }
                            
                            if (inversion.get(j).getSaldoSituacionActual().getMoneda() != null)
                            {
                                documentoSaldoSituacionActual.append("moneda", inversion.get(j).getSaldoSituacionActual().getMoneda());
                            }
                            
                            if (documentoSaldoSituacionActual.isEmpty() == false)
                            {
                                documentoInversion.append("saldoSituacionActual", documentoSaldoSituacionActual);
                            }
                        } 
                        
                        break;
                    case "MODIFICACIÓN":
                        
                        if (inversion.get(j).getSaldoDiciembreAnterior() != null)
                        { 
                            Document documentoSaldoDiciembreAnterior = new Document();
                            
                            if (inversion.get(j).getSaldoDiciembreAnterior().getValor() != null)
                            {
                                documentoSaldoDiciembreAnterior.append("valor", inversion.get(j).getSaldoDiciembreAnterior().getValor());
                            }
                            
                            if (inversion.get(j).getSaldoDiciembreAnterior().getMoneda() != null)
                            {
                                documentoSaldoDiciembreAnterior.append("moneda", inversion.get(j).getSaldoDiciembreAnterior().getMoneda());
                            }
                            
                            if (documentoSaldoDiciembreAnterior.isEmpty() == false)
                            {
                                documentoInversion.append("saldoDiciembreAnterior", documentoSaldoDiciembreAnterior);
                            }
                        } 
                        
                        if (inversion.get(j).getPorcentajeIncrementoDecremento() != null)
                        {
                            documentoInversion.append("porcentajeIncrementoDecremento", inversion.get(j).getPorcentajeIncrementoDecremento());
                        }
                        
                        break;
                    case "CONCLUSIÓN":
                        
                        if (inversion.get(j).getSaldoFechaConclusion() != null)
                        { 
                            Document documentoSaldoFechaConclusion = new Document();
                            
                            if (inversion.get(j).getSaldoFechaConclusion().getValor() != null)
                            {
                                documentoSaldoFechaConclusion.append("valor", inversion.get(j).getSaldoFechaConclusion().getValor());
                            }
                            
                            if (inversion.get(j).getSaldoFechaConclusion().getMoneda() != null)
                            {
                                documentoSaldoFechaConclusion.append("moneda", inversion.get(j).getSaldoFechaConclusion().getMoneda());
                            }
                            
                            if (documentoSaldoFechaConclusion.isEmpty() == false)
                            {
                                documentoInversion.append("saldoFechaConclusion", documentoSaldoFechaConclusion);
                            }
                        } 
                        
                        if (inversion.get(j).getPorcentajeIncrementoDecremento() != null)
                        {
                            documentoInversion.append("porcentajeIncrementoDecremento", inversion.get(j).getPorcentajeIncrementoDecremento());
                        }
                        
                        break;
                    default:
                        break;
                } 
                
                if (documentoInversion.isEmpty() == false)
                { 
                    arregloListaInversiones.add(documentoInversion);
                } 
            } 

        } 
        
        return arregloListaInversiones;

    } 
    
}
