package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesApoyoApoyos;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesListaDocumentosApoyos
{

    private ArrayList<DeclaracionesApoyoApoyos> apoyo;
    private boolean bolMostrarDatosPublicosYPrivados;
    private ArrayList<Document> arregloListaApoyos;

    public DeclaracionesListaDocumentosApoyos(ArrayList<DeclaracionesApoyoApoyos> apoyo, boolean bolMostrarDatosPublicosYPrivados)
    {
        this.apoyo = apoyo;
        this.bolMostrarDatosPublicosYPrivados = bolMostrarDatosPublicosYPrivados;
    }

    public ArrayList<Document> obtenerArregloDocumentosListaApoyos()
    {

        arregloListaApoyos = new ArrayList<>();

        if (this.bolMostrarDatosPublicosYPrivados == false)
        {
            for (int j = 0; j < apoyo.size(); j++)
            {
                Document documentoApoyo = new Document();

                if (apoyo.get(j).getTipoOperacion() != null)
                {
                    documentoApoyo.append("tipoOperacion", apoyo.get(j).getTipoOperacion());
                }

                if (apoyo.get(j).getTipoPersona() != null)
                {
                    documentoApoyo.append("tipoPersona", apoyo.get(j).getTipoPersona());
                }

                if (apoyo.get(j).getBeneficiarioPrograma() != null)
                {
                    Document documentoBeneficiarioPrograma = new Document();
                    if (apoyo.get(j).getBeneficiarioPrograma().getClave() != null)
                    {
                        if (apoyo.get(j).getBeneficiarioPrograma().getClave().equals("DC") || apoyo.get(j).getBeneficiarioPrograma().getClave().equals("OTRO"))
                        {

                            documentoBeneficiarioPrograma.append("clave", apoyo.get(j).getBeneficiarioPrograma().getClave());

                            if (apoyo.get(j).getBeneficiarioPrograma().getValor() != null)
                            {
                                documentoBeneficiarioPrograma.append("valor", apoyo.get(j).getBeneficiarioPrograma().getValor());
                            }

                        }
                    }
                    if (documentoBeneficiarioPrograma.isEmpty() == false)
                    {
                        documentoApoyo.append("beneficiarioPrograma", documentoBeneficiarioPrograma);
                    }
                }

                if (apoyo.get(j).getNombrePrograma() != null)
                {
                    documentoApoyo.append("nombrePrograma", apoyo.get(j).getNombrePrograma());
                }

                if (apoyo.get(j).getInstitucionOtorgante() != null)
                {
                    documentoApoyo.append("institucionOtorgante", apoyo.get(j).getInstitucionOtorgante());
                }

                if (apoyo.get(j).getNivelOrdenGobierno() != null)
                {
                    documentoApoyo.append("nivelOrdenGobierno", apoyo.get(j).getNivelOrdenGobierno());
                }

                if (apoyo.get(j).getTipoApoyo() != null)
                {
                    Document documentoTipoApoyo = new Document();

                    if (apoyo.get(j).getTipoApoyo().getClave() != null)
                    {
                        documentoTipoApoyo.append("clave", apoyo.get(j).getTipoApoyo().getClave());
                    }

                    if (apoyo.get(j).getTipoApoyo().getValor() != null)
                    {
                        documentoTipoApoyo.append("valor", apoyo.get(j).getTipoApoyo().getValor());
                    }

                    if (documentoTipoApoyo.isEmpty() == false)
                    {
                        documentoApoyo.append("tipoApoyo", documentoTipoApoyo);
                    }
                }

                if (apoyo.get(j).getFormaRecepcion() != null)
                {
                    documentoApoyo.append("formaRecepcion", apoyo.get(j).getFormaRecepcion());
                }

                if (apoyo.get(j).getMontoApoyoMensual() != null)
                {
                    Document documentoMontoApoyoMensual = new Document();

                    if (apoyo.get(j).getMontoApoyoMensual().getValor() != null)
                    {
                        documentoMontoApoyoMensual.append("valor", apoyo.get(j).getMontoApoyoMensual().getValor());
                    }

                    if (apoyo.get(j).getMontoApoyoMensual().getMoneda() != null)
                    {
                        documentoMontoApoyoMensual.append("moneda", apoyo.get(j).getMontoApoyoMensual().getMoneda());
                    }

                    if (documentoMontoApoyoMensual.isEmpty() == false)
                    {
                        documentoApoyo.append("montoApoyoMensual", documentoMontoApoyoMensual);
                    }
                }

                if (apoyo.get(j).getEspecifiqueApoyo() != null)
                {
                    documentoApoyo.append("especifiqueApoyo", apoyo.get(j).getEspecifiqueApoyo());
                }

                if (documentoApoyo.isEmpty() == false)
                {
                    arregloListaApoyos.add(documentoApoyo);
                }
            }
        } else
        {

            for (int j = 0; j < apoyo.size(); j++)
            {
                Document documentoApoyo = new Document();

                if (apoyo.get(j).getTipoOperacion() != null)
                {
                    documentoApoyo.append("tipoOperacion", apoyo.get(j).getTipoOperacion());
                }

                if (apoyo.get(j).getTipoPersona() != null)
                {
                    documentoApoyo.append("tipoPersona", apoyo.get(j).getTipoPersona());
                }

                if (apoyo.get(j).getBeneficiarioPrograma() != null)
                {
                    Document documentoBeneficiarioPrograma = new Document();

                    if (apoyo.get(j).getBeneficiarioPrograma().getClave() != null)
                    {
                        documentoBeneficiarioPrograma.append("clave", apoyo.get(j).getBeneficiarioPrograma().getClave());
                    }

                    if (apoyo.get(j).getBeneficiarioPrograma().getValor() != null)
                    {
                        documentoBeneficiarioPrograma.append("valor", apoyo.get(j).getBeneficiarioPrograma().getValor());
                    }

                    if (documentoBeneficiarioPrograma.isEmpty() == false)
                    {
                        documentoApoyo.append("beneficiarioPrograma", documentoBeneficiarioPrograma);
                    }
                }

                if (apoyo.get(j).getNombrePrograma() != null)
                {
                    documentoApoyo.append("nombrePrograma", apoyo.get(j).getNombrePrograma());
                }

                if (apoyo.get(j).getInstitucionOtorgante() != null)
                {
                    documentoApoyo.append("institucionOtorgante", apoyo.get(j).getInstitucionOtorgante());
                }

                if (apoyo.get(j).getNivelOrdenGobierno() != null)
                {
                    documentoApoyo.append("nivelOrdenGobierno", apoyo.get(j).getNivelOrdenGobierno());
                }

                if (apoyo.get(j).getTipoApoyo() != null)
                {
                    Document documentoTipoApoyo = new Document();

                    if (apoyo.get(j).getTipoApoyo().getClave() != null)
                    {
                        documentoTipoApoyo.append("clave", apoyo.get(j).getTipoApoyo().getClave());
                    }

                    if (apoyo.get(j).getTipoApoyo().getValor() != null)
                    {
                        documentoTipoApoyo.append("valor", apoyo.get(j).getTipoApoyo().getValor());
                    }

                    if (documentoTipoApoyo.isEmpty() == false)
                    {
                        documentoApoyo.append("tipoApoyo", documentoTipoApoyo);
                    }
                }

                if (apoyo.get(j).getFormaRecepcion() != null)
                {
                    documentoApoyo.append("formaRecepcion", apoyo.get(j).getFormaRecepcion());
                }

                if (apoyo.get(j).getMontoApoyoMensual() != null)
                {
                    Document documentoMontoApoyoMensual = new Document();

                    if (apoyo.get(j).getMontoApoyoMensual().getValor() != null)
                    {
                        documentoMontoApoyoMensual.append("valor", apoyo.get(j).getMontoApoyoMensual().getValor());
                    }

                    if (apoyo.get(j).getMontoApoyoMensual().getMoneda() != null)
                    {
                        documentoMontoApoyoMensual.append("moneda", apoyo.get(j).getMontoApoyoMensual().getMoneda());
                    }

                    if (documentoMontoApoyoMensual.isEmpty() == false)
                    {
                        documentoApoyo.append("montoApoyoMensual", documentoMontoApoyoMensual);
                    }
                }

                if (apoyo.get(j).getEspecifiqueApoyo() != null)
                {
                    documentoApoyo.append("especifiqueApoyo", apoyo.get(j).getEspecifiqueApoyo());
                }

                if (documentoApoyo.isEmpty() == false)
                {
                    arregloListaApoyos.add(documentoApoyo);
                }
            }

        }

        return arregloListaApoyos;

    }
}
