package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesClientesPrincipales;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesDocumentoClientesPrincipales
{

    private DeclaracionesClientesPrincipales clientesPrincipales;
    private boolean bolMostrarDatosPublicosYPrivados;
    private Document documentoClientesPrincipales;

    public DeclaracionesDocumentoClientesPrincipales(DeclaracionesClientesPrincipales clientesPrincipales, boolean bolMostrarDatosPublicosYPrivados) throws Exception
    {
        this.clientesPrincipales = clientesPrincipales;
        this.bolMostrarDatosPublicosYPrivados = bolMostrarDatosPublicosYPrivados;
    }

    public Document obtenerDocumentoClientesPrincipales()
    {

        documentoClientesPrincipales = new Document();

        if (clientesPrincipales != null)
        {
            if (this.bolMostrarDatosPublicosYPrivados == false)
            {

                if (clientesPrincipales.getNinguno() != null)
                {
                    documentoClientesPrincipales.append("ninguno", clientesPrincipales.getNinguno());
                }

                if (clientesPrincipales.getCliente() != null)
                {
                    ArrayList<Document> listaCliente = new DeclaracionesListaDocumentosClientesPrincipales(clientesPrincipales.getCliente(), this.bolMostrarDatosPublicosYPrivados).obtenerArregloDocumentosListaClientesPrincipales();
                    if (listaCliente.isEmpty() == false)
                    {
                        documentoClientesPrincipales.append("cliente", listaCliente);
                    }
                }

            } else
            {

                if (clientesPrincipales.getNinguno() != null)
                {
                    documentoClientesPrincipales.append("ninguno", clientesPrincipales.getNinguno());
                }

                if (clientesPrincipales.getCliente() != null)
                {
                    ArrayList<Document> listaCliente = new DeclaracionesListaDocumentosClientesPrincipales(clientesPrincipales.getCliente(), this.bolMostrarDatosPublicosYPrivados).obtenerArregloDocumentosListaClientesPrincipales();
                    if (listaCliente.isEmpty() == false)
                    {
                        documentoClientesPrincipales.append("cliente", listaCliente);
                    }
                }

                if (clientesPrincipales.getAclaracionesObservaciones() != null)
                {
                    documentoClientesPrincipales.append("aclaracionesObservaciones", clientesPrincipales.getAclaracionesObservaciones());
                }

            }
        }

        return documentoClientesPrincipales;

    }

}
