package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesEscolaridad;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesListaDocumentosEscolaridad
{

    private ArrayList<DeclaracionesEscolaridad> escolaridad;
    private ArrayList<Document> arregloListaEscolaridad;

    public DeclaracionesListaDocumentosEscolaridad(ArrayList<DeclaracionesEscolaridad> escolaridad)
    {
        this.escolaridad = escolaridad;
    }

    public ArrayList<Document> obtenerArregloDocumentosListaEscolaridad()
    {

        arregloListaEscolaridad = new ArrayList<>();

        for (int j = 0; j < escolaridad.size(); j++)
        {
            Document documentoEscolaridad = new Document();

            if (escolaridad.get(j).getTipoOperacion() != null)
            {
                documentoEscolaridad.append("tipoOperacion", escolaridad.get(j).getTipoOperacion());
            }

            if (escolaridad.get(j).getNivel() != null)
            {
                Document documentoNivel = new Document();
                if (escolaridad.get(j).getNivel().getClave() != null)
                {
                    documentoNivel.append("clave", escolaridad.get(j).getNivel().getClave());
                }
                if (escolaridad.get(j).getNivel().getValor() != null)
                {
                    documentoNivel.append("valor", escolaridad.get(j).getNivel().getValor());
                }
                if (documentoNivel.isEmpty() == false)
                {
                    documentoEscolaridad.append("nivel", documentoNivel);
                }
            }

            if (escolaridad.get(j).getInstitucionEducativa() != null)
            {
                Document documentoInstitucionEducativa = new Document();
                if (escolaridad.get(j).getInstitucionEducativa().getNombre() != null)
                {
                    documentoInstitucionEducativa.append("nombre", escolaridad.get(j).getInstitucionEducativa().getNombre());
                }
                if (escolaridad.get(j).getInstitucionEducativa().getUbicacion() != null)
                {
                    documentoInstitucionEducativa.append("ubicacion", escolaridad.get(j).getInstitucionEducativa().getUbicacion());
                }
                if (documentoInstitucionEducativa.isEmpty() == false)
                {
                    documentoEscolaridad.append("institucionEducativa", documentoInstitucionEducativa);
                }
            }

            if (escolaridad.get(j).getCarreraAreaConocimiento() != null)
            {
                documentoEscolaridad.append("carreraAreaConocimiento", escolaridad.get(j).getCarreraAreaConocimiento());
            }

            if (escolaridad.get(j).getEstatus() != null)
            {
                documentoEscolaridad.append("estatus", escolaridad.get(j).getEstatus());
            }

            if (escolaridad.get(j).getDocumentoObtenido() != null)
            {
                documentoEscolaridad.append("documentoObtenido", escolaridad.get(j).getDocumentoObtenido());
            }

            if (escolaridad.get(j).getFechaObtencion() != null)
            {
                documentoEscolaridad.append("fechaObtencion", escolaridad.get(j).getFechaObtencion());
            }

            if (documentoEscolaridad.isEmpty() == false)
            {
                arregloListaEscolaridad.add(documentoEscolaridad);
            }
        }

        return arregloListaEscolaridad;

    }

}
