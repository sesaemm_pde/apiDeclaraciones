package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesParticipacionParticipacion;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesListaDocumentosParticipacion
{

    private ArrayList<DeclaracionesParticipacionParticipacion> participacion;
    private boolean bolMostrarDatosPublicosYPrivados;
    private ArrayList<Document> arregloListaParticipacion;

    public DeclaracionesListaDocumentosParticipacion(ArrayList<DeclaracionesParticipacionParticipacion> participacion, boolean bolMostrarDatosPublicosYPrivados)
    {
        this.participacion = participacion;
        this.bolMostrarDatosPublicosYPrivados = bolMostrarDatosPublicosYPrivados;
    }

    public ArrayList<Document> obtenerArregloDocumentosListaParticipacion()
    {
        arregloListaParticipacion = new ArrayList<>();

        if (this.bolMostrarDatosPublicosYPrivados == false)
        {
            for (int j = 0; j < participacion.size(); j++)
            {
                Document documentoParticipacion = new Document();
                if (participacion.get(j).getTipoRelacion() != null)
                {
                    if (participacion.get(j).getTipoRelacion().equals("DECLARANTE"))
                    {

                        if (participacion.get(j).getTipoOperacion() != null)
                        {
                            documentoParticipacion.append("tipoOperacion", participacion.get(j).getTipoOperacion());
                        }

                        documentoParticipacion.append("tipoRelacion", participacion.get(j).getTipoRelacion());

                        if (participacion.get(j).getNombreEmpresaSociedadAsociacion() != null)
                        {
                            documentoParticipacion.append("nombreEmpresaSociedadAsociacion", participacion.get(j).getNombreEmpresaSociedadAsociacion());
                        }

                        if (participacion.get(j).getRfc() != null)
                        {
                            documentoParticipacion.append("rfc", participacion.get(j).getRfc());
                        }

                        if (participacion.get(j).getPorcentajeParticipacion() != null)
                        {
                            documentoParticipacion.append("porcentajeParticipacion", participacion.get(j).getPorcentajeParticipacion());
                        }

                        if (participacion.get(j).getTipoParticipacion() != null)
                        {
                            Document documentoTipoParticipacion = new Document();

                            if (participacion.get(j).getTipoParticipacion().getClave() != null)
                            {
                                documentoTipoParticipacion.append("clave", participacion.get(j).getTipoParticipacion().getClave());
                            }

                            if (participacion.get(j).getTipoParticipacion().getValor() != null)
                            {
                                documentoTipoParticipacion.append("valor", participacion.get(j).getTipoParticipacion().getValor());
                            }

                            if (documentoTipoParticipacion.isEmpty() == false)
                            {
                                documentoParticipacion.append("tipoParticipacion", documentoTipoParticipacion);
                            }
                        }

                        if (participacion.get(j).getRecibeRemuneracion() != null)
                        {
                            documentoParticipacion.append("recibeRemuneracion", participacion.get(j).getRecibeRemuneracion());
                        }

                        if (participacion.get(j).getMontoMensual() != null)
                        {
                            Document documentoMontoMensual = new Document();

                            if (participacion.get(j).getMontoMensual().getValor() != null)
                            {
                                documentoMontoMensual.append("valor", participacion.get(j).getMontoMensual().getValor());
                            }

                            if (participacion.get(j).getMontoMensual().getMoneda() != null)
                            {
                                documentoMontoMensual.append("moneda", participacion.get(j).getMontoMensual().getMoneda());
                            }

                            if (documentoMontoMensual.isEmpty() == false)
                            {
                                documentoParticipacion.append("montoMensual", documentoMontoMensual);
                            }
                        }

                        if (participacion.get(j).getUbicacion() != null)
                        {
                            Document documentoUbicacion = new Document();

                            if (participacion.get(j).getUbicacion().getPais() != null)
                            {
                                documentoUbicacion.append("pais", participacion.get(j).getUbicacion().getPais());
                            }

                            if (participacion.get(j).getUbicacion().getEntidadFederativa() != null)
                            {
                                Document documentoEntidadFederativa = new Document();

                                if (participacion.get(j).getUbicacion().getEntidadFederativa().getClave() != null)
                                {
                                    documentoEntidadFederativa.append("clave", participacion.get(j).getUbicacion().getEntidadFederativa().getClave());
                                }

                                if (participacion.get(j).getUbicacion().getEntidadFederativa().getValor() != null)
                                {
                                    documentoEntidadFederativa.append("valor", participacion.get(j).getUbicacion().getEntidadFederativa().getValor());
                                }

                                if (documentoEntidadFederativa.isEmpty() == false)
                                {
                                    documentoUbicacion.append("entidadFederativa", documentoEntidadFederativa);
                                }
                            }

                            if (documentoUbicacion.isEmpty() == false)
                            {
                                documentoParticipacion.append("ubicacion", documentoUbicacion);
                            }
                        }

                        if (participacion.get(j).getSector() != null)
                        {
                            Document documentoSector = new Document();

                            if (participacion.get(j).getSector().getClave() != null)
                            {
                                documentoSector.append("clave", participacion.get(j).getSector().getClave());
                            }

                            if (participacion.get(j).getSector().getValor() != null)
                            {
                                documentoSector.append("valor", participacion.get(j).getSector().getValor());
                            }

                            if (documentoSector.isEmpty() == false)
                            {
                                documentoParticipacion.append("sector", documentoSector);
                            }
                        }

                    }
                }
                if (documentoParticipacion.isEmpty() == false)
                {
                    arregloListaParticipacion.add(documentoParticipacion);
                }
            }
        } else
        {

            for (int j = 0; j < participacion.size(); j++)
            {
                Document documentoParticipacion = new Document();

                if (participacion.get(j).getTipoOperacion() != null)
                {
                    documentoParticipacion.append("tipoOperacion", participacion.get(j).getTipoOperacion());
                }

                if (participacion.get(j).getTipoRelacion() != null)
                {
                    documentoParticipacion.append("tipoRelacion", participacion.get(j).getTipoRelacion());
                }

                if (participacion.get(j).getNombreEmpresaSociedadAsociacion() != null)
                {
                    documentoParticipacion.append("nombreEmpresaSociedadAsociacion", participacion.get(j).getNombreEmpresaSociedadAsociacion());
                }

                if (participacion.get(j).getRfc() != null)
                {
                    documentoParticipacion.append("rfc", participacion.get(j).getRfc());
                }

                if (participacion.get(j).getPorcentajeParticipacion() != null)
                {
                    documentoParticipacion.append("porcentajeParticipacion", participacion.get(j).getPorcentajeParticipacion());
                }

                if (participacion.get(j).getTipoParticipacion() != null)
                {
                    Document documentoTipoParticipacion = new Document();

                    if (participacion.get(j).getTipoParticipacion().getClave() != null)
                    {
                        documentoTipoParticipacion.append("clave", participacion.get(j).getTipoParticipacion().getClave());
                    }

                    if (participacion.get(j).getTipoParticipacion().getValor() != null)
                    {
                        documentoTipoParticipacion.append("valor", participacion.get(j).getTipoParticipacion().getValor());
                    }

                    if (documentoTipoParticipacion.isEmpty() == false)
                    {
                        documentoParticipacion.append("tipoParticipacion", documentoTipoParticipacion);
                    }
                }

                if (participacion.get(j).getRecibeRemuneracion() != null)
                {
                    documentoParticipacion.append("recibeRemuneracion", participacion.get(j).getRecibeRemuneracion());
                }

                if (participacion.get(j).getMontoMensual() != null)
                {
                    Document documentoMontoMensual = new Document();

                    if (participacion.get(j).getMontoMensual().getValor() != null)
                    {
                        documentoMontoMensual.append("valor", participacion.get(j).getMontoMensual().getValor());
                    }

                    if (participacion.get(j).getMontoMensual().getMoneda() != null)
                    {
                        documentoMontoMensual.append("moneda", participacion.get(j).getMontoMensual().getMoneda());
                    }

                    if (documentoMontoMensual.isEmpty() == false)
                    {
                        documentoParticipacion.append("montoMensual", documentoMontoMensual);
                    }
                }

                if (participacion.get(j).getUbicacion() != null)
                {
                    Document documentoUbicacion = new Document();

                    if (participacion.get(j).getUbicacion().getPais() != null)
                    {
                        documentoUbicacion.append("pais", participacion.get(j).getUbicacion().getPais());
                    }

                    if (participacion.get(j).getUbicacion().getEntidadFederativa() != null)
                    {
                        Document documentoEntidadFederativa = new Document();

                        if (participacion.get(j).getUbicacion().getEntidadFederativa().getClave() != null)
                        {
                            documentoEntidadFederativa.append("clave", participacion.get(j).getUbicacion().getEntidadFederativa().getClave());
                        }

                        if (participacion.get(j).getUbicacion().getEntidadFederativa().getValor() != null)
                        {
                            documentoEntidadFederativa.append("valor", participacion.get(j).getUbicacion().getEntidadFederativa().getValor());
                        }

                        if (documentoEntidadFederativa.isEmpty() == false)
                        {
                            documentoUbicacion.append("entidadFederativa", documentoEntidadFederativa);
                        }
                    }

                    if (documentoUbicacion.isEmpty() == false)
                    {
                        documentoParticipacion.append("ubicacion", documentoUbicacion);
                    }
                }

                if (participacion.get(j).getSector() != null)
                {
                    Document documentoSector = new Document();

                    if (participacion.get(j).getSector().getClave() != null)
                    {
                        documentoSector.append("clave", participacion.get(j).getSector().getClave());
                    }

                    if (participacion.get(j).getSector().getValor() != null)
                    {
                        documentoSector.append("valor", participacion.get(j).getSector().getValor());
                    }

                    if (documentoSector.isEmpty() == false)
                    {
                        documentoParticipacion.append("sector", documentoSector);
                    }
                }

                if (documentoParticipacion.isEmpty() == false)
                {
                    arregloListaParticipacion.add(documentoParticipacion);
                }
            }

        }

        return arregloListaParticipacion;

    }

}
