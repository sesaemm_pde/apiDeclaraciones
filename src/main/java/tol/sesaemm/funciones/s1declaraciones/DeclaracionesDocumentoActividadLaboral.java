/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s1declaraciones;

import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesActividadLaboral;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesDocumentoActividadLaboral
{

    private DeclaracionesActividadLaboral actividadLaboral;
    private Document documentoActividadLaboral;

    public DeclaracionesDocumentoActividadLaboral(DeclaracionesActividadLaboral actividadLaboral)
    {
        this.actividadLaboral = actividadLaboral;
    }

    public Document obtenerDocumentoActividadLaboral()
    {

        documentoActividadLaboral = new Document();

        if (actividadLaboral.getClave() != null)
        {
            documentoActividadLaboral.append("clave", actividadLaboral.getClave());
        }

        if (actividadLaboral.getValor() != null)
        {
            documentoActividadLaboral.append("valor", actividadLaboral.getValor());
        }

        return documentoActividadLaboral;
    }

}
