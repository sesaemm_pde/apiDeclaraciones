package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesPrestamo;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesListaDocumentosPrestamoOComodato
{

    private ArrayList<DeclaracionesPrestamo> prestamo;
    private boolean bolMostrarDatosPublicosYPrivados;
    private ArrayList<Document> arregloListaPrestamoOComodato;

    public DeclaracionesListaDocumentosPrestamoOComodato(ArrayList<DeclaracionesPrestamo> prestamo, boolean bolMostrarDatosPublicosYPrivados)
    {
        this.prestamo = prestamo;
        this.bolMostrarDatosPublicosYPrivados = bolMostrarDatosPublicosYPrivados;
    }

    public ArrayList<Document> obtenerArregloDocumentosListaPrestamoOComodato()
    {

        arregloListaPrestamoOComodato = new ArrayList<>();

        if (this.bolMostrarDatosPublicosYPrivados == false)
        {
            for (int j = 0; j < prestamo.size(); j++)
            {
                Document documentoPrestamo = new Document();

                if (prestamo.get(j).getTipoOperacion() != null)
                {
                    documentoPrestamo.append("tipoOperacion", prestamo.get(j).getTipoOperacion());
                }

                if (prestamo.get(j).getTipoBien() != null)
                {
                    Document documentoTipoBien = new Document();

                    if (prestamo.get(j).getTipoBien().getInmueble() != null)
                    {
                        Document documentoInmueble = new Document();

                        if (prestamo.get(j).getTipoBien().getInmueble().getTipoInmueble() != null)
                        {
                            Document documentoTipoInmueble = new Document();

                            if (prestamo.get(j).getTipoBien().getInmueble().getTipoInmueble().getClave() != null)
                            {
                                documentoTipoInmueble.append("clave", prestamo.get(j).getTipoBien().getInmueble().getTipoInmueble().getClave());
                            }

                            if (prestamo.get(j).getTipoBien().getInmueble().getTipoInmueble().getValor() != null)
                            {
                                documentoTipoInmueble.append("valor", prestamo.get(j).getTipoBien().getInmueble().getTipoInmueble().getValor());
                            }

                            if (documentoTipoInmueble.isEmpty() == false)
                            {
                                documentoInmueble.append("tipoInmueble", documentoTipoInmueble);
                            }
                        }

                        if (documentoInmueble.isEmpty() == false)
                        {
                            documentoTipoBien.append("inmueble", documentoInmueble);
                        }
                    }

                    if (prestamo.get(j).getTipoBien().getVehiculo() != null)
                    {
                        Document documentoVehiculo = new Document();

                        if (prestamo.get(j).getTipoBien().getVehiculo().getTipo() != null)
                        {
                            Document documentoTipo = new Document();

                            if (prestamo.get(j).getTipoBien().getVehiculo().getTipo().getClave() != null)
                            {
                                documentoTipo.append("clave", prestamo.get(j).getTipoBien().getVehiculo().getTipo().getClave());
                            }

                            if (prestamo.get(j).getTipoBien().getVehiculo().getTipo().getValor() != null)
                            {
                                documentoTipo.append("valor", prestamo.get(j).getTipoBien().getVehiculo().getTipo().getValor());
                            }

                            if (documentoTipo.isEmpty() == false)
                            {
                                documentoVehiculo.append("tipo", documentoTipo);
                            }
                        }

                        if (prestamo.get(j).getTipoBien().getVehiculo().getMarca() != null)
                        {
                            documentoVehiculo.append("marca", prestamo.get(j).getTipoBien().getVehiculo().getMarca());
                        }

                        if (prestamo.get(j).getTipoBien().getVehiculo().getModelo() != null)
                        {
                            documentoVehiculo.append("modelo", prestamo.get(j).getTipoBien().getVehiculo().getModelo());
                        }

                        if (prestamo.get(j).getTipoBien().getVehiculo().getAnio() != null)
                        {
                            documentoVehiculo.append("anio", prestamo.get(j).getTipoBien().getVehiculo().getAnio());
                        }

                        if (documentoVehiculo.isEmpty() == false)
                        {
                            documentoTipoBien.append("vehiculo", documentoVehiculo);
                        }
                    }

                    if (documentoTipoBien.isEmpty() == false)
                    {
                        documentoPrestamo.append("tipoBien", documentoTipoBien);
                    }
                }

                if (prestamo.get(j).getDuenoTitular() != null)
                {
                    Document documentoDuenoTitular = new Document();
                    if (prestamo.get(j).getDuenoTitular().getTipoDuenoTitular() != null)
                    {
                        if (prestamo.get(j).getDuenoTitular().getTipoDuenoTitular().equals("MORAL"))
                        {

                            documentoDuenoTitular.append("tipoDuenoTitular", prestamo.get(j).getDuenoTitular().getTipoDuenoTitular());

                            if (prestamo.get(j).getDuenoTitular().getNombreTitular() != null)
                            {
                                documentoDuenoTitular.append("nombreTitular", prestamo.get(j).getDuenoTitular().getNombreTitular());
                            }

                            if (prestamo.get(j).getDuenoTitular().getRfc() != null)
                            {
                                documentoDuenoTitular.append("rfc", prestamo.get(j).getDuenoTitular().getRfc());
                            }

                            if (prestamo.get(j).getDuenoTitular().getRelacionConTitular() != null)
                            {
                                documentoDuenoTitular.append("relacionConTitular", prestamo.get(j).getDuenoTitular().getRelacionConTitular());
                            }

                        }
                    }
                    if (documentoDuenoTitular.isEmpty() == false)
                    {
                        documentoPrestamo.append("duenoTitular", documentoDuenoTitular);
                    }
                }

                if (documentoPrestamo.isEmpty() == false)
                {
                    arregloListaPrestamoOComodato.add(documentoPrestamo);
                }
            }
        } else
        {

            for (int j = 0; j < prestamo.size(); j++)
            {
                Document documentoPrestamo = new Document();

                if (prestamo.get(j).getTipoOperacion() != null)
                {
                    documentoPrestamo.append("tipoOperacion", prestamo.get(j).getTipoOperacion());
                }

                if (prestamo.get(j).getTipoBien() != null)
                {
                    Document documentoTipoBien = new Document();

                    if (prestamo.get(j).getTipoBien().getInmueble() != null)
                    {
                        Document documentoInmueble = new Document();

                        if (prestamo.get(j).getTipoBien().getInmueble().getTipoInmueble() != null)
                        {
                            Document documentoTipoInmueble = new Document();

                            if (prestamo.get(j).getTipoBien().getInmueble().getTipoInmueble().getClave() != null)
                            {
                                documentoTipoInmueble.append("clave", prestamo.get(j).getTipoBien().getInmueble().getTipoInmueble().getClave());
                            }

                            if (prestamo.get(j).getTipoBien().getInmueble().getTipoInmueble().getValor() != null)
                            {
                                documentoTipoInmueble.append("valor", prestamo.get(j).getTipoBien().getInmueble().getTipoInmueble().getValor());
                            }

                            if (documentoTipoInmueble.isEmpty() == false)
                            {
                                documentoInmueble.append("tipoInmueble", documentoTipoInmueble);
                            }
                        }

                        if (prestamo.get(j).getTipoBien().getInmueble().getDomicilioMexico() != null)
                        {
                            Document documentoDomicilioMexico = new DeclaracionesDocumentoDomicilioMexico(prestamo.get(j).getTipoBien().getInmueble().getDomicilioMexico()).obtenerDocumentoDomicilioMexico();
                            if (documentoDomicilioMexico.isEmpty() == false)
                            {
                                documentoInmueble.append("domicilioMexico", documentoDomicilioMexico);
                            }
                        }

                        if (prestamo.get(j).getTipoBien().getInmueble().getDomicilioExtranjero() != null)
                        {
                            Document documentoDomicilioExtranjero = new DeclaracionesDocumentoDomicilioExtranjero(prestamo.get(j).getTipoBien().getInmueble().getDomicilioExtranjero()).obtenerDocumentoDomicilioExtranjero();
                            if (documentoDomicilioExtranjero.isEmpty() == false)
                            {
                                documentoInmueble.append("domicilioExtranjero", documentoDomicilioExtranjero);
                            }
                        }

                        if (documentoInmueble.isEmpty() == false)
                        {
                            documentoTipoBien.append("inmueble", documentoInmueble);
                        }
                    }

                    if (prestamo.get(j).getTipoBien().getVehiculo() != null)
                    {
                        Document documentoVehiculo = new Document();

                        if (prestamo.get(j).getTipoBien().getVehiculo().getTipo() != null)
                        {
                            Document documentoTipo = new Document();

                            if (prestamo.get(j).getTipoBien().getVehiculo().getTipo().getClave() != null)
                            {
                                documentoTipo.append("clave", prestamo.get(j).getTipoBien().getVehiculo().getTipo().getClave());
                            }

                            if (prestamo.get(j).getTipoBien().getVehiculo().getTipo().getValor() != null)
                            {
                                documentoTipo.append("valor", prestamo.get(j).getTipoBien().getVehiculo().getTipo().getValor());
                            }

                            if (documentoTipo.isEmpty() == false)
                            {
                                documentoVehiculo.append("tipo", documentoTipo);
                            }
                        }

                        if (prestamo.get(j).getTipoBien().getVehiculo().getMarca() != null)
                        {
                            documentoVehiculo.append("marca", prestamo.get(j).getTipoBien().getVehiculo().getMarca());
                        }

                        if (prestamo.get(j).getTipoBien().getVehiculo().getModelo() != null)
                        {
                            documentoVehiculo.append("modelo", prestamo.get(j).getTipoBien().getVehiculo().getModelo());
                        }

                        if (prestamo.get(j).getTipoBien().getVehiculo().getAnio() != null)
                        {
                            documentoVehiculo.append("anio", prestamo.get(j).getTipoBien().getVehiculo().getAnio());
                        }

                        if (prestamo.get(j).getTipoBien().getVehiculo().getNumeroSerieRegistro() != null)
                        {
                            documentoVehiculo.append("numeroSerieRegistro", prestamo.get(j).getTipoBien().getVehiculo().getNumeroSerieRegistro());
                        }

                        if (prestamo.get(j).getTipoBien().getVehiculo().getLugarRegistro() != null)
                        {
                            Document documentoLugarRegistro = new Document();

                            if (prestamo.get(j).getTipoBien().getVehiculo().getLugarRegistro().getPais() != null)
                            {
                                documentoLugarRegistro.append("pais", prestamo.get(j).getTipoBien().getVehiculo().getLugarRegistro().getPais());
                            }

                            if (prestamo.get(j).getTipoBien().getVehiculo().getLugarRegistro().getEntidadFederativa() != null)
                            {
                                Document documentoEntidadFederativa = new Document();

                                if (prestamo.get(j).getTipoBien().getVehiculo().getLugarRegistro().getEntidadFederativa().getClave() != null)
                                {
                                    documentoEntidadFederativa.append("clave", prestamo.get(j).getTipoBien().getVehiculo().getLugarRegistro().getEntidadFederativa().getClave());
                                }

                                if (prestamo.get(j).getTipoBien().getVehiculo().getLugarRegistro().getEntidadFederativa().getValor() != null)
                                {
                                    documentoEntidadFederativa.append("valor", prestamo.get(j).getTipoBien().getVehiculo().getLugarRegistro().getEntidadFederativa().getValor());
                                }

                                if (documentoEntidadFederativa.isEmpty() == false)
                                {
                                    documentoLugarRegistro.append("entidadFederativa", documentoEntidadFederativa);
                                }
                            }

                            if (documentoLugarRegistro.isEmpty() == false)
                            {
                                documentoVehiculo.append("lugarRegistro", documentoLugarRegistro);
                            }
                        }

                        if (documentoVehiculo.isEmpty() == false)
                        {
                            documentoTipoBien.append("vehiculo", documentoVehiculo);
                        }
                    }

                    if (documentoTipoBien.isEmpty() == false)
                    {
                        documentoPrestamo.append("tipoBien", documentoTipoBien);
                    }
                }

                if (prestamo.get(j).getDuenoTitular() != null)
                {
                    Document documentoDuenoTitular = new Document();

                    if (prestamo.get(j).getDuenoTitular().getTipoDuenoTitular() != null)
                    {
                        documentoDuenoTitular.append("tipoDuenoTitular", prestamo.get(j).getDuenoTitular().getTipoDuenoTitular());
                    }

                    if (prestamo.get(j).getDuenoTitular().getNombreTitular() != null)
                    {
                        documentoDuenoTitular.append("nombreTitular", prestamo.get(j).getDuenoTitular().getNombreTitular());
                    }

                    if (prestamo.get(j).getDuenoTitular().getRfc() != null)
                    {
                        documentoDuenoTitular.append("rfc", prestamo.get(j).getDuenoTitular().getRfc());
                    }

                    if (prestamo.get(j).getDuenoTitular().getRelacionConTitular() != null)
                    {
                        documentoDuenoTitular.append("relacionConTitular", prestamo.get(j).getDuenoTitular().getRelacionConTitular());
                    }

                    if (documentoDuenoTitular.isEmpty() == false)
                    {
                        documentoPrestamo.append("duenoTitular", documentoDuenoTitular);
                    }
                }

                if (documentoPrestamo.isEmpty() == false)
                {
                    arregloListaPrestamoOComodato.add(documentoPrestamo);
                }
            }

        }

        return arregloListaPrestamoOComodato;

    }

}
