package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesExperienciaLaboral;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesDocumentoExperienciaLaboral
{

    private DeclaracionesExperienciaLaboral datosExperienciaLaboral;
    private boolean bolMostrarDatosPublicosYPrivados;
    private Document documentoExperienciaLaboral;

    public DeclaracionesDocumentoExperienciaLaboral(DeclaracionesExperienciaLaboral datosExperienciaLaboral, boolean bolMostrarDatosPublicosYPrivados) throws Exception
    {
        this.datosExperienciaLaboral = datosExperienciaLaboral;
        this.bolMostrarDatosPublicosYPrivados = bolMostrarDatosPublicosYPrivados;
    }

    public Document obtenerDocumentoExperienciaLaboral()
    {

        documentoExperienciaLaboral = new Document();

        if (this.bolMostrarDatosPublicosYPrivados == false)
        {

            documentoExperienciaLaboral.append("ninguno", datosExperienciaLaboral.getNinguno());

            if (datosExperienciaLaboral.getExperiencia() != null)
            {
                ArrayList<Document> arregloListaExperienciaLaboral = new DeclaracionesListaDocumentosExperienciaLaboral(datosExperienciaLaboral.getExperiencia()).obtenerArregloDocumentosExperienciaLaboral();
                if (arregloListaExperienciaLaboral.isEmpty() == false)
                {
                    documentoExperienciaLaboral.append("experiencia", arregloListaExperienciaLaboral);
                }
            }

        } else
        {

            documentoExperienciaLaboral.append("ninguno", datosExperienciaLaboral.getNinguno());

            if (datosExperienciaLaboral.getExperiencia() != null)
            {
                ArrayList<Document> arregloListaExperienciaLaboral = new DeclaracionesListaDocumentosExperienciaLaboral(datosExperienciaLaboral.getExperiencia()).obtenerArregloDocumentosExperienciaLaboral();
                if (arregloListaExperienciaLaboral.isEmpty() == false)
                {
                    documentoExperienciaLaboral.append("experiencia", arregloListaExperienciaLaboral);
                }
            }

            if (datosExperienciaLaboral.getAclaracionesObservaciones() != null)
            {
                documentoExperienciaLaboral.append("aclaracionesObservaciones", datosExperienciaLaboral.getAclaracionesObservaciones());
            }

        }

        return documentoExperienciaLaboral;

    }

}
