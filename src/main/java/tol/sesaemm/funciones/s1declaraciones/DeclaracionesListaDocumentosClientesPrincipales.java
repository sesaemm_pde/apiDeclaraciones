package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesCliente;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesListaDocumentosClientesPrincipales
{

    private ArrayList<DeclaracionesCliente> cliente;
    private boolean bolMostrarDatosPublicosYPrivados;
    private ArrayList<Document> arregloListaClientesPrincipales;

    public DeclaracionesListaDocumentosClientesPrincipales(ArrayList<DeclaracionesCliente> cliente, boolean bolMostrarDatosPublicosYPrivados)
    {
        this.cliente = cliente;
        this.bolMostrarDatosPublicosYPrivados = bolMostrarDatosPublicosYPrivados;
    }

    public ArrayList<Document> obtenerArregloDocumentosListaClientesPrincipales()
    {

        arregloListaClientesPrincipales = new ArrayList<>();

        if (this.bolMostrarDatosPublicosYPrivados == false)
        {
            for (int j = 0; j < cliente.size(); j++)
            {
                Document documentoClientePrincipal = new Document();
                if (cliente.get(j).getTipoRelacion() != null)
                {
                    if (cliente.get(j).getTipoRelacion().equals("DECLARANTE"))
                    {

                        if (cliente.get(j).getTipoOperacion() != null)
                        {
                            documentoClientePrincipal.append("tipoOperacion", cliente.get(j).getTipoOperacion());
                        }

                        if (cliente.get(j).getRealizaActividadLucrativa() != null)
                        {
                            documentoClientePrincipal.append("realizaActividadLucrativa", cliente.get(j).getRealizaActividadLucrativa());
                        }

                        documentoClientePrincipal.append("tipoRelacion", cliente.get(j).getTipoRelacion());

                        if (cliente.get(j).getEmpresa() != null)
                        {
                            Document documentoEmpresa = new Document();

                            if (cliente.get(j).getEmpresa().getNombreEmpresaServicio() != null)
                            {
                                documentoEmpresa.append("nombreEmpresaServicio", cliente.get(j).getEmpresa().getNombreEmpresaServicio());
                            }

                            if (cliente.get(j).getEmpresa().getRfc() != null)
                            {
                                documentoEmpresa.append("rfc", cliente.get(j).getEmpresa().getRfc());
                            }

                            if (documentoEmpresa.isEmpty() == false)
                            {
                                documentoClientePrincipal.append("empresa", documentoEmpresa);
                            }
                        }

                        if (cliente.get(j).getClientePrincipal() != null)
                        {
                            Document documentoCliente = new Document();
                            if (cliente.get(j).getClientePrincipal().getTipoPersona() != null)
                            {
                                if (cliente.get(j).getClientePrincipal().getTipoPersona().equals("MORAL"))
                                {

                                    documentoCliente.append("tipoPersona", cliente.get(j).getClientePrincipal().getTipoPersona());

                                    if (cliente.get(j).getClientePrincipal().getNombreRazonSocial() != null)
                                    {
                                        documentoCliente.append("nombreRazonSocial", cliente.get(j).getClientePrincipal().getNombreRazonSocial());
                                    }

                                    if (cliente.get(j).getClientePrincipal().getRfc() != null)
                                    {
                                        documentoCliente.append("rfc", cliente.get(j).getClientePrincipal().getRfc());
                                    }

                                }
                            }
                            if (documentoCliente.isEmpty() == false)
                            {
                                documentoClientePrincipal.append("clientePrincipal", documentoCliente);
                            }
                        }

                        if (cliente.get(j).getSector() != null)
                        {
                            Document documentoSector = new Document();

                            if (cliente.get(j).getSector().getClave() != null)
                            {
                                documentoSector.append("clave", cliente.get(j).getSector().getClave());
                            }

                            if (cliente.get(j).getSector().getValor() != null)
                            {
                                documentoSector.append("valor", cliente.get(j).getSector().getValor());
                            }

                            if (documentoSector.isEmpty() == false)
                            {
                                documentoClientePrincipal.append("sector", documentoSector);
                            }
                        }

                        if (cliente.get(j).getMontoAproximadoGanancia() != null)
                        {
                            Document documentoMontoAproximadoGanancia = new Document();

                            if (cliente.get(j).getMontoAproximadoGanancia().getValor() != null)
                            {
                                documentoMontoAproximadoGanancia.append("valor", cliente.get(j).getMontoAproximadoGanancia().getValor());
                            }

                            if (cliente.get(j).getMontoAproximadoGanancia().getMoneda() != null)
                            {
                                documentoMontoAproximadoGanancia.append("moneda", cliente.get(j).getMontoAproximadoGanancia().getMoneda());
                            }

                            if (documentoMontoAproximadoGanancia.isEmpty() == false)
                            {
                                documentoClientePrincipal.append("montoAproximadoGanancia", documentoMontoAproximadoGanancia);
                            }
                        }

                        if (cliente.get(j).getUbicacion() != null)
                        {
                            Document documentoUbicacion = new Document();

                            if (cliente.get(j).getUbicacion().getPais() != null)
                            {
                                documentoUbicacion.append("pais", cliente.get(j).getUbicacion().getPais());
                            }

                            if (cliente.get(j).getUbicacion().getEntidadFederativa() != null)
                            {
                                Document documentoEntidadFederativa = new Document();

                                if (cliente.get(j).getUbicacion().getEntidadFederativa().getClave() != null)
                                {
                                    documentoEntidadFederativa.append("clave", cliente.get(j).getUbicacion().getEntidadFederativa().getClave());
                                }

                                if (cliente.get(j).getUbicacion().getEntidadFederativa().getValor() != null)
                                {
                                    documentoEntidadFederativa.append("valor", cliente.get(j).getUbicacion().getEntidadFederativa().getValor());
                                }

                                if (documentoEntidadFederativa.isEmpty() == false)
                                {
                                    documentoUbicacion.append("entidadFederativa", documentoEntidadFederativa);
                                }
                            }

                            if (documentoUbicacion.isEmpty() == false)
                            {
                                documentoClientePrincipal.append("ubicacion", documentoUbicacion);
                            }
                        }

                    }
                }
                if (documentoClientePrincipal.isEmpty() == false)
                {
                    arregloListaClientesPrincipales.add(documentoClientePrincipal);
                }
            }
        } else
        {

            for (int j = 0; j < cliente.size(); j++)
            {
                Document documentoClientePrincipal = new Document();

                if (cliente.get(j).getTipoOperacion() != null)
                {
                    documentoClientePrincipal.append("tipoOperacion", cliente.get(j).getTipoOperacion());
                }

                if (cliente.get(j).getRealizaActividadLucrativa() != null)
                {
                    documentoClientePrincipal.append("realizaActividadLucrativa", cliente.get(j).getRealizaActividadLucrativa());
                }

                if (cliente.get(j).getTipoRelacion() != null)
                {
                    documentoClientePrincipal.append("tipoRelacion", cliente.get(j).getTipoRelacion());
                }

                if (cliente.get(j).getEmpresa() != null)
                {
                    Document documentoEmpresa = new Document();

                    if (cliente.get(j).getEmpresa().getNombreEmpresaServicio() != null)
                    {
                        documentoEmpresa.append("nombreEmpresaServicio", cliente.get(j).getEmpresa().getNombreEmpresaServicio());
                    }

                    if (cliente.get(j).getEmpresa().getRfc() != null)
                    {
                        documentoEmpresa.append("rfc", cliente.get(j).getEmpresa().getRfc());
                    }

                    if (documentoEmpresa.isEmpty() == false)
                    {
                        documentoClientePrincipal.append("empresa", documentoEmpresa);
                    }
                }

                if (cliente.get(j).getClientePrincipal() != null)
                {
                    Document documentoCliente = new Document();

                    if (cliente.get(j).getClientePrincipal().getTipoPersona() != null)
                    {
                        documentoCliente.append("tipoPersona", cliente.get(j).getClientePrincipal().getTipoPersona());
                    }

                    if (cliente.get(j).getClientePrincipal().getNombreRazonSocial() != null)
                    {
                        documentoCliente.append("nombreRazonSocial", cliente.get(j).getClientePrincipal().getNombreRazonSocial());
                    }

                    if (cliente.get(j).getClientePrincipal().getRfc() != null)
                    {
                        documentoCliente.append("rfc", cliente.get(j).getClientePrincipal().getRfc());
                    }

                    if (documentoCliente.isEmpty() == false)
                    {
                        documentoClientePrincipal.append("clientePrincipal", documentoCliente);
                    }
                }

                if (cliente.get(j).getSector() != null)
                {
                    Document documentoSector = new Document();

                    if (cliente.get(j).getSector().getClave() != null)
                    {
                        documentoSector.append("clave", cliente.get(j).getSector().getClave());
                    }

                    if (cliente.get(j).getSector().getValor() != null)
                    {
                        documentoSector.append("valor", cliente.get(j).getSector().getValor());
                    }

                    if (documentoSector.isEmpty() == false)
                    {
                        documentoClientePrincipal.append("sector", documentoSector);
                    }
                }

                if (cliente.get(j).getMontoAproximadoGanancia() != null)
                {
                    Document documentoMontoAproximadoGanancia = new Document();

                    if (cliente.get(j).getMontoAproximadoGanancia().getValor() != null)
                    {
                        documentoMontoAproximadoGanancia.append("valor", cliente.get(j).getMontoAproximadoGanancia().getValor());
                    }

                    if (cliente.get(j).getMontoAproximadoGanancia().getMoneda() != null)
                    {
                        documentoMontoAproximadoGanancia.append("moneda", cliente.get(j).getMontoAproximadoGanancia().getMoneda());
                    }

                    if (documentoMontoAproximadoGanancia.isEmpty() == false)
                    {
                        documentoClientePrincipal.append("montoAproximadoGanancia", documentoMontoAproximadoGanancia);
                    }
                }

                if (cliente.get(j).getUbicacion() != null)
                {
                    Document documentoUbicacion = new Document();

                    if (cliente.get(j).getUbicacion().getPais() != null)
                    {
                        documentoUbicacion.append("pais", cliente.get(j).getUbicacion().getPais());
                    }

                    if (cliente.get(j).getUbicacion().getEntidadFederativa() != null)
                    {
                        Document documentoEntidadFederativa = new Document();

                        if (cliente.get(j).getUbicacion().getEntidadFederativa().getClave() != null)
                        {
                            documentoEntidadFederativa.append("clave", cliente.get(j).getUbicacion().getEntidadFederativa().getClave());
                        }

                        if (cliente.get(j).getUbicacion().getEntidadFederativa().getValor() != null)
                        {
                            documentoEntidadFederativa.append("valor", cliente.get(j).getUbicacion().getEntidadFederativa().getValor());
                        }

                        if (documentoEntidadFederativa.isEmpty() == false)
                        {
                            documentoUbicacion.append("entidadFederativa", documentoEntidadFederativa);
                        }
                    }

                    if (documentoUbicacion.isEmpty() == false)
                    {
                        documentoClientePrincipal.append("ubicacion", documentoUbicacion);
                    }
                }

                if (documentoClientePrincipal.isEmpty() == false)
                {
                    arregloListaClientesPrincipales.add(documentoClientePrincipal);
                }
            }

        }

        return arregloListaClientesPrincipales;

    }
}
