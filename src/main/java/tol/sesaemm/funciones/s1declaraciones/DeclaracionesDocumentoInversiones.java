package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesInversionesCuentasValores;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesDocumentoInversiones
{

    private DeclaracionesInversionesCuentasValores inversiones;
    private boolean bolMostrarDatosPublicosYPrivados;
    private String strTipoDeclaracion;
    private Document documentoInversiones;

    public DeclaracionesDocumentoInversiones(DeclaracionesInversionesCuentasValores inversiones, String strTipoDeclaracion, boolean bolMostrarDatosPublicosYPrivados) throws Exception
    {
        this.inversiones = inversiones;
        this.strTipoDeclaracion = strTipoDeclaracion;
        this.bolMostrarDatosPublicosYPrivados = bolMostrarDatosPublicosYPrivados;
    }

    public Document obtenerDocumentoInversiones()
    {

        documentoInversiones = new Document();

        if (inversiones != null)
        {
            if (this.bolMostrarDatosPublicosYPrivados == false)
            {

                if (inversiones.getNinguno() != null)
                {
                    documentoInversiones.append("ninguno", inversiones.getNinguno());
                }

                if (inversiones.getInversion() != null)
                {
                    ArrayList<Document> arregloInversiones = new DeclaracionesListaDocumentosInversiones(inversiones.getInversion(), this.strTipoDeclaracion, this.bolMostrarDatosPublicosYPrivados).obtenerArregloDocumentosListaInversiones();
                    if (arregloInversiones.isEmpty() == false)
                    {
                        documentoInversiones.append("inversion", arregloInversiones);
                    }
                }

            } else
            {

                if (inversiones.getNinguno() != null)
                {
                    documentoInversiones.append("ninguno", inversiones.getNinguno());
                }

                if (inversiones.getInversion() != null)
                {
                    ArrayList<Document> arregloInversiones = new DeclaracionesListaDocumentosInversiones(inversiones.getInversion(), this.strTipoDeclaracion, this.bolMostrarDatosPublicosYPrivados).obtenerArregloDocumentosListaInversiones();
                    if (arregloInversiones.isEmpty() == false)
                    {
                        documentoInversiones.append("inversion", arregloInversiones);
                    }
                }

                if (inversiones.getAclaracionesObservaciones() != null)
                {
                    documentoInversiones.append("aclaracionesObservaciones", inversiones.getAclaracionesObservaciones());
                }

            }
        }

        return documentoInversiones;

    }

}
