package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesDatosEmpleoCargoComision;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesDocumentoDatosEmpleoCargoComision
{

    private DeclaracionesDatosEmpleoCargoComision datosEmpleoCargoComision;
    private boolean bolMostrarDatosPublicosYPrivados;
    private String strTipoDeclaracion;
    private Document documentoDatosEmpleoCargoComision;

    public DeclaracionesDocumentoDatosEmpleoCargoComision(DeclaracionesDatosEmpleoCargoComision datosEmpleoCargoComision, String strTipoDeclaracion, boolean bolMostrarDatosPublicosYPrivados) throws Exception
    {
        this.datosEmpleoCargoComision = datosEmpleoCargoComision;
        this.strTipoDeclaracion = strTipoDeclaracion;
        this.bolMostrarDatosPublicosYPrivados = bolMostrarDatosPublicosYPrivados;
    }

    public Document obtenerDocumentoDeDatosEmpleoCargoComision()
    {

        documentoDatosEmpleoCargoComision = new Document();

        if (this.bolMostrarDatosPublicosYPrivados == false)
        {

            if (datosEmpleoCargoComision.getTipoOperacion() != null)
            {
                documentoDatosEmpleoCargoComision.append("tipoOperacion", datosEmpleoCargoComision.getTipoOperacion());
            }

            if (datosEmpleoCargoComision.getNivelOrdenGobierno() != null)
            {
                documentoDatosEmpleoCargoComision.append("nivelOrdenGobierno", datosEmpleoCargoComision.getNivelOrdenGobierno());
            }

            if (datosEmpleoCargoComision.getAmbitoPublico() != null)
            {
                documentoDatosEmpleoCargoComision.append("ambitoPublico", datosEmpleoCargoComision.getAmbitoPublico());
            }

            documentoDatosEmpleoCargoComision.append("nombreEntePublico", datosEmpleoCargoComision.getNombreEntePublico());

            if (datosEmpleoCargoComision.getAreaAdscripcion() != null)
            {
                documentoDatosEmpleoCargoComision.append("areaAdscripcion", datosEmpleoCargoComision.getAreaAdscripcion());
            }

            documentoDatosEmpleoCargoComision.append("empleoCargoComision", datosEmpleoCargoComision.getEmpleoCargoComision());

            if (datosEmpleoCargoComision.getContratadoPorHonorarios() != null)
            {
                documentoDatosEmpleoCargoComision.append("contratadoPorHonorarios", datosEmpleoCargoComision.getContratadoPorHonorarios());
            }

            if (datosEmpleoCargoComision.getNivelEmpleoCargoComision() != null)
            {
                documentoDatosEmpleoCargoComision.append("nivelEmpleoCargoComision", datosEmpleoCargoComision.getNivelEmpleoCargoComision());
            }

            if (datosEmpleoCargoComision.getFuncionPrincipal() != null)
            {
                documentoDatosEmpleoCargoComision.append("funcionPrincipal", datosEmpleoCargoComision.getFuncionPrincipal());
            }

            if (datosEmpleoCargoComision.getFechaTomaPosesion() != null)
            {
                documentoDatosEmpleoCargoComision.append("fechaTomaPosesion", datosEmpleoCargoComision.getFechaTomaPosesion());
            }

            if (datosEmpleoCargoComision.getTelefonoOficina() != null)
            {
                Document documentoTelefonoOficina = new Document();
                if (datosEmpleoCargoComision.getTelefonoOficina().getTelefono() != null)
                {
                    documentoTelefonoOficina.append("telefono", datosEmpleoCargoComision.getTelefonoOficina().getTelefono());
                }
                if (datosEmpleoCargoComision.getTelefonoOficina().getExtension() != null)
                {
                    documentoTelefonoOficina.append("extension", datosEmpleoCargoComision.getTelefonoOficina().getExtension());
                }
                if (documentoTelefonoOficina.isEmpty() == false)
                {
                    documentoDatosEmpleoCargoComision.append("telefonoOficina", documentoTelefonoOficina);
                }
            }

            if (datosEmpleoCargoComision.getDomicilioMexico() != null)
            {
                Document documentoDomicilioMexico = new DeclaracionesDocumentoDomicilioMexico(datosEmpleoCargoComision.getDomicilioMexico()).obtenerDocumentoDomicilioMexico();
                if (documentoDomicilioMexico.isEmpty() == false)
                {
                    documentoDatosEmpleoCargoComision.append("domicilioMexico", documentoDomicilioMexico);
                }
            }

            if (datosEmpleoCargoComision.getDomicilioExtranjero() != null)
            {
                Document documentoDomicilioExtranjero = new DeclaracionesDocumentoDomicilioExtranjero(datosEmpleoCargoComision.getDomicilioExtranjero()).obtenerDocumentoDomicilioExtranjero();
                if (documentoDomicilioExtranjero.isEmpty() == false)
                {
                    documentoDatosEmpleoCargoComision.append("domicilioExtranjero", documentoDomicilioExtranjero);
                }
            }

            if (strTipoDeclaracion.equals("MODIFICACIÓN"))
            {

                if (datosEmpleoCargoComision.getCuentaConOtroCargoPublico() != null)
                {
                    documentoDatosEmpleoCargoComision.append("cuentaConOtroCargoPublico", datosEmpleoCargoComision.getCuentaConOtroCargoPublico());
                }

                if (datosEmpleoCargoComision.getOtroEmpleoCargoComision() != null)
                {
                    ArrayList<Document> arregloOtroEmpleoCargoComision = new ArrayList<>();
                    for (int i = 0; i < datosEmpleoCargoComision.getOtroEmpleoCargoComision().size(); i++)
                    {
                        Document documentoOtroEmpleoCargoComision = new Document();

                        if (datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getNivelOrdenGobierno() != null)
                        {
                            documentoOtroEmpleoCargoComision.append("nivelOrdenGobierno", datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getNivelOrdenGobierno());
                        }

                        if (datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getAmbitoPublico() != null)
                        {
                            documentoOtroEmpleoCargoComision.append("ambitoPublico", datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getAmbitoPublico());
                        }

                        if (datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getNombreEntePublico() != null)
                        {
                            documentoOtroEmpleoCargoComision.append("nombreEntePublico", datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getNombreEntePublico());
                        }

                        if (datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getAreaAdscripcion() != null)
                        {
                            documentoOtroEmpleoCargoComision.append("areaAdscripcion", datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getAreaAdscripcion());
                        }

                        if (datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getEmpleoCargoComision() != null)
                        {
                            documentoOtroEmpleoCargoComision.append("empleoCargoComision", datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getEmpleoCargoComision());
                        }

                        if (datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getContratadoPorHonorarios() != null)
                        {
                            documentoOtroEmpleoCargoComision.append("contratadoPorHonorarios", datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getContratadoPorHonorarios());
                        }

                        if (datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getNivelEmpleoCargoComision() != null)
                        {
                            documentoOtroEmpleoCargoComision.append("nivelEmpleoCargoComision", datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getNivelEmpleoCargoComision());
                        }

                        if (datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getFuncionPrincipal() != null)
                        {
                            documentoOtroEmpleoCargoComision.append("funcionPrincipal", datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getFuncionPrincipal());
                        }

                        if (datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getFechaTomaPosesion() != null)
                        {
                            documentoOtroEmpleoCargoComision.append("fechaTomaPosesion", datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getFechaTomaPosesion());
                        }

                        if (datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getTelefonoOficina() != null)
                        {
                            Document documentoTelefonoOficina = new Document();
                            if (datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getTelefonoOficina().getTelefono() != null)
                            {
                                documentoTelefonoOficina.append("telefono", datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getTelefonoOficina().getTelefono());
                            }
                            if (datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getTelefonoOficina().getExtension() != null)
                            {
                                documentoTelefonoOficina.append("extension", datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getTelefonoOficina().getExtension());
                            }
                            if (documentoTelefonoOficina.isEmpty() == false)
                            {
                                documentoOtroEmpleoCargoComision.append("telefonoOficina", documentoTelefonoOficina);
                            }
                        }

                        if (datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getDomicilioMexico() != null)
                        {
                            Document documentoDomicilioMexico = new DeclaracionesDocumentoDomicilioMexico(datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getDomicilioMexico()).obtenerDocumentoDomicilioMexico();
                            if (documentoDomicilioMexico.isEmpty() == false)
                            {
                                documentoOtroEmpleoCargoComision.append("domicilioMexico", documentoDomicilioMexico);
                            }
                        }

                        if (datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getDomicilioExtranjero() != null)
                        {
                            Document documentoDomicilioExtranjero = new DeclaracionesDocumentoDomicilioExtranjero(datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getDomicilioExtranjero()).obtenerDocumentoDomicilioExtranjero();
                            if (documentoDomicilioExtranjero.isEmpty() == false)
                            {
                                documentoOtroEmpleoCargoComision.append("domicilioExtranjero", documentoDomicilioExtranjero);
                            }
                        }

                        if (documentoOtroEmpleoCargoComision.isEmpty() == false)
                        {
                            arregloOtroEmpleoCargoComision.add(documentoOtroEmpleoCargoComision);
                        }
                    }
                    if (arregloOtroEmpleoCargoComision.isEmpty() == false)
                    {
                        documentoDatosEmpleoCargoComision.append("otroEmpleoCargoComision", arregloOtroEmpleoCargoComision);
                    }
                }

            }

        } else
        {

            if (datosEmpleoCargoComision.getTipoOperacion() != null)
            {
                documentoDatosEmpleoCargoComision.append("tipoOperacion", datosEmpleoCargoComision.getTipoOperacion());
            }

            if (datosEmpleoCargoComision.getNivelOrdenGobierno() != null)
            {
                documentoDatosEmpleoCargoComision.append("nivelOrdenGobierno", datosEmpleoCargoComision.getNivelOrdenGobierno());
            }

            if (datosEmpleoCargoComision.getAmbitoPublico() != null)
            {
                documentoDatosEmpleoCargoComision.append("ambitoPublico", datosEmpleoCargoComision.getAmbitoPublico());
            }

            documentoDatosEmpleoCargoComision.append("nombreEntePublico", datosEmpleoCargoComision.getNombreEntePublico());

            if (datosEmpleoCargoComision.getAreaAdscripcion() != null)
            {
                documentoDatosEmpleoCargoComision.append("areaAdscripcion", datosEmpleoCargoComision.getAreaAdscripcion());
            }

            documentoDatosEmpleoCargoComision.append("empleoCargoComision", datosEmpleoCargoComision.getEmpleoCargoComision());

            documentoDatosEmpleoCargoComision.append("contratadoPorHonorarios", datosEmpleoCargoComision.getContratadoPorHonorarios());

            if (datosEmpleoCargoComision.getNivelEmpleoCargoComision() != null)
            {
                documentoDatosEmpleoCargoComision.append("nivelEmpleoCargoComision", datosEmpleoCargoComision.getNivelEmpleoCargoComision());
            }

            if (datosEmpleoCargoComision.getFuncionPrincipal() != null)
            {
                documentoDatosEmpleoCargoComision.append("funcionPrincipal", datosEmpleoCargoComision.getFuncionPrincipal());
            }

            if (datosEmpleoCargoComision.getFechaTomaPosesion() != null)
            {
                documentoDatosEmpleoCargoComision.append("fechaTomaPosesion", datosEmpleoCargoComision.getFechaTomaPosesion());
            }

            if (datosEmpleoCargoComision.getTelefonoOficina() != null)
            {
                Document documentoTelefonoOficina = new Document();
                if (datosEmpleoCargoComision.getTelefonoOficina().getTelefono() != null)
                {
                    documentoTelefonoOficina.append("telefono", datosEmpleoCargoComision.getTelefonoOficina().getTelefono());
                }
                if (datosEmpleoCargoComision.getTelefonoOficina().getExtension() != null)
                {
                    documentoTelefonoOficina.append("extension", datosEmpleoCargoComision.getTelefonoOficina().getExtension());
                }
                if (documentoTelefonoOficina.isEmpty() == false)
                {
                    documentoDatosEmpleoCargoComision.append("telefonoOficina", documentoTelefonoOficina);
                }
            }

            if (datosEmpleoCargoComision.getDomicilioMexico() != null)
            {
                Document documentoDomicilioMexico = new DeclaracionesDocumentoDomicilioMexico(datosEmpleoCargoComision.getDomicilioMexico()).obtenerDocumentoDomicilioMexico();
                if (documentoDomicilioMexico.isEmpty() == false)
                {
                    documentoDatosEmpleoCargoComision.append("domicilioMexico", documentoDomicilioMexico);
                }
            }

            if (datosEmpleoCargoComision.getDomicilioExtranjero() != null)
            {
                Document documentoDomicilioExtranjero = new DeclaracionesDocumentoDomicilioExtranjero(datosEmpleoCargoComision.getDomicilioExtranjero()).obtenerDocumentoDomicilioExtranjero();
                if (documentoDomicilioExtranjero.isEmpty() == false)
                {
                    documentoDatosEmpleoCargoComision.append("domicilioExtranjero", documentoDomicilioExtranjero);
                }
            }

            if (datosEmpleoCargoComision.getAclaracionesObservaciones() != null)
            {
                documentoDatosEmpleoCargoComision.append("aclaracionesObservaciones", datosEmpleoCargoComision.getAclaracionesObservaciones());
            }

            if (strTipoDeclaracion.equals("MODIFICACIÓN"))
            {

                if (datosEmpleoCargoComision.getCuentaConOtroCargoPublico() != null)
                {
                    documentoDatosEmpleoCargoComision.append("cuentaConOtroCargoPublico", datosEmpleoCargoComision.getCuentaConOtroCargoPublico());
                }

                if (datosEmpleoCargoComision.getOtroEmpleoCargoComision() != null)
                {
                    ArrayList<Document> arregloOtroEmpleoCargoComision = new ArrayList<>();
                    for (int i = 0; i < datosEmpleoCargoComision.getOtroEmpleoCargoComision().size(); i++)
                    {
                        Document documentoOtroEmpleoCargoComision = new Document();

                        if (datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getNivelOrdenGobierno() != null)
                        {
                            documentoOtroEmpleoCargoComision.append("nivelOrdenGobierno", datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getNivelOrdenGobierno());
                        }

                        if (datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getAmbitoPublico() != null)
                        {
                            documentoOtroEmpleoCargoComision.append("ambitoPublico", datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getAmbitoPublico());
                        }

                        if (datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getNombreEntePublico() != null)
                        {
                            documentoOtroEmpleoCargoComision.append("nombreEntePublico", datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getNombreEntePublico());
                        }

                        if (datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getAreaAdscripcion() != null)
                        {
                            documentoOtroEmpleoCargoComision.append("areaAdscripcion", datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getAreaAdscripcion());
                        }

                        if (datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getEmpleoCargoComision() != null)
                        {
                            documentoOtroEmpleoCargoComision.append("empleoCargoComision", datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getEmpleoCargoComision());
                        }

                        documentoOtroEmpleoCargoComision.append("contratadoPorHonorarios", datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getContratadoPorHonorarios());

                        if (datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getNivelEmpleoCargoComision() != null)
                        {
                            documentoOtroEmpleoCargoComision.append("nivelEmpleoCargoComision", datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getNivelEmpleoCargoComision());
                        }

                        if (datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getFuncionPrincipal() != null)
                        {
                            documentoOtroEmpleoCargoComision.append("funcionPrincipal", datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getFuncionPrincipal());
                        }

                        if (datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getFechaTomaPosesion() != null)
                        {
                            documentoOtroEmpleoCargoComision.append("fechaTomaPosesion", datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getFechaTomaPosesion());
                        }

                        if (datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getTelefonoOficina() != null)
                        {
                            Document documentoTelefonoOficina = new Document();
                            if (datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getTelefonoOficina().getTelefono() != null)
                            {
                                documentoTelefonoOficina.append("telefono", datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getTelefonoOficina().getTelefono());
                            }
                            if (datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getTelefonoOficina().getExtension() != null)
                            {
                                documentoTelefonoOficina.append("extension", datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getTelefonoOficina().getExtension());
                            }
                            if (documentoTelefonoOficina.isEmpty() == false)
                            {
                                documentoOtroEmpleoCargoComision.append("telefonoOficina", documentoTelefonoOficina);
                            }
                        }

                        if (datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getDomicilioMexico() != null)
                        {
                            Document documentoDomicilioMexico = new DeclaracionesDocumentoDomicilioMexico(datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getDomicilioMexico()).obtenerDocumentoDomicilioMexico();
                            if (documentoDomicilioMexico.isEmpty() == false)
                            {
                                documentoOtroEmpleoCargoComision.append("domicilioMexico", documentoDomicilioMexico);
                            }
                        }

                        if (datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getDomicilioExtranjero() != null)
                        {
                            Document documentoDomicilioExtranjero = new DeclaracionesDocumentoDomicilioExtranjero(datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getDomicilioExtranjero()).obtenerDocumentoDomicilioExtranjero();
                            if (documentoDomicilioExtranjero.isEmpty() == false)
                            {
                                documentoOtroEmpleoCargoComision.append("domicilioExtranjero", documentoDomicilioExtranjero);
                            }
                        }

                        if (datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getAclaracionesObservaciones() != null)
                        {
                            documentoOtroEmpleoCargoComision.append("aclaracionesObservaciones", datosEmpleoCargoComision.getOtroEmpleoCargoComision().get(i).getAclaracionesObservaciones());
                        }

                        if (documentoOtroEmpleoCargoComision.isEmpty() == false)
                        {
                            arregloOtroEmpleoCargoComision.add(documentoOtroEmpleoCargoComision);
                        }
                    }
                    if (arregloOtroEmpleoCargoComision.isEmpty() == false)
                    {
                        documentoDatosEmpleoCargoComision.append("otroEmpleoCargoComision", arregloOtroEmpleoCargoComision);
                    }
                }

            }

        }

        return documentoDatosEmpleoCargoComision;

    }

}
