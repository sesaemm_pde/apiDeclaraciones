/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.funciones.s1declaraciones;

import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesMetadata;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesDocumentoMetadata
{

    private DeclaracionesMetadata metadata;                         

    public DeclaracionesDocumentoMetadata(DeclaracionesMetadata metadata)
    {
        this.metadata = metadata;
    }

    public Document obtenerMetadata()
    { 

        Document documentoMetadata = new Document();

        documentoMetadata.append("actualizacion", metadata.getActualizacion());
        documentoMetadata.append("institucion", metadata.getInstitucion());
        documentoMetadata.append("tipo", metadata.getTipo());
        documentoMetadata.append("declaracionCompleta", metadata.getDeclaracionCompleta());
        documentoMetadata.append("actualizacionConflictoInteres", metadata.getActualizacionConflictoInteres());

        return documentoMetadata;

    }

}
