package tol.sesaemm.funciones.s1declaraciones;

import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesDatosPareja;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesDocumentoDatosPareja
{

    private DeclaracionesDatosPareja datosPareja;
    private boolean bolMostrarDatosPublicosYPrivados;
    private Document documentoDatosPareja;

    public DeclaracionesDocumentoDatosPareja(DeclaracionesDatosPareja datosPareja, boolean bolMostrarDatosPublicosYPrivados) throws Exception
    {
        this.datosPareja = datosPareja;
        this.bolMostrarDatosPublicosYPrivados = bolMostrarDatosPublicosYPrivados;
    }

    public Document obtenerDocumentoDatosPareja()
    {

        documentoDatosPareja = new Document();

        if (datosPareja != null)
        {
            if (this.bolMostrarDatosPublicosYPrivados == false)
            {

            } else
            {

                if (datosPareja.getNinguno() != null)
                {
                    documentoDatosPareja.append("ninguno", datosPareja.getNinguno());
                }

                if (datosPareja.getTipoOperacion() != null)
                {
                    documentoDatosPareja.append("tipoOperacion", datosPareja.getTipoOperacion());
                }

                if (datosPareja.getNombre() != null)
                {
                    documentoDatosPareja.append("nombre", datosPareja.getNombre());
                }

                if (datosPareja.getPrimerApellido() != null)
                {
                    documentoDatosPareja.append("primerApellido", datosPareja.getPrimerApellido());
                }

                if (datosPareja.getSegundoApellido() != null)
                {
                    documentoDatosPareja.append("segundoApellido", datosPareja.getSegundoApellido());
                }

                if (datosPareja.getFechaNacimiento() != null)
                {
                    documentoDatosPareja.append("fechaNacimiento", datosPareja.getFechaNacimiento());
                }

                if (datosPareja.getRfc() != null)
                {
                    documentoDatosPareja.append("rfc", datosPareja.getRfc());
                }

                if (datosPareja.getRelacionConDeclarante() != null)
                {
                    documentoDatosPareja.append("relacionConDeclarante", datosPareja.getRelacionConDeclarante());
                }

                if (datosPareja.getCiudadanoExtranjero() != null)
                {
                    documentoDatosPareja.append("ciudadanoExtranjero", datosPareja.getCiudadanoExtranjero());
                }

                if (datosPareja.getCurp() != null)
                {
                    documentoDatosPareja.append("curp", datosPareja.getCurp());
                }

                if (datosPareja.getEsDependienteEconomico() != null)
                {
                    documentoDatosPareja.append("esDependienteEconomico", datosPareja.getEsDependienteEconomico());
                }

                if (datosPareja.getHabitaDomicilioDeclarante() != null)
                {
                    documentoDatosPareja.append("habitaDomicilioDeclarante", datosPareja.getHabitaDomicilioDeclarante());
                }

                if (datosPareja.getLugarDondeReside() != null)
                {
                    documentoDatosPareja.append("lugarDondeReside", datosPareja.getLugarDondeReside());
                }

                if (datosPareja.getDomicilioMexico() != null)
                {
                    Document documentoDomicilioMexico = new DeclaracionesDocumentoDomicilioMexico(datosPareja.getDomicilioMexico()).obtenerDocumentoDomicilioMexico();
                    if (documentoDomicilioMexico.isEmpty() == false)
                    {
                        documentoDatosPareja.append("domicilioMexico", documentoDomicilioMexico);
                    }
                }

                if (datosPareja.getDomicilioExtranjero() != null)
                {
                    Document documentoDomicilioExtranjero = new DeclaracionesDocumentoDomicilioExtranjero(datosPareja.getDomicilioExtranjero()).obtenerDocumentoDomicilioExtranjero();
                    if (documentoDomicilioExtranjero.isEmpty() == false)
                    {
                        documentoDatosPareja.append("domicilioExtranjero", documentoDomicilioExtranjero);
                    }
                }

                if (datosPareja.getActividadLaboral() != null)
                {
                    Document documentoActiviadLaboral = new DeclaracionesDocumentoActividadLaboral(datosPareja.getActividadLaboral()).obtenerDocumentoActividadLaboral();
                    if (documentoActiviadLaboral.isEmpty() == false)
                    {
                        documentoDatosPareja.append("actividadLaboral", documentoActiviadLaboral);
                    }
                }

                if (datosPareja.getActividadLaboralSectorPublico() != null)
                {
                    Document documentoActividadLaboralSectorPublico = new DeclaracionesDocumentoActividadLaboralSectorPublico(datosPareja.getActividadLaboralSectorPublico(), null, "datosPareja").obtenerDocumentoActividadLaboralSectorPublico();
                    if (documentoActividadLaboralSectorPublico.isEmpty() == false)
                    {
                        documentoDatosPareja.append("actividadLaboralSectorPublico", documentoActividadLaboralSectorPublico);
                    }
                }

                if (datosPareja.getActividadLaboralSectorPrivadoOtro() != null)
                {
                    Document documentoActividadLaboralSectorPrivadoOtro = new DeclaracionesDocumentoActividadLaboralSectorPrivadoOtro(datosPareja.getActividadLaboralSectorPrivadoOtro(), null, "datosPareja").obtenerDocumentoActividadLaboralSectorPrivadoOtro();
                    if (documentoActividadLaboralSectorPrivadoOtro.isEmpty() == false)
                    {
                        documentoDatosPareja.append("actividadLaboralSectorPrivadoOtro", documentoActividadLaboralSectorPrivadoOtro);
                    }
                }

                if (datosPareja.getAclaracionesObservaciones() != null)
                {
                    documentoDatosPareja.append("aclaracionesObservaciones", datosPareja.getAclaracionesObservaciones());
                }

            }
        }

        return documentoDatosPareja;

    }

}
