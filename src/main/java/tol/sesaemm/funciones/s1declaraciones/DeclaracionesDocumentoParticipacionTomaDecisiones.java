package tol.sesaemm.funciones.s1declaraciones;

import java.util.ArrayList;
import org.bson.Document;
import tol.sesaemm.javabeans.s1declaraciones.DeclaracionesParticipacionTomaDecisiones;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx Colaboracion: Cristian Luna
 * cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
public class DeclaracionesDocumentoParticipacionTomaDecisiones
{

    private DeclaracionesParticipacionTomaDecisiones datosParticipacionTomaDeDecisiones;
    private boolean bolMostrarDatosPublicosYPrivados;
    private Document documentoParticipacionTomaDecisiones;

    public DeclaracionesDocumentoParticipacionTomaDecisiones(DeclaracionesParticipacionTomaDecisiones datosParticipacionTomaDeDecisiones, boolean bolMostrarDatosPublicosYPrivados) throws Exception
    {
        this.datosParticipacionTomaDeDecisiones = datosParticipacionTomaDeDecisiones;
        this.bolMostrarDatosPublicosYPrivados = bolMostrarDatosPublicosYPrivados;
    }

    public Document obtenerDocumentoParticipacionTomaDecisiones()
    {

        documentoParticipacionTomaDecisiones = new Document();

        if (datosParticipacionTomaDeDecisiones != null)
        {
            if (this.bolMostrarDatosPublicosYPrivados == false)
            {

                if (datosParticipacionTomaDeDecisiones.getNinguno() != null)
                {
                    documentoParticipacionTomaDecisiones.append("ninguno", datosParticipacionTomaDeDecisiones.getNinguno());
                }

                if (datosParticipacionTomaDeDecisiones.getParticipacion() != null)
                {
                    ArrayList listaParticipacion = new DeclaracionesListaDocumentosParticipacionTomaDecisiones(datosParticipacionTomaDeDecisiones.getParticipacion(), this.bolMostrarDatosPublicosYPrivados).obtenerArregloDocumentosListaParticipacionTomaDecisiones();
                    if (listaParticipacion.isEmpty() == false)
                    {
                        documentoParticipacionTomaDecisiones.append("participacion", listaParticipacion);
                    }
                }

            } else
            {

                if (datosParticipacionTomaDeDecisiones.getNinguno() != null)
                {
                    documentoParticipacionTomaDecisiones.append("ninguno", datosParticipacionTomaDeDecisiones.getNinguno());
                }

                if (datosParticipacionTomaDeDecisiones.getParticipacion() != null)
                {
                    ArrayList listaParticipacion = new DeclaracionesListaDocumentosParticipacionTomaDecisiones(datosParticipacionTomaDeDecisiones.getParticipacion(), this.bolMostrarDatosPublicosYPrivados).obtenerArregloDocumentosListaParticipacionTomaDecisiones();
                    if (listaParticipacion.isEmpty() == false)
                    {
                        documentoParticipacionTomaDecisiones.append("participacion", listaParticipacion);
                    }
                }

                if (datosParticipacionTomaDeDecisiones.getAclaracionesObservaciones() != null)
                {
                    documentoParticipacionTomaDecisiones.append("aclaracionesObservaciones", datosParticipacionTomaDeDecisiones.getAclaracionesObservaciones());
                }

            }
        }

        return documentoParticipacionTomaDecisiones;

    }

}
