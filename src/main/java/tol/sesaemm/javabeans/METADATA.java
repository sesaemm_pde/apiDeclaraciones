/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class METADATA implements Serializable
  {
    private String actualizacion;
    private String institucion;
    private String contacto;
    private String personaContacto;

    public METADATA()
      {
        this.actualizacion = "";
        this.institucion = "";
        this.contacto = "";
        this.personaContacto = "";
      }

    public String getActualizacion()
      {
        return actualizacion;
      }

    public void setActualizacion(String actualizacion)
      {
        this.actualizacion = actualizacion;
      }

    public String getInstitucion()
      {
        return institucion;
      }

    public void setInstitucion(String institucion)
      {
        this.institucion = institucion;
      }

    public String getContacto()
      {
        return contacto;
      }

    public void setContacto(String contacto)
      {
        this.contacto = contacto;
      }

    public String getPersonaContacto()
      {
        return personaContacto;
      }

    public void setPersonaContacto(String personaContacto)
      {
        this.personaContacto = personaContacto;
      }

  }
