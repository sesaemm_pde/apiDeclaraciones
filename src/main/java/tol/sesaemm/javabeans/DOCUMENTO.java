/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans;

import java.io.Serializable;
import org.bson.codecs.pojo.annotations.BsonProperty;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DOCUMENTO implements Serializable
  {
    @BsonProperty("id")
    private String id;
    private String tipo;
    private String titulo;
    private String descripcion;
    private String url;
    private String fecha;

    public DOCUMENTO()
      {
      }
    
    public DOCUMENTO(String id, String tipo, String titulo, String descripcion, String url, String fecha)
      {
        this.id = id;
        this.tipo = tipo;
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.url = url;
        this.fecha = fecha;
      }

    
    public String getId()
      {
        return id;
      }

    public void setId(String id)
      {
        this.id = id;
      }

    public String getTipo()
      {
        return tipo;
      }

    public void setTipo(String tipo)
      {
        this.tipo = tipo;
      }

    public String getTitulo()
      {
        return titulo;
      }

    public void setTitulo(String titulo)
      {
        this.titulo = titulo;
      }

    public String getDescripcion()
      {
        return descripcion;
      }

    public void setDescripcion(String descripcion)
      {
        this.descripcion = descripcion;
      }

    public String getUrl()
      {
        return url;
      }

    public void setUrl(String url)
      {
        this.url = url;
      }

    public String getFecha()
      {
        return fecha;
      }

    public void setFecha(String fecha)
      {
        this.fecha = fecha;
      }
    
    
    
  }
