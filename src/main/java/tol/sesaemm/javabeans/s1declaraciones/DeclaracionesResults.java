/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import org.bson.codecs.pojo.annotations.BsonId;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;
import tol.sesaemm.javabeans.METADATOS;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesResults implements Serializable
  {

    @BsonId
    @BsonProperty("_id")
    private ObjectId idSpdn;                            
    private String id;                                  
    private DeclaracionesMetadata metadata;
    private DeclaracionesDeclaracion declaracion;
    @JsonIgnore                                                                 
    private String dependencia;
    @JsonIgnore                                                                 
    private METADATOS metadatos;
    @JsonIgnore                                                                 
    private String publicar;    
    
    public DeclaracionesResults()
      {
      }

    public ObjectId getIdSpdn()
      {
        return idSpdn;
      }

    public void setIdSpdn(ObjectId idSpdn)
      {
        this.idSpdn = idSpdn;
      }

    public String getId()
      {
        return id;
      }

    public void setId(String id)
      {
        this.id = id;
      }

    public DeclaracionesMetadata getMetadata()
      {
        return metadata;
      }

    public void setMetadata(DeclaracionesMetadata metadata)
      {
        this.metadata = metadata;
      }

    public DeclaracionesDeclaracion getDeclaracion()
      {
        return declaracion;
      }

    public void setDeclaracion(DeclaracionesDeclaracion declaracion)
      {
        this.declaracion = declaracion;
      }

    public String getDependencia()
      {
        return dependencia;
      }

    public void setDependencia(String dependencia)
      {
        this.dependencia = dependencia;
      }

    public METADATOS getMetadatos()
      {
        return metadatos;
      }

    public void setMetadatos(METADATOS metadatos)
      {
        this.metadatos = metadatos;
      }

    public String getPublicar()
      {
        return publicar;
      }

    public void setPublicar(String publicar)
      {
        this.publicar = publicar;
      }

  }
