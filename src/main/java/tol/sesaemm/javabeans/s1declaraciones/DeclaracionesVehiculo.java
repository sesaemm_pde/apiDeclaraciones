/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesVehiculo implements Serializable{
    
    private String tipoOperacion;
    private DeclaracionesTipoVehiculo tipoVehiculo;
    private ArrayList<DeclaracionesTitularBien> titular;
    private ArrayList<DeclaracionesTransmisor> transmisor;
    private String marca;
    private String modelo;
    private Integer anio;
    private String numeroSerieRegistro;
    private ArrayList<DeclaracionesTercero> tercero;
    private DeclaracionesLugarRegistro lugarRegistro;
    private DeclaracionesFormaAdquisicion formaAdquisicion;
    private String formaPago;
    private DeclaracionesMonto valorAdquisicion;
    private String fechaAdquisicion;
    private DeclaracionesMotivoBaja motivoBaja;

    public String getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public DeclaracionesTipoVehiculo getTipoVehiculo() {
        return tipoVehiculo;
    }

    public void setTipoVehiculo(DeclaracionesTipoVehiculo tipoVehiculo) {
        this.tipoVehiculo = tipoVehiculo;
    }

    public ArrayList<DeclaracionesTitularBien> getTitular() {
        return titular;
    }

    public void setTitular(ArrayList<DeclaracionesTitularBien> titular) {
        this.titular = titular;
    }

    public ArrayList<DeclaracionesTransmisor> getTransmisor() {
        return transmisor;
    }

    public void setTransmisor(ArrayList<DeclaracionesTransmisor> transmisor) {
        this.transmisor = transmisor;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    public String getNumeroSerieRegistro() {
        return numeroSerieRegistro;
    }

    public void setNumeroSerieRegistro(String numeroSerieRegistro) {
        this.numeroSerieRegistro = numeroSerieRegistro;
    }

    public ArrayList<DeclaracionesTercero> getTercero() {
        return tercero;
    }

    public void setTercero(ArrayList<DeclaracionesTercero> tercero) {
        this.tercero = tercero;
    }

    public DeclaracionesLugarRegistro getLugarRegistro() {
        return lugarRegistro;
    }

    public void setLugarRegistro(DeclaracionesLugarRegistro lugarRegistro) {
        this.lugarRegistro = lugarRegistro;
    }

    public DeclaracionesFormaAdquisicion getFormaAdquisicion() {
        return formaAdquisicion;
    }

    public void setFormaAdquisicion(DeclaracionesFormaAdquisicion formaAdquisicion) {
        this.formaAdquisicion = formaAdquisicion;
    }

    public String getFormaPago() {
        return formaPago;
    }

    public void setFormaPago(String formaPago) {
        this.formaPago = formaPago;
    }

    public DeclaracionesMonto getValorAdquisicion() {
        return valorAdquisicion;
    }

    public void setValorAdquisicion(DeclaracionesMonto valorAdquisicion) {
        this.valorAdquisicion = valorAdquisicion;
    }

    public String getFechaAdquisicion() {
        return fechaAdquisicion;
    }

    public void setFechaAdquisicion(String fechaAdquisicion) {
        this.fechaAdquisicion = fechaAdquisicion;
    }

    public DeclaracionesMotivoBaja getMotivoBaja() {
        return motivoBaja;
    }

    public void setMotivoBaja(DeclaracionesMotivoBaja motivoBaja) {
        this.motivoBaja = motivoBaja;
    }
}
