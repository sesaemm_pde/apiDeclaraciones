/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesExperienciaSectorPublicoSectorPrivado implements Serializable{
    
    private String tipoOperacion;
    private DeclaracionesAmbitoSector ambitoSector;
    private String nivelOrdenGobierno;
    private String ambitoPublico;
    private String nombreEntePublico;
    private String areaAdscripcion;
    private String empleoCargoComision;
    private String funcionPrincipal;
    private String fechaIngreso;
    private String fechaEgreso;
    private String ubicacion;
    private String nombreEmpresaSociedadAsociacion;
    private String rfc;
    private String area;
    private String puesto;
    private DeclaracionesSector sector;

    public String getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public DeclaracionesAmbitoSector getAmbitoSector() {
        return ambitoSector;
    }

    public void setAmbitoSector(DeclaracionesAmbitoSector ambitoSector) {
        this.ambitoSector = ambitoSector;
    }

    public String getNivelOrdenGobierno() {
        return nivelOrdenGobierno;
    }

    public void setNivelOrdenGobierno(String nivelOrdenGobierno) {
        this.nivelOrdenGobierno = nivelOrdenGobierno;
    }

    public String getAmbitoPublico() {
        return ambitoPublico;
    }

    public void setAmbitoPublico(String ambitoPublico) {
        this.ambitoPublico = ambitoPublico;
    }

    public String getNombreEntePublico() {
        return nombreEntePublico;
    }

    public void setNombreEntePublico(String nombreEntePublico) {
        this.nombreEntePublico = nombreEntePublico;
    }

    public String getAreaAdscripcion() {
        return areaAdscripcion;
    }

    public void setAreaAdscripcion(String areaAdscripcion) {
        this.areaAdscripcion = areaAdscripcion;
    }

    public String getEmpleoCargoComision() {
        return empleoCargoComision;
    }

    public void setEmpleoCargoComision(String empleoCargoComision) {
        this.empleoCargoComision = empleoCargoComision;
    }

    public String getFuncionPrincipal() {
        return funcionPrincipal;
    }

    public void setFuncionPrincipal(String funcionPrincipal) {
        this.funcionPrincipal = funcionPrincipal;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getFechaEgreso() {
        return fechaEgreso;
    }

    public void setFechaEgreso(String fechaEgreso) {
        this.fechaEgreso = fechaEgreso;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getNombreEmpresaSociedadAsociacion() {
        return nombreEmpresaSociedadAsociacion;
    }

    public void setNombreEmpresaSociedadAsociacion(String nombreEmpresaSociedadAsociacion) {
        this.nombreEmpresaSociedadAsociacion = nombreEmpresaSociedadAsociacion;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public DeclaracionesSector getSector() {
        return sector;
    }

    public void setSector(DeclaracionesSector sector) {
        this.sector = sector;
    }
}
