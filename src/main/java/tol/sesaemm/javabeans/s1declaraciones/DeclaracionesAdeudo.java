/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesAdeudo implements Serializable{
    
    private String tipoOperacion;
    private ArrayList<DeclaracionesTitularBien> titular;
    private DeclaracionesTipoAdeudo tipoAdeudo;
    private String numeroCuentaContrato;
    private String fechaAdquisicion;
    private DeclaracionesMonto montoOriginal;
    private DeclaracionesMonto saldoInsolutoSituacionActual;
    private DeclaracionesMonto saldoInsolutoDiciembreAnterior;
    private DeclaracionesMonto saldoInsolutoFechaConclusion;
    private Integer porcentajeIncrementoDecremento;
    private ArrayList<DeclaracionesTercero> tercero;
    private DeclaracionesOtorganteCredito otorganteCredito;
    private DeclaranteLocalizacionAdeudo localizacionAdeudo;

    public String getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public ArrayList<DeclaracionesTitularBien> getTitular() {
        return titular;
    }

    public void setTitular(ArrayList<DeclaracionesTitularBien> titular) {
        this.titular = titular;
    }

    public DeclaracionesTipoAdeudo getTipoAdeudo() {
        return tipoAdeudo;
    }

    public void setTipoAdeudo(DeclaracionesTipoAdeudo tipoAdeudo) {
        this.tipoAdeudo = tipoAdeudo;
    }

    public String getNumeroCuentaContrato() {
        return numeroCuentaContrato;
    }

    public void setNumeroCuentaContrato(String numeroCuentaContrato) {
        this.numeroCuentaContrato = numeroCuentaContrato;
    }

    public String getFechaAdquisicion() {
        return fechaAdquisicion;
    }

    public void setFechaAdquisicion(String fechaAdquisicion) {
        this.fechaAdquisicion = fechaAdquisicion;
    }

    public DeclaracionesMonto getMontoOriginal() {
        return montoOriginal;
    }

    public void setMontoOriginal(DeclaracionesMonto montoOriginal) {
        this.montoOriginal = montoOriginal;
    }

    public DeclaracionesMonto getSaldoInsolutoSituacionActual() {
        return saldoInsolutoSituacionActual;
    }

    public void setSaldoInsolutoSituacionActual(DeclaracionesMonto saldoInsolutoSituacionActual) {
        this.saldoInsolutoSituacionActual = saldoInsolutoSituacionActual;
    }

    public DeclaracionesMonto getSaldoInsolutoDiciembreAnterior() {
        return saldoInsolutoDiciembreAnterior;
    }

    public void setSaldoInsolutoDiciembreAnterior(DeclaracionesMonto saldoInsolutoDiciembreAnterior) {
        this.saldoInsolutoDiciembreAnterior = saldoInsolutoDiciembreAnterior;
    }

    public DeclaracionesMonto getSaldoInsolutoFechaConclusion() {
        return saldoInsolutoFechaConclusion;
    }

    public void setSaldoInsolutoFechaConclusion(DeclaracionesMonto saldoInsolutoFechaConclusion) {
        this.saldoInsolutoFechaConclusion = saldoInsolutoFechaConclusion;
    }

    public Integer getPorcentajeIncrementoDecremento() {
        return porcentajeIncrementoDecremento;
    }

    public void setPorcentajeIncrementoDecremento(Integer porcentajeIncrementoDecremento) {
        this.porcentajeIncrementoDecremento = porcentajeIncrementoDecremento;
    }

    public ArrayList<DeclaracionesTercero> getTercero() {
        return tercero;
    }

    public void setTercero(ArrayList<DeclaracionesTercero> tercero) {
        this.tercero = tercero;
    }

    public DeclaracionesOtorganteCredito getOtorganteCredito() {
        return otorganteCredito;
    }

    public void setOtorganteCredito(DeclaracionesOtorganteCredito otorganteCredito) {
        this.otorganteCredito = otorganteCredito;
    }

    public DeclaranteLocalizacionAdeudo getLocalizacionAdeudo() {
        return localizacionAdeudo;
    }

    public void setLocalizacionAdeudo(DeclaranteLocalizacionAdeudo localizacionAdeudo) {
        this.localizacionAdeudo = localizacionAdeudo;
    }
}
