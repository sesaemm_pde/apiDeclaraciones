/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesParticipacionParticipacionTomaDecisiones implements Serializable{
    
    private String tipoOperacion;
    private String tipoRelacion;
    private DeclaracionesTipoInstitucion tipoInstitucion;
    private String nombreInstitucion;
    private String rfc;
    private String puestoRol;
    private String fechaInicioParticipacion;
    private Boolean recibeRemuneracion;
    private DeclaracionesMonto montoMensual;
    private DeclaracionesUbicacion ubicacion;

    public String getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public String getTipoRelacion() {
        return tipoRelacion;
    }

    public void setTipoRelacion(String tipoRelacion) {
        this.tipoRelacion = tipoRelacion;
    }

    public DeclaracionesTipoInstitucion getTipoInstitucion() {
        return tipoInstitucion;
    }

    public void setTipoInstitucion(DeclaracionesTipoInstitucion tipoInstitucion) {
        this.tipoInstitucion = tipoInstitucion;
    }

    public String getNombreInstitucion() {
        return nombreInstitucion;
    }

    public void setNombreInstitucion(String nombreInstitucion) {
        this.nombreInstitucion = nombreInstitucion;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getPuestoRol() {
        return puestoRol;
    }

    public void setPuestoRol(String puestoRol) {
        this.puestoRol = puestoRol;
    }

    public String getFechaInicioParticipacion() {
        return fechaInicioParticipacion;
    }

    public void setFechaInicioParticipacion(String fechaInicioParticipacion) {
        this.fechaInicioParticipacion = fechaInicioParticipacion;
    }

    public Boolean getRecibeRemuneracion() {
        return recibeRemuneracion;
    }

    public void setRecibeRemuneracion(Boolean recibeRemuneracion) {
        this.recibeRemuneracion = recibeRemuneracion;
    }

    public DeclaracionesMonto getMontoMensual() {
        return montoMensual;
    }

    public void setMontoMensual(DeclaracionesMonto montoMensual) {
        this.montoMensual = montoMensual;
    }

    public DeclaracionesUbicacion getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(DeclaracionesUbicacion ubicacion) {
        this.ubicacion = ubicacion;
    }
}
