/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesBienesInmueblesQuery implements Serializable{
    
    private DeclaracionesSuperficieConstruccionQuery superficieConstruccion;
    private DeclaracionesSuperficieTerrenoQuery superficieTerreno;
    private String formaAdquisicion;
    private DeclaracionesValorAdquisicionQuery valorAdquisicion;

    public DeclaracionesSuperficieConstruccionQuery getSuperficieConstruccion() {
        return superficieConstruccion;
    }

    public void setSuperficieConstruccion(DeclaracionesSuperficieConstruccionQuery superficieConstruccion) {
        this.superficieConstruccion = superficieConstruccion;
    }

    public DeclaracionesSuperficieTerrenoQuery getSuperficieTerreno() {
        return superficieTerreno;
    }

    public void setSuperficieTerreno(DeclaracionesSuperficieTerrenoQuery superficieTerreno) {
        this.superficieTerreno = superficieTerreno;
    }

    public String getFormaAdquisicion() {
        return formaAdquisicion;
    }

    public void setFormaAdquisicion(String formaAdquisicion) {
        this.formaAdquisicion = formaAdquisicion;
    }

    public DeclaracionesValorAdquisicionQuery getValorAdquisicion() {
        return valorAdquisicion;
    }

    public void setValorAdquisicion(DeclaracionesValorAdquisicionQuery valorAdquisicion) {
        this.valorAdquisicion = valorAdquisicion;
    }
}
