/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesParticipacion implements Serializable{
    
    private Boolean ninguno;
    private ArrayList<DeclaracionesParticipacionParticipacion> participacion;
    private String aclaracionesObservaciones;

    public Boolean getNinguno() {
        return ninguno;
    }

    public void setNinguno(Boolean ninguno) {
        this.ninguno = ninguno;
    }

    public ArrayList<DeclaracionesParticipacionParticipacion> getParticipacion() {
        return participacion;
    }

    public void setParticipacion(ArrayList<DeclaracionesParticipacionParticipacion> participacion) {
        this.participacion = participacion;
    }

    public String getAclaracionesObservaciones() {
        return aclaracionesObservaciones;
    }

    public void setAclaracionesObservaciones(String aclaracionesObservaciones) {
        this.aclaracionesObservaciones = aclaracionesObservaciones;
    }
}
