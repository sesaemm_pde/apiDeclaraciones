/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesRepresentacionRepresentaciones implements Serializable{
    
    private String tipoOperacion;
    private String tipoRelacion;
    private String tipoRepresentacion;
    private String fechaInicioRepresentacion;
    private String tipoPersona;
    private String nombreRazonSocial;
    private String rfc;
    private Boolean recibeRemuneracion;
    private DeclaracionesMonto montoMensual;
    private DeclaracionesUbicacion ubicacion;
    private DeclaracionesSector sector;

    public String getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public String getTipoRelacion() {
        return tipoRelacion;
    }

    public void setTipoRelacion(String tipoRelacion) {
        this.tipoRelacion = tipoRelacion;
    }

    public String getTipoRepresentacion() {
        return tipoRepresentacion;
    }

    public void setTipoRepresentacion(String tipoRepresentacion) {
        this.tipoRepresentacion = tipoRepresentacion;
    }

    public String getFechaInicioRepresentacion() {
        return fechaInicioRepresentacion;
    }

    public void setFechaInicioRepresentacion(String fechaInicioRepresentacion) {
        this.fechaInicioRepresentacion = fechaInicioRepresentacion;
    }

    public String getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public String getNombreRazonSocial() {
        return nombreRazonSocial;
    }

    public void setNombreRazonSocial(String nombreRazonSocial) {
        this.nombreRazonSocial = nombreRazonSocial;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public Boolean getRecibeRemuneracion() {
        return recibeRemuneracion;
    }

    public void setRecibeRemuneracion(Boolean recibeRemuneracion) {
        this.recibeRemuneracion = recibeRemuneracion;
    }

    public DeclaracionesMonto getMontoMensual() {
        return montoMensual;
    }

    public void setMontoMensual(DeclaracionesMonto montoMensual) {
        this.montoMensual = montoMensual;
    }

    public DeclaracionesUbicacion getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(DeclaracionesUbicacion ubicacion) {
        this.ubicacion = ubicacion;
    }

    public DeclaracionesSector getSector() {
        return sector;
    }

    public void setSector(DeclaracionesSector sector) {
        this.sector = sector;
    }
}
