/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesDeclaracion implements Serializable{
    
    private DeclaracionesSituacionPatrimonial situacionPatrimonial;
    private DeclaracionesInteres interes;

    public DeclaracionesSituacionPatrimonial getSituacionPatrimonial() {
        return situacionPatrimonial;
    }

    public void setSituacionPatrimonial(DeclaracionesSituacionPatrimonial situacionPatrimonial) {
        this.situacionPatrimonial = situacionPatrimonial;
    }

    public DeclaracionesInteres getInteres() {
        return interes;
    }

    public void setInteres(DeclaracionesInteres interes) {
        this.interes = interes;
    }
}
