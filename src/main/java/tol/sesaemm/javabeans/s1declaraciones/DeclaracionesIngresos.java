/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesIngresos implements Serializable{
    
    private DeclaracionesMonto remuneracionMensualCargoPublico;
    private DeclaracionesMonto otrosIngresosMensualesTotal;
    private DeclaracionesMonto remuneracionAnualCargoPublico;
    private DeclaracionesMonto otrosIngresosAnualesTotal;
    private DeclaracionesMonto remuneracionConclusionCargoPublico;
    private DeclaracionesMonto otrosIngresosConclusionTotal;
    private DeclaracionesActividadIndustrialComercialEmpresarial actividadIndustrialComercialEmpresarial;
    private DeclaracionesActividadFinanciera actividadFinanciera;
    private DeclaracionesServiciosProfesionales serviciosProfesionales;
    private DeclaracionesEnajenacionBienes enajenacionBienes;
    private DeclaracionesOtrosIngresos otrosIngresos;
    private DeclaracionesMonto ingresoMensualNetoDeclarante;
    private DeclaracionesMonto ingresoMensualNetoParejaDependiente;
    private DeclaracionesMonto totalIngresosMensualesNetos;
    private DeclaracionesMonto ingresoAnualNetoDeclarante;
    private DeclaracionesMonto ingresoAnualNetoParejaDependiente;
    private DeclaracionesMonto totalIngresosAnualesNetos;
    private DeclaracionesMonto ingresoConclusionNetoDeclarante;
    private DeclaracionesMonto ingresoConclusionNetoParejaDependiente;
    private DeclaracionesMonto totalIngresosConclusionNetos;
    private String aclaracionesObservaciones;

    public DeclaracionesMonto getRemuneracionMensualCargoPublico() {
        return remuneracionMensualCargoPublico;
    }

    public void setRemuneracionMensualCargoPublico(DeclaracionesMonto remuneracionMensualCargoPublico) {
        this.remuneracionMensualCargoPublico = remuneracionMensualCargoPublico;
    }

    public DeclaracionesMonto getOtrosIngresosMensualesTotal() {
        return otrosIngresosMensualesTotal;
    }

    public void setOtrosIngresosMensualesTotal(DeclaracionesMonto otrosIngresosMensualesTotal) {
        this.otrosIngresosMensualesTotal = otrosIngresosMensualesTotal;
    }

    public DeclaracionesMonto getRemuneracionAnualCargoPublico() {
        return remuneracionAnualCargoPublico;
    }

    public void setRemuneracionAnualCargoPublico(DeclaracionesMonto remuneracionAnualCargoPublico) {
        this.remuneracionAnualCargoPublico = remuneracionAnualCargoPublico;
    }

    public DeclaracionesMonto getOtrosIngresosAnualesTotal() {
        return otrosIngresosAnualesTotal;
    }

    public void setOtrosIngresosAnualesTotal(DeclaracionesMonto otrosIngresosAnualesTotal) {
        this.otrosIngresosAnualesTotal = otrosIngresosAnualesTotal;
    }

    public DeclaracionesMonto getRemuneracionConclusionCargoPublico() {
        return remuneracionConclusionCargoPublico;
    }

    public void setRemuneracionConclusionCargoPublico(DeclaracionesMonto remuneracionConclusionCargoPublico) {
        this.remuneracionConclusionCargoPublico = remuneracionConclusionCargoPublico;
    }

    public DeclaracionesMonto getOtrosIngresosConclusionTotal() {
        return otrosIngresosConclusionTotal;
    }

    public void setOtrosIngresosConclusionTotal(DeclaracionesMonto otrosIngresosConclusionTotal) {
        this.otrosIngresosConclusionTotal = otrosIngresosConclusionTotal;
    }

    public DeclaracionesActividadIndustrialComercialEmpresarial getActividadIndustrialComercialEmpresarial() {
        return actividadIndustrialComercialEmpresarial;
    }

    public void setActividadIndustrialComercialEmpresarial(DeclaracionesActividadIndustrialComercialEmpresarial actividadIndustrialComercialEmpresarial) {
        this.actividadIndustrialComercialEmpresarial = actividadIndustrialComercialEmpresarial;
    }

    public DeclaracionesActividadFinanciera getActividadFinanciera() {
        return actividadFinanciera;
    }

    public void setActividadFinanciera(DeclaracionesActividadFinanciera actividadFinanciera) {
        this.actividadFinanciera = actividadFinanciera;
    }

    public DeclaracionesServiciosProfesionales getServiciosProfesionales() {
        return serviciosProfesionales;
    }

    public void setServiciosProfesionales(DeclaracionesServiciosProfesionales serviciosProfesionales) {
        this.serviciosProfesionales = serviciosProfesionales;
    }

    public DeclaracionesEnajenacionBienes getEnajenacionBienes() {
        return enajenacionBienes;
    }

    public void setEnajenacionBienes(DeclaracionesEnajenacionBienes enajenacionBienes) {
        this.enajenacionBienes = enajenacionBienes;
    }

    public DeclaracionesOtrosIngresos getOtrosIngresos() {
        return otrosIngresos;
    }

    public void setOtrosIngresos(DeclaracionesOtrosIngresos otrosIngresos) {
        this.otrosIngresos = otrosIngresos;
    }

    public DeclaracionesMonto getIngresoMensualNetoDeclarante() {
        return ingresoMensualNetoDeclarante;
    }

    public void setIngresoMensualNetoDeclarante(DeclaracionesMonto ingresoMensualNetoDeclarante) {
        this.ingresoMensualNetoDeclarante = ingresoMensualNetoDeclarante;
    }

    public DeclaracionesMonto getIngresoMensualNetoParejaDependiente() {
        return ingresoMensualNetoParejaDependiente;
    }

    public void setIngresoMensualNetoParejaDependiente(DeclaracionesMonto ingresoMensualNetoParejaDependiente) {
        this.ingresoMensualNetoParejaDependiente = ingresoMensualNetoParejaDependiente;
    }

    public DeclaracionesMonto getTotalIngresosMensualesNetos() {
        return totalIngresosMensualesNetos;
    }

    public void setTotalIngresosMensualesNetos(DeclaracionesMonto totalIngresosMensualesNetos) {
        this.totalIngresosMensualesNetos = totalIngresosMensualesNetos;
    }

    public DeclaracionesMonto getIngresoAnualNetoDeclarante() {
        return ingresoAnualNetoDeclarante;
    }

    public void setIngresoAnualNetoDeclarante(DeclaracionesMonto ingresoAnualNetoDeclarante) {
        this.ingresoAnualNetoDeclarante = ingresoAnualNetoDeclarante;
    }

    public DeclaracionesMonto getIngresoAnualNetoParejaDependiente() {
        return ingresoAnualNetoParejaDependiente;
    }

    public void setIngresoAnualNetoParejaDependiente(DeclaracionesMonto ingresoAnualNetoParejaDependiente) {
        this.ingresoAnualNetoParejaDependiente = ingresoAnualNetoParejaDependiente;
    }

    public DeclaracionesMonto getTotalIngresosAnualesNetos() {
        return totalIngresosAnualesNetos;
    }

    public void setTotalIngresosAnualesNetos(DeclaracionesMonto totalIngresosAnualesNetos) {
        this.totalIngresosAnualesNetos = totalIngresosAnualesNetos;
    }

    public DeclaracionesMonto getIngresoConclusionNetoDeclarante() {
        return ingresoConclusionNetoDeclarante;
    }

    public void setIngresoConclusionNetoDeclarante(DeclaracionesMonto ingresoConclusionNetoDeclarante) {
        this.ingresoConclusionNetoDeclarante = ingresoConclusionNetoDeclarante;
    }

    public DeclaracionesMonto getIngresoConclusionNetoParejaDependiente() {
        return ingresoConclusionNetoParejaDependiente;
    }

    public void setIngresoConclusionNetoParejaDependiente(DeclaracionesMonto ingresoConclusionNetoParejaDependiente) {
        this.ingresoConclusionNetoParejaDependiente = ingresoConclusionNetoParejaDependiente;
    }

    public DeclaracionesMonto getTotalIngresosConclusionNetos() {
        return totalIngresosConclusionNetos;
    }

    public void setTotalIngresosConclusionNetos(DeclaracionesMonto totalIngresosConclusionNetos) {
        this.totalIngresosConclusionNetos = totalIngresosConclusionNetos;
    }

    public String getAclaracionesObservaciones() {
        return aclaracionesObservaciones;
    }

    public void setAclaracionesObservaciones(String aclaracionesObservaciones) {
        this.aclaracionesObservaciones = aclaracionesObservaciones;
    }
}
