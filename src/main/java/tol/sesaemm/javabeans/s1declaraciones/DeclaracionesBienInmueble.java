/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesBienInmueble implements Serializable{
    
    private String tipoOperacion;
    private DeclaracionesTipoInmueble tipoInmueble;
    private ArrayList<DeclaracionesTitularBien> titular;
    private Integer porcentajePropiedad;
    private DeclaracionesSuperficie superficieTerreno;
    private DeclaracionesSuperficie superficieConstruccion;
    private ArrayList<DeclaracionesTercero> tercero;
    private ArrayList<DeclaracionesTransmisor> transmisor;
    private DeclaracionesFormaAdquisicion formaAdquisicion;
    private String formaPago;
    private DeclaracionesMonto valorAdquisicion;
    private String fechaAdquisicion;
    private String datoIdentificacion;
    private String valorConformeA;
    private DeclaracionesDomicilioMexico domicilioMexico;
    private DeclaracionesDomicilioExtranjero domicilioExtranjero;
    private DeclaracionesMotivoBaja motivoBaja;

    public String getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public DeclaracionesTipoInmueble getTipoInmueble() {
        return tipoInmueble;
    }

    public void setTipoInmueble(DeclaracionesTipoInmueble tipoInmueble) {
        this.tipoInmueble = tipoInmueble;
    }

    public ArrayList<DeclaracionesTitularBien> getTitular() {
        return titular;
    }

    public void setTitular(ArrayList<DeclaracionesTitularBien> titular) {
        this.titular = titular;
    }

    public Integer getPorcentajePropiedad() {
        return porcentajePropiedad;
    }

    public void setPorcentajePropiedad(Integer porcentajePropiedad) {
        this.porcentajePropiedad = porcentajePropiedad;
    }

    public DeclaracionesSuperficie getSuperficieTerreno() {
        return superficieTerreno;
    }

    public void setSuperficieTerreno(DeclaracionesSuperficie superficieTerreno) {
        this.superficieTerreno = superficieTerreno;
    }

    public DeclaracionesSuperficie getSuperficieConstruccion() {
        return superficieConstruccion;
    }

    public void setSuperficieConstruccion(DeclaracionesSuperficie superficieConstruccion) {
        this.superficieConstruccion = superficieConstruccion;
    }

    public ArrayList<DeclaracionesTercero> getTercero() {
        return tercero;
    }

    public void setTercero(ArrayList<DeclaracionesTercero> tercero) {
        this.tercero = tercero;
    }

    public ArrayList<DeclaracionesTransmisor> getTransmisor() {
        return transmisor;
    }

    public void setTransmisor(ArrayList<DeclaracionesTransmisor> transmisor) {
        this.transmisor = transmisor;
    }

    public DeclaracionesFormaAdquisicion getFormaAdquisicion() {
        return formaAdquisicion;
    }

    public void setFormaAdquisicion(DeclaracionesFormaAdquisicion formaAdquisicion) {
        this.formaAdquisicion = formaAdquisicion;
    }

    public String getFormaPago() {
        return formaPago;
    }

    public void setFormaPago(String formaPago) {
        this.formaPago = formaPago;
    }

    public DeclaracionesMonto getValorAdquisicion() {
        return valorAdquisicion;
    }

    public void setValorAdquisicion(DeclaracionesMonto valorAdquisicion) {
        this.valorAdquisicion = valorAdquisicion;
    }

    public String getFechaAdquisicion() {
        return fechaAdquisicion;
    }

    public void setFechaAdquisicion(String fechaAdquisicion) {
        this.fechaAdquisicion = fechaAdquisicion;
    }

    public String getDatoIdentificacion() {
        return datoIdentificacion;
    }

    public void setDatoIdentificacion(String datoIdentificacion) {
        this.datoIdentificacion = datoIdentificacion;
    }

    public String getValorConformeA() {
        return valorConformeA;
    }

    public void setValorConformeA(String valorConformeA) {
        this.valorConformeA = valorConformeA;
    }

    public DeclaracionesDomicilioMexico getDomicilioMexico() {
        return domicilioMexico;
    }

    public void setDomicilioMexico(DeclaracionesDomicilioMexico domicilioMexico) {
        this.domicilioMexico = domicilioMexico;
    }

    public DeclaracionesDomicilioExtranjero getDomicilioExtranjero() {
        return domicilioExtranjero;
    }

    public void setDomicilioExtranjero(DeclaracionesDomicilioExtranjero domicilioExtranjero) {
        this.domicilioExtranjero = domicilioExtranjero;
    }

    public DeclaracionesMotivoBaja getMotivoBaja() {
        return motivoBaja;
    }

    public void setMotivoBaja(DeclaracionesMotivoBaja motivoBaja) {
        this.motivoBaja = motivoBaja;
    }
}
