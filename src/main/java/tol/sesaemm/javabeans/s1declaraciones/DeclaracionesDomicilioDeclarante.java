/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesDomicilioDeclarante implements Serializable{

    private DeclaracionesDomicilioMexico domicilioMexico;
    private DeclaracionesDomicilioExtranjero domicilioExtranjero;
    private String aclaracionesObservaciones;

    public DeclaracionesDomicilioMexico getDomicilioMexico() {
        return domicilioMexico;
    }

    public void setDomicilioMexico(DeclaracionesDomicilioMexico domicilioMexico) {
        this.domicilioMexico = domicilioMexico;
    }

    public DeclaracionesDomicilioExtranjero getDomicilioExtranjero() {
        return domicilioExtranjero;
    }

    public void setDomicilioExtranjero(DeclaracionesDomicilioExtranjero domicilioExtranjero) {
        this.domicilioExtranjero = domicilioExtranjero;
    }

    public String getAclaracionesObservaciones() {
        return aclaracionesObservaciones;
    }

    public void setAclaracionesObservaciones(String aclaracionesObservaciones) {
        this.aclaracionesObservaciones = aclaracionesObservaciones;
    }
}
