/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesDatosEmpleoCargoComisionQuery implements Serializable{
    
    private String nombreEntePublico;
    private String entidadFederativa;
    private String municipioAlcaldia;
    private String empleoCargoComision;
    private String nivelOrdenGobierno;
    private String nivelEmpleoCargoComision;

    public String getNombreEntePublico() {
        return nombreEntePublico;
    }

    public void setNombreEntePublico(String nombreEntePublico) {
        this.nombreEntePublico = nombreEntePublico;
    }

    public String getEntidadFederativa() {
        return entidadFederativa;
    }

    public void setEntidadFederativa(String entidadFederativa) {
        this.entidadFederativa = entidadFederativa;
    }

    public String getMunicipioAlcaldia() {
        return municipioAlcaldia;
    }

    public void setMunicipioAlcaldia(String municipioAlcaldia) {
        this.municipioAlcaldia = municipioAlcaldia;
    }

    public String getEmpleoCargoComision() {
        return empleoCargoComision;
    }

    public void setEmpleoCargoComision(String empleoCargoComision) {
        this.empleoCargoComision = empleoCargoComision;
    }

    public String getNivelOrdenGobierno() {
        return nivelOrdenGobierno;
    }

    public void setNivelOrdenGobierno(String nivelOrdenGobierno) {
        this.nivelOrdenGobierno = nivelOrdenGobierno;
    }

    public String getNivelEmpleoCargoComision() {
        return nivelEmpleoCargoComision;
    }

    public void setNivelEmpleoCargoComision(String nivelEmpleoCargoComision) {
        this.nivelEmpleoCargoComision = nivelEmpleoCargoComision;
    }
}
