/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesPost implements Serializable{
    
    private int page;
    private int pageSize;
    private DeclaracionesSort sort;
    private DeclaracionesQuery query;

    public int getPage()
    {
        return page;
    }

    public void setPage(int page)
    {
        this.page = page;
    }

    public int getPageSize()
    {
        return pageSize;
    }

    public void setPageSize(int pageSize)
    {
        this.pageSize = pageSize;
    }

    public DeclaracionesSort getSort()
    {
        return sort;
    }

    public void setSort(DeclaracionesSort sort)
    {
        this.sort = sort;
    }

    public DeclaracionesQuery getQuery()
    {
        return query;
    }

    public void setQuery(DeclaracionesQuery query)
    {
        this.query = query;
    }
}