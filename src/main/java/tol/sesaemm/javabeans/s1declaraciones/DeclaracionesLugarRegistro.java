/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesLugarRegistro {
    
    private String pais;
    private DeclaracionesEntidadFederativa entidadFederativa;

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public DeclaracionesEntidadFederativa getEntidadFederativa() {
        return entidadFederativa;
    }

    public void setEntidadFederativa(DeclaracionesEntidadFederativa entidadFederativa) {
        this.entidadFederativa = entidadFederativa;
    }
}
