/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesOtrosIngresos implements Serializable{
    
    private DeclaracionesMonto remuneracionTotal;
    private ArrayList<DeclaracionesIngresosOtrosIngresos> ingresos;

    public DeclaracionesMonto getRemuneracionTotal() {
        return remuneracionTotal;
    }

    public void setRemuneracionTotal(DeclaracionesMonto remuneracionTotal) {
        this.remuneracionTotal = remuneracionTotal;
    }

    public ArrayList<DeclaracionesIngresosOtrosIngresos> getIngresos() {
        return ingresos;
    }

    public void setIngresos(ArrayList<DeclaracionesIngresosOtrosIngresos> ingresos) {
        this.ingresos = ingresos;
    }
}
