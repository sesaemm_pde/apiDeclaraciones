/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesDatosGenerales {
    
    private String nombre;
    private String primerApellido;
    private String segundoApellido;
    private String curp;
    private DeclaracionesRfc rfc;
    private DeclaracionesCorreoElectronico correoElectronico;
    private DeclaracionesTelefono telefono;
    private DeclaracionesSituacionPersonalEstadoCivil situacionPersonalEstadoCivil;
    private DeclaracionesRegimenMatrimonial regimenMatrimonial;
    private String paisNacimiento;
    private String nacionalidad;
    private String aclaracionesObservaciones;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public DeclaracionesRfc getRfc() {
        return rfc;
    }

    public void setRfc(DeclaracionesRfc rfc) {
        this.rfc = rfc;
    }

    public DeclaracionesCorreoElectronico getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(DeclaracionesCorreoElectronico correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public DeclaracionesTelefono getTelefono() {
        return telefono;
    }

    public void setTelefono(DeclaracionesTelefono telefono) {
        this.telefono = telefono;
    }

    public DeclaracionesSituacionPersonalEstadoCivil getSituacionPersonalEstadoCivil() {
        return situacionPersonalEstadoCivil;
    }

    public void setSituacionPersonalEstadoCivil(DeclaracionesSituacionPersonalEstadoCivil situacionPersonalEstadoCivil) {
        this.situacionPersonalEstadoCivil = situacionPersonalEstadoCivil;
    }

    public DeclaracionesRegimenMatrimonial getRegimenMatrimonial() {
        return regimenMatrimonial;
    }

    public void setRegimenMatrimonial(DeclaracionesRegimenMatrimonial regimenMatrimonial) {
        this.regimenMatrimonial = regimenMatrimonial;
    }

    public String getPaisNacimiento() {
        return paisNacimiento;
    }

    public void setPaisNacimiento(String paisNacimiento) {
        this.paisNacimiento = paisNacimiento;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getAclaracionesObservaciones() {
        return aclaracionesObservaciones;
    }

    public void setAclaracionesObservaciones(String aclaracionesObservaciones) {
        this.aclaracionesObservaciones = aclaracionesObservaciones;
    }
}
