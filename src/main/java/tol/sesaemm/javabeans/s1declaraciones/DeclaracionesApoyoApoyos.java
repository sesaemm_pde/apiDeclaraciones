/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesApoyoApoyos implements Serializable{
    
    private String tipoOperacion;
    private String tipoPersona;
    private DeclaracionesBeneficiariosPrograma beneficiarioPrograma;
    private String nombrePrograma;
    private String institucionOtorgante;
    private String nivelOrdenGobierno;
    private DeclaracionesTipoApoyo tipoApoyo;
    private String formaRecepcion;
    private DeclaracionesMonto montoApoyoMensual;
    private String especifiqueApoyo;

    public String getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public String getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public DeclaracionesBeneficiariosPrograma getBeneficiarioPrograma() {
        return beneficiarioPrograma;
    }

    public void setBeneficiarioPrograma(DeclaracionesBeneficiariosPrograma beneficiarioPrograma) {
        this.beneficiarioPrograma = beneficiarioPrograma;
    }

    public String getNombrePrograma() {
        return nombrePrograma;
    }

    public void setNombrePrograma(String nombrePrograma) {
        this.nombrePrograma = nombrePrograma;
    }

    public String getInstitucionOtorgante() {
        return institucionOtorgante;
    }

    public void setInstitucionOtorgante(String institucionOtorgante) {
        this.institucionOtorgante = institucionOtorgante;
    }

    public String getNivelOrdenGobierno() {
        return nivelOrdenGobierno;
    }

    public void setNivelOrdenGobierno(String nivelOrdenGobierno) {
        this.nivelOrdenGobierno = nivelOrdenGobierno;
    }

    public DeclaracionesTipoApoyo getTipoApoyo() {
        return tipoApoyo;
    }

    public void setTipoApoyo(DeclaracionesTipoApoyo tipoApoyo) {
        this.tipoApoyo = tipoApoyo;
    }

    public String getFormaRecepcion() {
        return formaRecepcion;
    }

    public void setFormaRecepcion(String formaRecepcion) {
        this.formaRecepcion = formaRecepcion;
    }

    public DeclaracionesMonto getMontoApoyoMensual() {
        return montoApoyoMensual;
    }

    public void setMontoApoyoMensual(DeclaracionesMonto montoApoyoMensual) {
        this.montoApoyoMensual = montoApoyoMensual;
    }

    public String getEspecifiqueApoyo() {
        return especifiqueApoyo;
    }

    public void setEspecifiqueApoyo(String especifiqueApoyo) {
        this.especifiqueApoyo = especifiqueApoyo;
    }
}
