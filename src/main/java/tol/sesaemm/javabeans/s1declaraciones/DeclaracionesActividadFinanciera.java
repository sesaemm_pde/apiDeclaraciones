/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesActividadFinanciera implements Serializable{
    
    private DeclaracionesMonto remuneracionTotal;
    private ArrayList<DeclaracionesActividadesActividadFinanciera> actividades;

    public DeclaracionesMonto getRemuneracionTotal() {
        return remuneracionTotal;
    }

    public void setRemuneracionTotal(DeclaracionesMonto remuneracionTotal) {
        this.remuneracionTotal = remuneracionTotal;
    }

    public ArrayList<DeclaracionesActividadesActividadFinanciera> getActividades() {
        return actividades;
    }

    public void setActividades(ArrayList<DeclaracionesActividadesActividadFinanciera> actividades) {
        this.actividades = actividades;
    }
}
