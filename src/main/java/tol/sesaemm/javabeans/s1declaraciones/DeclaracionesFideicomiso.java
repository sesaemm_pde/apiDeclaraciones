/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesFideicomiso implements Serializable{
    
    private String tipoOperacion;
    private String tipoRelacion;
    private String tipoFideicomiso;
    private String tipoParticipacion;
    private String rfcFideicomiso;
    private DeclaracionesFideicomitente fideicomitente;
    private DeclaracionesFiduciario fiduciario;
    private DeclaracionesFideicomisario fideicomisario;
    private DeclaracionesSector sector;
    private String extranjero;

    public String getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public String getTipoRelacion() {
        return tipoRelacion;
    }

    public void setTipoRelacion(String tipoRelacion) {
        this.tipoRelacion = tipoRelacion;
    }

    public String getTipoFideicomiso() {
        return tipoFideicomiso;
    }

    public void setTipoFideicomiso(String tipoFideicomiso) {
        this.tipoFideicomiso = tipoFideicomiso;
    }

    public String getTipoParticipacion() {
        return tipoParticipacion;
    }

    public void setTipoParticipacion(String tipoParticipacion) {
        this.tipoParticipacion = tipoParticipacion;
    }

    public String getRfcFideicomiso() {
        return rfcFideicomiso;
    }

    public void setRfcFideicomiso(String rfcFideicomiso) {
        this.rfcFideicomiso = rfcFideicomiso;
    }

    public DeclaracionesFideicomitente getFideicomitente() {
        return fideicomitente;
    }

    public void setFideicomitente(DeclaracionesFideicomitente fideicomitente) {
        this.fideicomitente = fideicomitente;
    }

    public DeclaracionesFiduciario getFiduciario() {
        return fiduciario;
    }

    public void setFiduciario(DeclaracionesFiduciario fiduciario) {
        this.fiduciario = fiduciario;
    }

    public DeclaracionesFideicomisario getFideicomisario() {
        return fideicomisario;
    }

    public void setFideicomisario(DeclaracionesFideicomisario fideicomisario) {
        this.fideicomisario = fideicomisario;
    }

    public DeclaracionesSector getSector() {
        return sector;
    }

    public void setSector(DeclaracionesSector sector) {
        this.sector = sector;
    }

    public String getExtranjero() {
        return extranjero;
    }

    public void setExtranjero(String extranjero) {
        this.extranjero = extranjero;
    }
}
