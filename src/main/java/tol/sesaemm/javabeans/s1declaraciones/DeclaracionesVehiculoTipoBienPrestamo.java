/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesVehiculoTipoBienPrestamo implements Serializable{
    
    private DeclaracionesTipoVehiculo tipo;
    private String marca;
    private String modelo;
    private Integer anio;
    private String numeroSerieRegistro;
    private DeclaracionesLugarRegistro lugarRegistro;

    public DeclaracionesTipoVehiculo getTipo() {
        return tipo;
    }

    public void setTipo(DeclaracionesTipoVehiculo tipo) {
        this.tipo = tipo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    public String getNumeroSerieRegistro() {
        return numeroSerieRegistro;
    }

    public void setNumeroSerieRegistro(String numeroSerieRegistro) {
        this.numeroSerieRegistro = numeroSerieRegistro;
    }

    public DeclaracionesLugarRegistro getLugarRegistro() {
        return lugarRegistro;
    }

    public void setLugarRegistro(DeclaracionesLugarRegistro lugarRegistro) {
        this.lugarRegistro = lugarRegistro;
    }
}
