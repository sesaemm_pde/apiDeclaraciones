/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesActividadAnualAnterior implements Serializable{
    
    private Boolean servidorPublicoAnioAnterior;
    private String fechaIngreso;
    private String fechaConclusion;
    private DeclaracionesMonto remuneracionNetaCargoPublico;
    private DeclaracionesMonto otrosIngresosTotal;
    private DeclaracionesActividadIndustrialComercialEmpresarial actividadIndustrialComercialEmpresarial;
    private DeclaracionesActividadFinanciera actividadFinanciera;
    private DeclaracionesServiciosProfesionales serviciosProfesionales;
    private DeclaracionesEnajenacionBienes enajenacionBienes;
    private DeclaracionesOtrosIngresos otrosIngresos;
    private DeclaracionesMonto ingresoNetoAnualDeclarante;
    private DeclaracionesMonto ingresoNetoAnualParejaDependiente;
    private DeclaracionesMonto totalIngresosNetosAnuales;
    private String aclaracionesObservaciones;

    public Boolean getServidorPublicoAnioAnterior() {
        return servidorPublicoAnioAnterior;
    }

    public void setServidorPublicoAnioAnterior(Boolean servidorPublicoAnioAnterior) {
        this.servidorPublicoAnioAnterior = servidorPublicoAnioAnterior;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getFechaConclusion() {
        return fechaConclusion;
    }

    public void setFechaConclusion(String fechaConclusion) {
        this.fechaConclusion = fechaConclusion;
    }

    public DeclaracionesMonto getRemuneracionNetaCargoPublico() {
        return remuneracionNetaCargoPublico;
    }

    public void setRemuneracionNetaCargoPublico(DeclaracionesMonto remuneracionNetaCargoPublico) {
        this.remuneracionNetaCargoPublico = remuneracionNetaCargoPublico;
    }

    public DeclaracionesMonto getOtrosIngresosTotal() {
        return otrosIngresosTotal;
    }

    public void setOtrosIngresosTotal(DeclaracionesMonto otrosIngresosTotal) {
        this.otrosIngresosTotal = otrosIngresosTotal;
    }

    public DeclaracionesActividadIndustrialComercialEmpresarial getActividadIndustrialComercialEmpresarial() {
        return actividadIndustrialComercialEmpresarial;
    }

    public void setActividadIndustrialComercialEmpresarial(DeclaracionesActividadIndustrialComercialEmpresarial actividadIndustrialComercialEmpresarial) {
        this.actividadIndustrialComercialEmpresarial = actividadIndustrialComercialEmpresarial;
    }

    public DeclaracionesActividadFinanciera getActividadFinanciera() {
        return actividadFinanciera;
    }

    public void setActividadFinanciera(DeclaracionesActividadFinanciera actividadFinanciera) {
        this.actividadFinanciera = actividadFinanciera;
    }

    public DeclaracionesServiciosProfesionales getServiciosProfesionales() {
        return serviciosProfesionales;
    }

    public void setServiciosProfesionales(DeclaracionesServiciosProfesionales serviciosProfesionales) {
        this.serviciosProfesionales = serviciosProfesionales;
    }

    public DeclaracionesEnajenacionBienes getEnajenacionBienes() {
        return enajenacionBienes;
    }

    public void setEnajenacionBienes(DeclaracionesEnajenacionBienes enajenacionBienes) {
        this.enajenacionBienes = enajenacionBienes;
    }

    public DeclaracionesOtrosIngresos getOtrosIngresos() {
        return otrosIngresos;
    }

    public void setOtrosIngresos(DeclaracionesOtrosIngresos otrosIngresos) {
        this.otrosIngresos = otrosIngresos;
    }

    public DeclaracionesMonto getIngresoNetoAnualDeclarante() {
        return ingresoNetoAnualDeclarante;
    }

    public void setIngresoNetoAnualDeclarante(DeclaracionesMonto ingresoNetoAnualDeclarante) {
        this.ingresoNetoAnualDeclarante = ingresoNetoAnualDeclarante;
    }

    public DeclaracionesMonto getIngresoNetoAnualParejaDependiente() {
        return ingresoNetoAnualParejaDependiente;
    }

    public void setIngresoNetoAnualParejaDependiente(DeclaracionesMonto ingresoNetoAnualParejaDependiente) {
        this.ingresoNetoAnualParejaDependiente = ingresoNetoAnualParejaDependiente;
    }

    public DeclaracionesMonto getTotalIngresosNetosAnuales() {
        return totalIngresosNetosAnuales;
    }

    public void setTotalIngresosNetosAnuales(DeclaracionesMonto totalIngresosNetosAnuales) {
        this.totalIngresosNetosAnuales = totalIngresosNetosAnuales;
    }

    public String getAclaracionesObservaciones() {
        return aclaracionesObservaciones;
    }

    public void setAclaracionesObservaciones(String aclaracionesObservaciones) {
        this.aclaracionesObservaciones = aclaracionesObservaciones;
    }
}
