/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesDomicilioMexico implements Serializable{
    
    private String calle;
    private String numeroExterior;
    private String numeroInterior;
    private String coloniaLocalidad;
    private DeclaracionesMunicipioAlcaldia municipioAlcaldia;
    private DeclaracionesEntidadFederativa entidadFederativa;
    private String codigoPostal;

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getNumeroExterior() {
        return numeroExterior;
    }

    public void setNumeroExterior(String numeroExterior) {
        this.numeroExterior = numeroExterior;
    }

    public String getNumeroInterior() {
        return numeroInterior;
    }

    public void setNumeroInterior(String numeroInterior) {
        this.numeroInterior = numeroInterior;
    }

    public String getColoniaLocalidad() {
        return coloniaLocalidad;
    }

    public void setColoniaLocalidad(String coloniaLocalidad) {
        this.coloniaLocalidad = coloniaLocalidad;
    }

    public DeclaracionesMunicipioAlcaldia getMunicipioAlcaldia() {
        return municipioAlcaldia;
    }

    public void setMunicipioAlcaldia(DeclaracionesMunicipioAlcaldia municipioAlcaldia) {
        this.municipioAlcaldia = municipioAlcaldia;
    }

    public DeclaracionesEntidadFederativa getEntidadFederativa() {
        return entidadFederativa;
    }

    public void setEntidadFederativa(DeclaracionesEntidadFederativa entidadFederativa) {
        this.entidadFederativa = entidadFederativa;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }
}
