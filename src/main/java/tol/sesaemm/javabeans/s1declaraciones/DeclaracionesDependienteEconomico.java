/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesDependienteEconomico implements Serializable{
    
    private String tipoOperacion;
    private String nombre;
    private String primerApellido;
    private String segundoApellido;
    private String fechaNacimiento;
    private String rfc;
    private DeclaracionesParentescoRelacion parentescoRelacion;
    private Boolean extranjero;
    private String curp;
    private Boolean habitaDomicilioDeclarante;
    private String lugarDondeReside;
    private DeclaracionesDomicilioMexico domicilioMexico;
    private DeclaracionesDomicilioExtranjero domicilioExtranjero;
    private DeclaracionesActividadLaboral actividadLaboral;
    private DeclaracionesActividadLaboralSectorPublicoDependienteEconomico actividadLaboralSectorPublico;    
    private DeclaracionesActividadLaboralSectorPrivadoOtroDependienteEconomico actividadLaboralSectorPrivadoOtro;

    public String getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public DeclaracionesParentescoRelacion getParentescoRelacion() {
        return parentescoRelacion;
    }

    public void setParentescoRelacion(DeclaracionesParentescoRelacion parentescoRelacion) {
        this.parentescoRelacion = parentescoRelacion;
    }

    public Boolean getExtranjero() {
        return extranjero;
    }

    public void setExtranjero(Boolean extranjero) {
        this.extranjero = extranjero;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public Boolean getHabitaDomicilioDeclarante() {
        return habitaDomicilioDeclarante;
    }

    public void setHabitaDomicilioDeclarante(Boolean habitaDomicilioDeclarante) {
        this.habitaDomicilioDeclarante = habitaDomicilioDeclarante;
    }

    public String getLugarDondeReside() {
        return lugarDondeReside;
    }

    public void setLugarDondeReside(String lugarDondeReside) {
        this.lugarDondeReside = lugarDondeReside;
    }

    public DeclaracionesDomicilioMexico getDomicilioMexico() {
        return domicilioMexico;
    }

    public void setDomicilioMexico(DeclaracionesDomicilioMexico domicilioMexico) {
        this.domicilioMexico = domicilioMexico;
    }

    public DeclaracionesDomicilioExtranjero getDomicilioExtranjero() {
        return domicilioExtranjero;
    }

    public void setDomicilioExtranjero(DeclaracionesDomicilioExtranjero domicilioExtranjero) {
        this.domicilioExtranjero = domicilioExtranjero;
    }

    public DeclaracionesActividadLaboral getActividadLaboral() {
        return actividadLaboral;
    }

    public void setActividadLaboral(DeclaracionesActividadLaboral actividadLaboral) {
        this.actividadLaboral = actividadLaboral;
    }

    public DeclaracionesActividadLaboralSectorPublicoDependienteEconomico getActividadLaboralSectorPublico() {
        return actividadLaboralSectorPublico;
    }

    public void setActividadLaboralSectorPublico(DeclaracionesActividadLaboralSectorPublicoDependienteEconomico actividadLaboralSectorPublico) {
        this.actividadLaboralSectorPublico = actividadLaboralSectorPublico;
    }

    public DeclaracionesActividadLaboralSectorPrivadoOtroDependienteEconomico getActividadLaboralSectorPrivadoOtro() {
        return actividadLaboralSectorPrivadoOtro;
    }

    public void setActividadLaboralSectorPrivadoOtro(DeclaracionesActividadLaboralSectorPrivadoOtroDependienteEconomico actividadLaboralSectorPrivadoOtro) {
        this.actividadLaboralSectorPrivadoOtro = actividadLaboralSectorPrivadoOtro;
    }

}
