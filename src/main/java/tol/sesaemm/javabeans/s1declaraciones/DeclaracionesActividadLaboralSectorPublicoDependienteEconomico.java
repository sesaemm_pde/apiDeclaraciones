/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesActividadLaboralSectorPublicoDependienteEconomico implements Serializable{
    
    private String nivelOrdenGobierno;
    private String ambitoPublico;
    private String nombreEntePublico;
    private String areaAdscripcion;
    private String empleoCargoComision;
    private String funcionPrincipal;
    private DeclaracionesMonto salarioMensualNeto;
    private String fechaIngreso;

    public String getNivelOrdenGobierno() {
        return nivelOrdenGobierno;
    }

    public void setNivelOrdenGobierno(String nivelOrdenGobierno) {
        this.nivelOrdenGobierno = nivelOrdenGobierno;
    }

    public String getAmbitoPublico() {
        return ambitoPublico;
    }

    public void setAmbitoPublico(String ambitoPublico) {
        this.ambitoPublico = ambitoPublico;
    }

    public String getNombreEntePublico() {
        return nombreEntePublico;
    }

    public void setNombreEntePublico(String nombreEntePublico) {
        this.nombreEntePublico = nombreEntePublico;
    }

    public String getAreaAdscripcion() {
        return areaAdscripcion;
    }

    public void setAreaAdscripcion(String areaAdscripcion) {
        this.areaAdscripcion = areaAdscripcion;
    }

    public String getEmpleoCargoComision() {
        return empleoCargoComision;
    }

    public void setEmpleoCargoComision(String empleoCargoComision) {
        this.empleoCargoComision = empleoCargoComision;
    }

    public String getFuncionPrincipal() {
        return funcionPrincipal;
    }

    public void setFuncionPrincipal(String funcionPrincipal) {
        this.funcionPrincipal = funcionPrincipal;
    }

    public DeclaracionesMonto getSalarioMensualNeto() {
        return salarioMensualNeto;
    }

    public void setSalarioMensualNeto(DeclaracionesMonto salarioMensualNeto) {
        this.salarioMensualNeto = salarioMensualNeto;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }
}
