/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesQuery implements Serializable{
    
    private String id;
    private String nombres;
    private String primerApellido;           
    private String segundoApellido;
    private String escolaridadNivel;
    private DeclaracionesDatosEmpleoCargoComisionQuery datosEmpleoCargoComision;
    private DeclaracionesBienesInmueblesQuery bienesInmuebles;
    private DeclaracionesTotalIngresosNetosQuery totalIngresosNetos;
    private String rfcSolicitante;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getEscolaridadNivel() {
        return escolaridadNivel;
    }

    public void setEscolaridadNivel(String escolaridadNivel) {
        this.escolaridadNivel = escolaridadNivel;
    }

    public DeclaracionesDatosEmpleoCargoComisionQuery getDatosEmpleoCargoComision() {
        return datosEmpleoCargoComision;
    }

    public void setDatosEmpleoCargoComision(DeclaracionesDatosEmpleoCargoComisionQuery datosEmpleoCargoComision) {
        this.datosEmpleoCargoComision = datosEmpleoCargoComision;
    }

    public DeclaracionesBienesInmueblesQuery getBienesInmuebles() {
        return bienesInmuebles;
    }

    public void setBienesInmuebles(DeclaracionesBienesInmueblesQuery bienesInmuebles) {
        this.bienesInmuebles = bienesInmuebles;
    }

    public DeclaracionesTotalIngresosNetosQuery getTotalIngresosNetos() {
        return totalIngresosNetos;
    }

    public void setTotalIngresosNetos(DeclaracionesTotalIngresosNetosQuery totalIngresosNetos) {
        this.totalIngresosNetos = totalIngresosNetos;
    }

    public String getRfcSolicitante() {
        return rfcSolicitante;
    }

    public void setRfcSolicitante(String rfcSolicitante) {
        this.rfcSolicitante = rfcSolicitante;
    }
}
