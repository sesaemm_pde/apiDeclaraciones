/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesInteres implements Serializable{
    
    private DeclaracionesParticipacion participacion;
    private DeclaracionesParticipacionTomaDecisiones participacionTomaDecisiones;
    private DeclaracionesApoyos apoyos;
    private DeclaracionesRepresentaciones representacion;
    private DeclaracionesClientesPrincipales clientesPrincipales;
    private DeclaracionesBeneficiosPrivados beneficiosPrivados;
    private DeclaracionesFideicomisos fideicomisos;

    public DeclaracionesParticipacion getParticipacion() {
        return participacion;
    }

    public void setParticipacion(DeclaracionesParticipacion participacion) {
        this.participacion = participacion;
    }

    public DeclaracionesParticipacionTomaDecisiones getParticipacionTomaDecisiones() {
        return participacionTomaDecisiones;
    }

    public void setParticipacionTomaDecisiones(DeclaracionesParticipacionTomaDecisiones participacionTomaDecisiones) {
        this.participacionTomaDecisiones = participacionTomaDecisiones;
    }

    public DeclaracionesApoyos getApoyos() {
        return apoyos;
    }

    public void setApoyos(DeclaracionesApoyos apoyos) {
        this.apoyos = apoyos;
    }

    public DeclaracionesRepresentaciones getRepresentacion() {
        return representacion;
    }

    public void setRepresentacion(DeclaracionesRepresentaciones representacion) {
        this.representacion = representacion;
    }

    public DeclaracionesClientesPrincipales getClientesPrincipales() {
        return clientesPrincipales;
    }

    public void setClientesPrincipales(DeclaracionesClientesPrincipales clientesPrincipales) {
        this.clientesPrincipales = clientesPrincipales;
    }

    public DeclaracionesBeneficiosPrivados getBeneficiosPrivados() {
        return beneficiosPrivados;
    }

    public void setBeneficiosPrivados(DeclaracionesBeneficiosPrivados beneficiosPrivados) {
        this.beneficiosPrivados = beneficiosPrivados;
    }

    public DeclaracionesFideicomisos getFideicomisos() {
        return fideicomisos;
    }

    public void setFideicomisos(DeclaracionesFideicomisos fideicomisos) {
        this.fideicomisos = fideicomisos;
    }
}
