/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesDatosEmpleoCargoComision implements Serializable{
    
    private String tipoOperacion;
    private String nivelOrdenGobierno;
    private String ambitoPublico;
    private String nombreEntePublico;
    private String areaAdscripcion;
    private String empleoCargoComision;
    private Boolean contratadoPorHonorarios;
    private String nivelEmpleoCargoComision;
    private String funcionPrincipal;
    private String fechaTomaPosesion;
    private DeclaracionesTelefonoOficina telefonoOficina;
    private DeclaracionesDomicilioMexico domicilioMexico;
    private DeclaracionesDomicilioExtranjero domicilioExtranjero;
    private String aclaracionesObservaciones;
    private Boolean cuentaConOtroCargoPublico;
    private ArrayList<DeclaracionesOtroEmpleoCargoComision> otroEmpleoCargoComision;

    public String getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public String getNivelOrdenGobierno() {
        return nivelOrdenGobierno;
    }

    public void setNivelOrdenGobierno(String nivelOrdenGobierno) {
        this.nivelOrdenGobierno = nivelOrdenGobierno;
    }

    public String getAmbitoPublico() {
        return ambitoPublico;
    }

    public void setAmbitoPublico(String ambitoPublico) {
        this.ambitoPublico = ambitoPublico;
    }

    public String getNombreEntePublico() {
        return nombreEntePublico;
    }

    public void setNombreEntePublico(String nombreEntePublico) {
        this.nombreEntePublico = nombreEntePublico;
    }

    public String getAreaAdscripcion() {
        return areaAdscripcion;
    }

    public void setAreaAdscripcion(String areaAdscripcion) {
        this.areaAdscripcion = areaAdscripcion;
    }

    public String getEmpleoCargoComision() {
        return empleoCargoComision;
    }

    public void setEmpleoCargoComision(String empleoCargoComision) {
        this.empleoCargoComision = empleoCargoComision;
    }

    public Boolean getContratadoPorHonorarios() {
        return contratadoPorHonorarios;
    }

    public void setContratadoPorHonorarios(Boolean contratadoPorHonorarios) {
        this.contratadoPorHonorarios = contratadoPorHonorarios;
    }

    public String getNivelEmpleoCargoComision() {
        return nivelEmpleoCargoComision;
    }

    public void setNivelEmpleoCargoComision(String nivelEmpleoCargoComision) {
        this.nivelEmpleoCargoComision = nivelEmpleoCargoComision;
    }

    public String getFuncionPrincipal() {
        return funcionPrincipal;
    }

    public void setFuncionPrincipal(String funcionPrincipal) {
        this.funcionPrincipal = funcionPrincipal;
    }

    public String getFechaTomaPosesion() {
        return fechaTomaPosesion;
    }

    public void setFechaTomaPosesion(String fechaTomaPosesion) {
        this.fechaTomaPosesion = fechaTomaPosesion;
    }

    public DeclaracionesTelefonoOficina getTelefonoOficina() {
        return telefonoOficina;
    }

    public void setTelefonoOficina(DeclaracionesTelefonoOficina telefonoOficina) {
        this.telefonoOficina = telefonoOficina;
    }

    public DeclaracionesDomicilioMexico getDomicilioMexico() {
        return domicilioMexico;
    }

    public void setDomicilioMexico(DeclaracionesDomicilioMexico domicilioMexico) {
        this.domicilioMexico = domicilioMexico;
    }

    public DeclaracionesDomicilioExtranjero getDomicilioExtranjero() {
        return domicilioExtranjero;
    }

    public void setDomicilioExtranjero(DeclaracionesDomicilioExtranjero domicilioExtranjero) {
        this.domicilioExtranjero = domicilioExtranjero;
    }

    public String getAclaracionesObservaciones() {
        return aclaracionesObservaciones;
    }

    public void setAclaracionesObservaciones(String aclaracionesObservaciones) {
        this.aclaracionesObservaciones = aclaracionesObservaciones;
    }

    public Boolean getCuentaConOtroCargoPublico() {
        return cuentaConOtroCargoPublico;
    }

    public void setCuentaConOtroCargoPublico(Boolean cuentaConOtroCargoPublico) {
        this.cuentaConOtroCargoPublico = cuentaConOtroCargoPublico;
    }

    public ArrayList<DeclaracionesOtroEmpleoCargoComision> getOtroEmpleoCargoComision() {
        return otroEmpleoCargoComision;
    }

    public void setOtroEmpleoCargoComision(ArrayList<DeclaracionesOtroEmpleoCargoComision> otroEmpleoCargoComision) {
        this.otroEmpleoCargoComision = otroEmpleoCargoComision;
    }
}
