/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesServiciosServiciosProfesionales implements Serializable {
    
    private DeclaracionesMonto remuneracion;
    private String tipoServicio;

    public DeclaracionesMonto getRemuneracion() {
        return remuneracion;
    }

    public void setRemuneracion(DeclaracionesMonto remuneracion) {
        this.remuneracion = remuneracion;
    }

    public String getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(String tipoServicio) {
        this.tipoServicio = tipoServicio;
    }       
}
