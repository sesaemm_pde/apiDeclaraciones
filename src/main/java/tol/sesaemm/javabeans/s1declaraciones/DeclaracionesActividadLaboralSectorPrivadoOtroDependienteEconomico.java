/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.javabeans.s1declaraciones;

import java.io.Serializable;

/**
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
 */
public class DeclaracionesActividadLaboralSectorPrivadoOtroDependienteEconomico implements Serializable{
    
    private String nombreEmpresaSociedadAsociacion;
    private String rfc;
    private String empleoCargo;
    private String fechaIngreso;
    private DeclaracionesMonto salarioMensualNeto;
    private Boolean proveedorContratistaGobierno;
    private DeclaracionesSector sector;

    public String getNombreEmpresaSociedadAsociacion() {
        return nombreEmpresaSociedadAsociacion;
    }

    public void setNombreEmpresaSociedadAsociacion(String nombreEmpresaSociedadAsociacion) {
        this.nombreEmpresaSociedadAsociacion = nombreEmpresaSociedadAsociacion;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getEmpleoCargo() {
        return empleoCargo;
    }

    public void setEmpleoCargo(String empleoCargo) {
        this.empleoCargo = empleoCargo;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public DeclaracionesMonto getSalarioMensualNeto() {
        return salarioMensualNeto;
    }

    public void setSalarioMensualNeto(DeclaracionesMonto salarioMensualNeto) {
        this.salarioMensualNeto = salarioMensualNeto;
    }

    public Boolean getProveedorContratistaGobierno() {
        return proveedorContratistaGobierno;
    }

    public void setProveedorContratistaGobierno(Boolean proveedorContratistaGobierno) {
        this.proveedorContratistaGobierno = proveedorContratistaGobierno;
    }

    public DeclaracionesSector getSector() {
        return sector;
    }

    public void setSector(DeclaracionesSector sector) {
        this.sector = sector;
    }
    
}
